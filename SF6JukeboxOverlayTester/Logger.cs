﻿using MediaPlayerLib.Network.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SF6JukeboxOverlayTester
{
    public class Logger : ILogger
    {
        public event EventHandler<string> LogEvent;

        public void Log(string message)
        {
            LogEvent?.Invoke(this, message);
        }
    }
}
