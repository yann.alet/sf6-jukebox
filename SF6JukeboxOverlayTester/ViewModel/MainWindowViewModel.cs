﻿using MediaPlayerDAL;
using MediaPlayerLib;
using MediaPlayerLib.Network;
using MediaPlayerLib.Network.Contract;
using MediaPlayerLib.Network.Message;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SF6JukeboxOverlayTester.ViewModel
{
    public class MainWindowViewModel : BaseViewModel
    {
        private bool _isServerStarted;
        private Logger _logger;
        private string logs;
        private string _overlayFilePath;

        public MainWindowViewModel()
        {
            _logger = new Logger();
            _logger.LogEvent += OnLogEvent;
            StartServerCommand = new RelayCommand(OnStartServer, CanStartServer);
            SimulateTrackCommand = new RelayCommand(OnSimulateTrack, CanSimulateTrack);
            BrowseOverlayFileCommand = new RelayCommand(OnBrowseOverlayFile);
            BrowseMp3FileCommand = new RelayCommand(OnBrowseMp3File);
            Logs = String.Empty;
            //OverlayFilePath = @"C:\Users\Yann\source\repos\SF6Jukebox\SF6JukeboxOverlay\bin\Release\SF6JukeboxOverlay.exe";
        }

        private OverlayServer OverlayServer { get; set; }
        public RelayCommand StartServerCommand { get; set; }
        public RelayCommand BrowseOverlayFileCommand { get; set; }
        public RelayCommand BrowseMp3FileCommand { get; set; }
        public RelayCommand SimulateTrackCommand { get; set; }

        private void OnLogEvent(object sender, string e)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Logs);
            sb.AppendLine(e);
            Logs = sb.ToString();
        }

        public string OverlayFilePath
        {
            get => _overlayFilePath;
            set
            {
                _overlayFilePath = value;
                OnPropertyChanged();                
            }
        }

        public string Mp3FilePath
        {
            get => _mp3FilePath;
            set
            {
                _mp3FilePath = value;
                _isOverlayUpdated = false;
                OnPropertyChanged();
            }
        }

        public bool IsServerStarted 
        {
            get => _isServerStarted;
            set
            {
                _isServerStarted = value;
                OnPropertyChanged();
            }
        }

        public string Logs 
        { 
            get => logs;
            set
            {
                logs = value;
                OnPropertyChanged();
            }
        }

        private bool CanStartServer(object obj)
        {
            return !IsServerStarted && File.Exists(OverlayFilePath);
        }

        private void OnStartServer(object obj)
        {
            if(!File.Exists(OverlayFilePath))
            {
                MessageBox.Show("SF6JukeboxOverlay.exe not found.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            
            Process.Start(OverlayFilePath);
            OverlayServer = new OverlayServer(1212, _logger);
            OverlayServer.Start();
            IsServerStarted = true;
        }

        public void Disconnect()
        {
            if (OverlayServer != null)
            {
                OverlayServer.Stop();
                OverlayServer = null;
            }
            IsServerStarted = false;
        }

        private void OnBrowseOverlayFile(object obj)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "SF6JukeboxOverlay (*.exe)|*.exe";
            if(openFileDialog.ShowDialog() == true)
            {
                OverlayFilePath = openFileDialog.FileName;
            }
        }

        private void OnBrowseMp3File(object obj)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "MP3 File (*.mp3)|*.mp3";
            if (openFileDialog.ShowDialog() == true)
            {
                Mp3FilePath = openFileDialog.FileName;
            }
        }

        private bool CanSimulateTrack(object obj)
        {
            return IsServerStarted && File.Exists(Mp3FilePath);
        }

        private bool _isOverlayUpdated;
        private string _mp3FilePath;

        private void OnSimulateTrack(object obj)
        {
            if(!_isOverlayUpdated)
            {
                OverlayServer.Send(new FlowSceneChangedMessage() { FlowScene = (int)FlowSceneId.eBattleMain });
                OverlayServer.Send(new FightStateChangedMessage() { FightState = (int)FightState.Now });
                OverlayServer.Send(new TrackUpdateMessage() { Track = Mp3FilePath, Progress = 0.5f });
                _isOverlayUpdated = true;
            }
            else
            {
                OverlayServer.Send(new ShowOverlayMessage());
            }
        }
    }
}
