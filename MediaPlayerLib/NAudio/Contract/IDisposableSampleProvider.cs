﻿using NAudio.Wave;
using NAudio.Wave.SampleProviders;
using System;

namespace MediaPlayerLib.NAudio.Contract
{
    public interface IDisposableSampleProvider : ISampleProvider, IDisposable
    {
        event EventHandler<SampleProviderEventArgs> PlaybackCompleted;
        string FileName { get; }
    }
}
