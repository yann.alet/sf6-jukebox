﻿using NAudio.Wave.SampleProviders;
using NAudio.Wave;
using System;
using MediaPlayerLib.Network.Contract;
using MediaPlayerLib.NAudio.Contract;
using NAudio.Mixer;

namespace MediaPlayerLib.NAudio
{
    public class AudioPlaybackEngine : IDisposable
    {
        private readonly IWavePlayer outputDevice;
        private readonly MixingSampleProvider mixer;
        public event EventHandler<DisposableSampleProviderEventArgs> PlaybackCompleted;
        //public const int MaxSampleRate = 48000;
        public const int MaxSampleRate = 44100;

        public AudioPlaybackEngine(ILogger logger, int sampleRate = MaxSampleRate, int channelCount = 2)
        {
            outputDevice = new WaveOutEvent();
            mixer = new MixingSampleProvider(WaveFormat.CreateIeeeFloatWaveFormat(sampleRate, channelCount));
            mixer.MixerInputEnded += Mixer_MixerInputEnded;
            mixer.ReadFully = true;
            outputDevice.Init(mixer);            
            outputDevice.Play();            
            Logger = logger;
        }

        private void Mixer_MixerInputEnded(object sender, SampleProviderEventArgs e)
        {
            
        }

        private ISampleProvider ConvertToRightChannelCount(ISampleProvider input)
        {
            if (input.WaveFormat.Channels == mixer.WaveFormat.Channels
                /*&& input.WaveFormat.SampleRate == mixer.WaveFormat.SampleRate*/)
            {
                return input;
            }
            /*if (input.WaveFormat.Channels == 1 && mixer.WaveFormat.Channels == 2)
            {
                return new MonoToStereoSampleProvider(input);
            }*/
            throw new NotImplementedException("Not yet implemented this channel count conversion");
        }

        public void Play(IDisposableSampleProvider sampler)
        {
            AddMixerInput(sampler);
            sampler.PlaybackCompleted -= Sampler_PlaybackCompleted;
            sampler.PlaybackCompleted += Sampler_PlaybackCompleted;
            if(outputDevice.PlaybackState == PlaybackState.Paused)
            {
                outputDevice.Play();
            }
        }

        private void Sampler_PlaybackCompleted(object sender, SampleProviderEventArgs e)
        {
            PlaybackCompleted?.Invoke(this, new DisposableSampleProviderEventArgs(sender as IDisposableSampleProvider));
        }

        public void Stop(IDisposableSampleProvider sampler) 
        {
            sampler.PlaybackCompleted -= Sampler_PlaybackCompleted;
            mixer.RemoveMixerInput(sampler);            
            sampler.Dispose();
        }

        public void Pause()
        {
            outputDevice.Pause();            
        }

        public void Resume()
        {
            outputDevice.Play();
        }

        private void AddMixerInput(IDisposableSampleProvider input)
        {
            input.PlaybackCompleted -= Sampler_PlaybackCompleted;
            input.PlaybackCompleted += Sampler_PlaybackCompleted;
            mixer.AddMixerInput(ConvertToRightChannelCount(input));            
        }

        public void Dispose()
        {
            outputDevice.Dispose();
        }

        public ILogger Logger { get; }
    }
}

