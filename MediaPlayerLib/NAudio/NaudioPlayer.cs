﻿using MediaPlayerDAL.Model;
using MediaPlayerLib.Network.Contract;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace MediaPlayerLib.NAudio
{
    public class NaudioPlayer : IDisposable
    {
        public class OnReadInfo
        {
            public OnReadInfo(int playerId, long position, long length)
            {
                Position = position;
                Length = length;
                PlayerId = playerId;
            }

            public long Length { get; }
            public long Position { get; }
            public int PlayerId { get; }
        }

        public event EventHandler<DisposableSampleProviderEventArgs> PlaybackCompleted;

        public event EventHandler<OnReadInfo> OnRead;
        public event EventHandler<int> OnCrossFade;
        public string InstanceId { get; private set; }

        public NaudioPlayer(ILogger logger, Settings settings)
        {
            Logger = logger;
            _settings = settings;
            InstanceId = Guid.NewGuid().ToString();
            Logger.Log($"Instantiating player {InstanceId}");
            AudioPlaybackEngine = new AudioPlaybackEngine(logger);
            AudioPlaybackEngine.PlaybackCompleted += AudioPlaybackEngine_PlaybackCompleted;
            TrackSampler1 = new LateInitAudioFileReader(logger);
            TrackSampler2 = new LateInitAudioFileReader(logger);
            TrackSampler1.OnRead += TrackSampler1_OnRead;
            TrackSampler2.OnRead += TrackSampler2_OnRead;
            TrackSampler = new List<LateInitAudioFileReader>
            {
                TrackSampler1,
                TrackSampler2
            };
            MainTrackSamplerIndex = 0;
        }

        public int GetPlayerId()
        {
            return MainTrackSampler == TrackSampler1 ? 0 : 1;
        }

        public int GetTransitionPlayerId()
        {
            return MainTrackSampler == TrackSampler1 ? 1 : 0;
        }

        private void TrackSampler2_OnRead(object sender, LateInitAudioFileReader.OnReadInfo e)
        {
            OnRead?.Invoke(sender, new OnReadInfo(1, e.Position, e.Length));
        }

        private void TrackSampler1_OnRead(object sender, LateInitAudioFileReader.OnReadInfo e)
        {
            OnRead?.Invoke(sender, new OnReadInfo(0 , e.Position, e.Length));
        }

        private List<LateInitAudioFileReader> TrackSampler { get; set; }
        private LateInitAudioFileReader TrackSampler1 { get; set; }
        private LateInitAudioFileReader TrackSampler2 { get; set; }
        private int MainTrackSamplerIndex;
        private readonly Settings _settings;

        private AudioPlaybackEngine AudioPlaybackEngine { get; }
        public float FadeOutDuration { get; set; }
        private ILogger Logger { get; }
        public bool IsPlaying { get; private set; }
        public string FileName { get; private set; }
        public bool IsCrossFading { get; set; }
        
        public float Volume 
        {
            get
            {
                return MainTrackSampler.Volume;
            }
            set
            {
                if(MainTrackSampler != null)
                {
                    MainTrackSampler.Volume = value;
                }                
            }
        }

        public LateInitAudioFileReader MainTrackSampler
        {
            get
            {
                return TrackSampler[MainTrackSamplerIndex];
            }
        }

        private LateInitAudioFileReader SecondaryTrackSampler
        {
            get
            {
                return TrackSampler[(MainTrackSamplerIndex + 1) % 2];
            }
        }

        private void TrackSampler_PlaybackCompleted(object sender, DisposableSampleProviderEventArgs e)
        {
            if (sender == MainTrackSampler)
            {
                PlaybackCompleted?.Invoke(this, e);
            }
        }

        private void SwapSamplers()
        {
            MainTrackSamplerIndex = (MainTrackSamplerIndex + 1) % 2;
        }

        public float[] GetAudioData(int playerId)
        {
            return playerId == 0 ? TrackSampler1.AudioData : TrackSampler2.AudioData;
        }

        public float[] GetAudioData(string path, PlaybackSettings settings)
        {
            var cachedAudioFile = new AudioFile(Logger, path, settings.StartupSilence.Value, settings.StartupTrimming.Value, settings.EndTrimming.Value, false);
            return cachedAudioFile.AudioData;
        }

        public void CrossFadeWith(string path, float duration, PlaybackSettings playbackSettings)
        {
            Logger.Log($"CrossFadeWith {path}");
            float targetVolume = Volume;
            IsCrossFading = true;
            SwapSamplers();
            MainTrackSampler.Init(path, playbackSettings);
            MainTrackSampler.Volume = 0;
            MainTrackSampler.Position = playbackSettings.TransitionIsSynced ? SecondaryTrackSampler.Position : 0;
            MainTrackSampler.SetPlaybackSettings(playbackSettings);
            AudioPlaybackEngine.Play(MainTrackSampler);
            if (playbackSettings.TransitionIsSynced)
            {
                // Resync
                long resumePosition = SecondaryTrackSampler.Position;
                MainTrackSampler.Position = resumePosition;
            }
            OnCrossFade?.Invoke(this, MainTrackSampler == TrackSampler1 ? 0 : 1);
            Task.Run(() =>
            {
                FadeOut(SecondaryTrackSampler, duration);
                AudioPlaybackEngine.Stop(SecondaryTrackSampler);
            });

            Task.Run(() =>
            {
                FadeIn(MainTrackSampler, duration, targetVolume);
                IsCrossFading = false;
            });
        }

        public void Play(string path, PlaybackSettings playbackSettings)
        {
            Logger.Log($"Play {path}");
            FileName = path;
            MainTrackSampler.Init(path, playbackSettings);
            MainTrackSampler.Position = 0;
            try
            {
                AudioPlaybackEngine.Play(MainTrackSampler);
            }
            catch (Exception e)
            {
                Logger.Log($"Error {e}");
            }

            IsPlaying = true;   
            Volume = _settings.Volume;
        }

        public void Stop(string fileName)
        {
            Logger.Log($"Stop {fileName}");

            if (IsPlaying)
            {
                if(TrackSampler1.FileName == fileName)
                {
                    FadeOut(TrackSampler1, FadeOutDuration);
                    AudioPlaybackEngine.Stop(TrackSampler1);
                    Logger.Log($"Stopped {FileName}");
                }
                else if(TrackSampler2.FileName == fileName)
                {
                    FadeOut(TrackSampler2, FadeOutDuration);
                    AudioPlaybackEngine.Stop(TrackSampler2);
                    Logger.Log($"Stopped {FileName}");
                }
                else
                {
                    Logger.Log($"File {fileName} not found.");
                }
                IsPlaying = false;                
            }
            else
            {
                Logger.Log("Nothing to stop.");
            }
        }

        public void Stop()
        {
            Logger.Log("Stop");
            
            if (IsPlaying)
            {
                FadeOut(MainTrackSampler, FadeOutDuration);
                AudioPlaybackEngine.Stop(MainTrackSampler);                
                IsPlaying = false;

                Logger.Log($"Stopped {FileName}");
            }
            else
            {
                Logger.Log("Nothing to stop.");
            }
        }

        public void Pause()
        {
            Logger.Log("Pause");

            AudioPlaybackEngine.Pause();
        }

        public void Resume()
        {
            Logger.Log("Resume");

            AudioPlaybackEngine.Resume();
        }

        private void FadeIn(LateInitAudioFileReader soundSampleProvider, float duration, float targetVolume)
        {
            if (IsPlaying)
            {
                Logger.Log($"FadeIn {soundSampleProvider.FileName}");
                float fadeoutSleepStep = 50;
                float fadeoutDuration = duration * 1000;
                if (fadeoutDuration <= fadeoutSleepStep)
                {
                    return;
                }

                int iterations = Convert.ToInt32(fadeoutDuration / fadeoutSleepStep);
                float volumeDownStep = targetVolume / iterations;

                for (int i = 0; i < iterations; i++)
                {
                    soundSampleProvider.Volume += volumeDownStep;
                    Thread.Sleep((int)fadeoutSleepStep);
                }
            }
            else
            {
                Logger.Log("Nothing to fade in.");
            }
        }

        private void FadeOut(LateInitAudioFileReader soundSampleProvider, float duration)
        {
            if (IsPlaying)
            {
                Logger.Log($"FadeOut {soundSampleProvider.FileName}");
                float fadeoutSleepStep = 50;
                float fadeoutDuration = duration * 1000;
                if (fadeoutDuration <= fadeoutSleepStep)
                {
                    return;
                }

                int iterations = Convert.ToInt32(fadeoutDuration / fadeoutSleepStep);
                float volumeDownStep = soundSampleProvider.Volume / iterations;

                for (int i = 0; i < iterations; i++)
                {
                    soundSampleProvider.Volume -= volumeDownStep;
                    Thread.Sleep((int)fadeoutSleepStep);
                }
            }
            else
            {
                Logger.Log("Nothing to fade out.");
            }
        }

        public void SetPosition(int positionInMilliSeconds)
        {
            double positionInSeconds = positionInMilliSeconds / 1000.0f;
            var p = positionInSeconds * 44100 * 2;
            var position = Convert.ToInt32(Math.Round(p / 2, MidpointRounding.AwayFromZero) * 2);
            MainTrackSampler.Position = position;
        }

        public float GetProgress()
        {
            return Convert.ToSingle(MainTrackSampler.Position) / MainTrackSampler.Length;
        }

        private void AudioPlaybackEngine_PlaybackCompleted(object sender, DisposableSampleProviderEventArgs e)
        {
            PlaybackCompleted?.Invoke(this, e);
        }

        public void Dispose()
        {
            Stop();
        }

        internal void SetPlaybackSettings(int playerId, PlaybackSettings playbackSettings)
        {
            switch (playerId)
            {
                case 0:
                    TrackSampler1.SetPlaybackSettings(playbackSettings);
                    break;
                case 1:
                    TrackSampler2.SetPlaybackSettings(playbackSettings);
                    break;
            }
        }
    }
}
