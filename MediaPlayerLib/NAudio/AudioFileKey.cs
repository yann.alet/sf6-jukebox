﻿using System;
using System.Collections.Generic;

namespace MediaPlayerLib.NAudio
{
    public struct AudioFileKey
    {
        public AudioFileKey(string filePath, TimeSpan startupSilence, TimeSpan startupTrimming, TimeSpan endTrimming)
        {
            FilePath = filePath;
            StartupSilence = startupSilence;
            StartupTrimming = startupTrimming;
            EndTrimming = endTrimming;
        }

        public string FilePath { get; }
        public TimeSpan StartupSilence { get; }
        public TimeSpan StartupTrimming { get; }
        public TimeSpan EndTrimming { get; }

        public override bool Equals(object obj)
        {
            var key = (AudioFileKey)obj;
            bool isEqual = true;
            isEqual &= FilePath == key.FilePath;
            isEqual &= StartupSilence == key.StartupSilence;
            isEqual &= StartupTrimming == key.StartupTrimming;
            isEqual &= EndTrimming == key.EndTrimming;
            return isEqual;
        }

        public override int GetHashCode()
        {
            int hashCode = -1636013633;
            hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(FilePath);
            hashCode = hashCode * -1521134295 + StartupSilence.GetHashCode();
            hashCode = hashCode * -1521134295 + StartupTrimming.GetHashCode();
            hashCode = hashCode * -1521134295 + EndTrimming.GetHashCode();
            return hashCode;
        }
    }
}
