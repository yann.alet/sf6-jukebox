﻿namespace MediaPlayerLib.NAudio
{
    public struct AudioFileData
    {
        public AudioFileData(float[] data)
        {
            Data = data;
        }

        public float[] Data { get; }
    }
}
