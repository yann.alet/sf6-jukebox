﻿using MediaPlayerDAL.Model;
using MediaPlayerLib.NAudio.Contract;
using MediaPlayerLib.Network.Contract;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;
using System;

namespace MediaPlayerLib.NAudio
{
    public class LateInitAudioFileReader : IDisposableSampleProvider
    {
        public class OnReadInfo
        {
            public OnReadInfo(long position, long length)
            {
                Position = position;
                Length = length;
            }

            public long Length { get; }
            public long Position { get; }
        }

        public event EventHandler<OnReadInfo> OnRead;
        public event EventHandler<SampleProviderEventArgs> PlaybackCompleted;
        private bool _isDisposed;
        private AudioFile _audioFile;
        private float _gainMultiplier = 2.0f;
        private string FileReaderId;

        public float[] AudioData => _audioFile.AudioData;
        
        public LateInitAudioFileReader(ILogger logger)
        {
            FileReaderId = Guid.NewGuid().ToString();
            Logger = logger;            
        }

        public void Init(string path, PlaybackSettings playbackSettings)
        {
            Logger.Log($"Init {path}");
            SetPlaybackSettings(playbackSettings);
            var startupSilence = TimeSpan.Zero;
            var startupTrimming = TimeSpan.Zero;
            var endTrimming = TimeSpan.Zero;
            if (playbackSettings != null)
            {
                if (playbackSettings.StartupSilence.HasValue)
                {
                    startupSilence = playbackSettings.StartupSilence.Value;
                }
                if(playbackSettings.StartupTrimming.HasValue)
                {
                    startupTrimming = playbackSettings.StartupTrimming.Value;
                }
                if(playbackSettings.EndTrimming.HasValue)
                {
                    endTrimming = playbackSettings.EndTrimming.Value;
                }
            }

            _audioFile = new AudioFile(Logger, path, startupSilence, startupTrimming, endTrimming);
            FileName = path;
            FirstSampleRead = false;
        }

        public WaveFormat WaveFormat => _audioFile.WaveFormat;

        private bool FirstSampleRead { get; set; }


        public int Read(float[] buffer, int offset, int count)
        {
            var availableSamples = _audioFile.AudioData.Length - Position;
            var samplesToCopy = Math.Min(availableSamples, count);
            int destOffset = offset;
            for (int sourceSample = 0; sourceSample < samplesToCopy; sourceSample += 2)
            {
                _audioFile.Sample((int)Position + sourceSample, out float outL, out float outR);
                buffer[destOffset + 0] = outL * Volume * Gain;
                buffer[destOffset + 1] = outR * Volume * Gain;
                destOffset += 2;
                if(!FirstSampleRead)
                {
                    Logger.Log("Playing...");
                    FirstSampleRead = true;
                }
            }
            
            Position += samplesToCopy;
            OnRead?.Invoke(this, new OnReadInfo(Position, _audioFile.Length));

            if (Position >= _audioFile.Length)
            {
                if (IsInfinite)
                {
                    for (int i = 0; i < buffer.Length; i++)
                    {
                        buffer[i] = 0;
                    }

                    samplesToCopy = count;
                }
                else if(_audioFile.IsLoaded)
                {
                    PlaybackCompleted?.Invoke(this, new SampleProviderEventArgs(this));
                }
            }
            
            return (int)samplesToCopy;
        }

        public void Dispose()
        {
            _isDisposed = true;         
        }

        internal void SetPlaybackSettings(PlaybackSettings playbackSettings)
        {
            if (playbackSettings.Gain.HasValue)
            {
                Gain = (1 + playbackSettings.Gain.Value / 100) * _gainMultiplier;
            }
            if (playbackSettings.Volume.HasValue)
            {
                Volume = playbackSettings.Volume.Value;
            }      
            if(playbackSettings.IsInfinite.HasValue)
            {
                IsInfinite = playbackSettings.IsInfinite.Value;
            }
        }

        public float Volume { get; set; }

        public float Gain { get; set; } = 1.0f;

        public long Position { get; set; }

        public bool IsInfinite { get; set; }

        public long Length
        {
            get => _audioFile.Length;
        }

        public string FileName { get; private set; }
        public ILogger Logger { get; }
    }
}
