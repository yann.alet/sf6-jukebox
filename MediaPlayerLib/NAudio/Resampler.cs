﻿using MediaPlayerLib.Network.Contract;
using NAudio.Lame;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace MediaPlayerLib.NAudio
{
    public class Resampler
    {
        public Resampler(ILogger logger, string backupFolder, int targetSampleRate)
        {
            Logger = logger;
            BackupFolder = backupFolder;
            TargetSampleRate = targetSampleRate;
        }

        public ILogger Logger { get; }
        public string BackupFolder { get; }
        public int TargetSampleRate { get; }

        public void Resample(List<string> files, Action<string, float> onProgressCallback = null)
        {
            int i = 0;
            Semaphore semaphore = new Semaphore(1, 1);
            Parallel.ForEach(files, file =>
            {
                string fileName = Path.GetFileName(file);
                Logger.Log($"Starting with {fileName}");
                var sourceTrackTagFile = TagLib.File.Create(file);

                string sourceFile = Path.Combine(BackupFolder, Path.GetFileName(file));
                if (File.Exists(sourceFile))
                {
                    File.Delete(sourceFile);
                }

                File.Copy(file, sourceFile);
                File.Delete(file);
                // not thread safe, so try as many times as necessary...
                // temporary until i find a thread safe lib.

                bool success = false;
                while (!success)
                {
                    try
                    {
                        using (var reader = new Mp3FileReader(sourceFile))
                        {
                            var outFormat = new WaveFormat(TargetSampleRate, reader.WaveFormat.Channels);
                            using (var resampler = new MediaFoundationResampler(reader, outFormat))
                            {
                                string outFile = Path.GetTempFileName();
                                WaveFileWriter.CreateWaveFile(outFile, resampler);
                                var bytes = File.ReadAllBytes(outFile);
                                File.Delete(outFile);
                                using (var retMs = new MemoryStream())
                                using (var ms = new MemoryStream(bytes))
                                using (var rdr = new WaveFileReader(ms))
                                using (var wtr = new LameMP3FileWriter(retMs, rdr.WaveFormat, 128))
                                {
                                    rdr.CopyTo(wtr);
                                    File.WriteAllBytes(file, retMs.ToArray());
                                }
                                success = true;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Logger.Log($"Exception while processing {fileName}:\r\n{ex.ToString()}");
                    }
                }
                
                var resampledTrackTagFile = TagLib.File.Create(file);
#pragma warning disable CS0618 // Type or member is obsolete
                TagLib.Tag.Duplicate(sourceTrackTagFile.Tag, resampledTrackTagFile.Tag, true);
#pragma warning restore CS0618 // Type or member is obsolete
                resampledTrackTagFile.Save();

                Thread.Sleep(1000);
                Logger.Log($"Done with {fileName}");
                semaphore.WaitOne();
                Logger.Log($"Unlocking for {fileName}");
                if (onProgressCallback != null)
                {
                    float progress = ++i / Convert.ToSingle(files.Count);
                    onProgressCallback(file, progress);
                    Logger.Log($"Resampling {fileName} - i: {i} - progress: {progress}");
                }
                semaphore.Release();
            });
        }

        public List<string> GetFilesWithInvalidSampleRate(List<string> files, Action<string, float> onProgressCallback = null)
        {
            List<string> results = new List<string>();
            int i = 0;
            foreach (string file in files)
            {
                i++;
                if (File.Exists(file))
                {
                    if (!HasValidSampleRate(file))
                    {
                        results.Add(file);
                    }
                }

                if (onProgressCallback != null)
                {
                    onProgressCallback(file, i / Convert.ToSingle(files.Count));
                }                
            }

            return results;
        }

        public bool HasValidSampleRate(string file)
        {
            bool result;
            var audioFileRead = new AudioFileReader(file);
            result = audioFileRead.WaveFormat.SampleRate == TargetSampleRate;
            audioFileRead.Dispose();
            return result;
        }
    }
}
