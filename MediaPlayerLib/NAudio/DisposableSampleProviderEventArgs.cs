﻿using MediaPlayerLib.NAudio.Contract;
using NAudio.Wave;
using NAudio.Wave.SampleProviders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaPlayerLib.NAudio
{
    public class DisposableSampleProviderEventArgs : SampleProviderEventArgs
    {
        public IDisposableSampleProvider Source { get; }
        public DisposableSampleProviderEventArgs(IDisposableSampleProvider sampleProvider) : base(sampleProvider)
        {
            Source = sampleProvider;
        }
    }
}
