﻿using MediaPlayerLib.Network.Contract;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace MediaPlayerLib.NAudio
{
    public class AudioFile
    {
        public static List<Tuple<AudioFileKey, AudioFileData>> Cache { get; }
        public float[] AudioData { get; private set; }
        public WaveFormat WaveFormat { get; private set; }
        public ILogger Logger { get; }
        public string AudioFileName { get; }
        public TimeSpan StartupSilence { get; }
        public TimeSpan StartupTrimming { get; }
        public TimeSpan EndTrimming { get; }
        public long Length { get; set; }
        private AudioFileReader AudioFileReader { get; set; }
        private List<float> WholeData { get; set; }
        public bool IsLoaded { get; set; }
        public string InstanceId { get; private set; }

        static AudioFile()
        {
            Cache = new List<Tuple<AudioFileKey, AudioFileData>>();
        }        

        public AudioFile(ILogger logger, string audioFileName, bool loadAsync = true)
            :this(logger, audioFileName, TimeSpan.Zero, TimeSpan.Zero, TimeSpan.Zero, loadAsync)
        {
        }

        public AudioFile(ILogger logger, string audioFileName, TimeSpan startupSilence, TimeSpan startupTrimming, TimeSpan endTrimming, bool loadAsync = true)
        {
            InstanceId = Guid.NewGuid().ToString();
            WholeData = new List<float>();
            AudioFileReader = new AudioFileReader(audioFileName);
            Logger = logger;
            AudioFileName = audioFileName;
            StartupSilence = startupSilence;
            StartupTrimming = startupTrimming;
            EndTrimming = endTrimming;
            WaveFormat = AudioFileReader.WaveFormat;
            AudioData = new float[GetDurationDataSize(startupSilence) + AudioFileReader.Length/4 - GetDurationDataSize(startupTrimming) - GetDurationDataSize(endTrimming)];
            AudioDataAccess = new Semaphore(1, 1);
            var key = new AudioFileKey(audioFileName, startupSilence, startupTrimming, endTrimming);
            var cached = Cache.FirstOrDefault(x => x.Item1.Equals(key));
            if (cached != null)
            {
                AudioData = cached.Item2.Data;
                WholeData = new List<float>(AudioData);
                Length = AudioData.Length;
                IsLoaded = true;
            }
            else
            {
                if (loadAsync)
                {
                    Task.Run(Load);
                }
                else
                {
                    Load();
                }
            }
        }

        private void Load()
        {
            WriteSilence();
            WriteAudioData();
            AudioDataAccess.WaitOne();
            AudioData = WholeData.ToArray();
            AudioDataAccess.Release();
            Length = AudioData.Length;
            var key = new AudioFileKey(AudioFileName, StartupSilence, StartupTrimming, EndTrimming);
            var data = new AudioFileData(AudioData);
            Cache.Add(new Tuple<AudioFileKey, AudioFileData>(key, data));

            if(Cache.Count > 10)
            {
                Cache.RemoveAt(0);
            }

            IsLoaded = true;
        }

        Semaphore AudioDataAccess { get; set; }

        private void WriteSilence()
        {
            if(StartupSilence > TimeSpan.Zero)
            {
                int silenceDataSize = GetDurationDataSize(StartupSilence);
                var silenceChunk = new float[silenceDataSize];
                for(int  i = 0; i < silenceDataSize; i+= ChunkSize)
                {
                    int size = ChunkSize;
                    if (i + ChunkSize > silenceDataSize)
                    {
                        silenceChunk = silenceChunk.Take(silenceDataSize - i).ToArray();
                        size = silenceDataSize - i;
                    }

                    AudioDataAccess.WaitOne();
                    WholeData.AddRange(silenceChunk.Take(size));
                    AudioDataAccess.Release();
                }
            }
        }

        public void Sample(int index, out float left, out float right)
        {
            AudioDataAccess.WaitOne();
            int count = WholeData.Count;
            AudioDataAccess.Release();
            while (index + 1 > count)
            {
                Logger.Log("Buffering...");
                AudioDataAccess.WaitOne();
                count = WholeData.Count;
                AudioDataAccess.Release();
                Thread.Sleep(10);
            }

            AudioDataAccess.WaitOne();
            left = WholeData[index];
            right = WholeData[index + 1];
            AudioDataAccess.Release();
        }

        public static float[] GetAudioData(string filePath)
        {
            using (var audioFileReader = new AudioFileReader(filePath))
            {
                var readBuffer = new float[audioFileReader.WaveFormat.SampleRate * audioFileReader.WaveFormat.Channels];
                int samplesRead;
                List<float> samples = new List<float>();
                while ((samplesRead = audioFileReader.Read(readBuffer, 0, audioFileReader.WaveFormat.SampleRate)) > 0)
                {
                    samples.AddRange(readBuffer.Take(samplesRead));                    
                }

                return samples.ToArray();
            }
        }

        private void WriteAudioData()
        {
            int startTrimmingDataSize = GetDurationDataSize(StartupTrimming);
            int endTrimmingDataSize = GetDurationDataSize(EndTrimming);
            var readBuffer = new float[AudioFileReader.WaveFormat.SampleRate * AudioFileReader.WaveFormat.Channels];
            int samplesRead;
            while ((samplesRead = AudioFileReader.Read(readBuffer, 0, ChunkSize)) > 0)
            {
                AudioDataAccess.WaitOne();
                WholeData.AddRange(readBuffer.Take(samplesRead));
                AudioDataAccess.Release();
            }
            AudioDataAccess.WaitOne();
            WholeData = WholeData.Skip(startTrimmingDataSize).Take(WholeData.Count - startTrimmingDataSize - endTrimmingDataSize).ToList();
            AudioDataAccess.Release();
        }



        int ChunkSize = 13230;

        int GetDurationDataSize(TimeSpan duration)
        {
            double durationInSeconds = duration.TotalMilliseconds / 1000;
            double size = durationInSeconds * 44100 * 2;
            int arraySize = Convert.ToInt32(Math.Round(size / 2, MidpointRounding.AwayFromZero) * 2);
            return arraySize;
        }

        private float[] GetSilence(TimeSpan duration)
        {
            int arraySize = GetDurationDataSize(duration);
            float[] silence = new float[arraySize];
            return silence;
        }
    }
}
