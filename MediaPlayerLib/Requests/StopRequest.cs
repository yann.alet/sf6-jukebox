﻿namespace MediaPlayerLib.Requests
{
    public class StopRequest : PlaybackRequest
    {
        public StopRequest(string fileName)
        {
            FileName = fileName;
        }

        public StopRequest()
        {
        }

        public override string Name => "Stop";
        public string FileName { get; }
    }
}
