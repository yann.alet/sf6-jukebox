﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaPlayerLib.Requests
{
    public class TransitionRequest : PlaybackRequest
    {
        public TransitionRequest(Track track)
        {
            Track = track;
        }

        public Track Track { get; }

        public override string Name => "Transition";
    }
}
