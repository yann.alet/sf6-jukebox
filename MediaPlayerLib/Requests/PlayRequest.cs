﻿using MediaPlayerDAL;
using MediaPlayerDAL.Model;

namespace MediaPlayerLib.Requests
{
    public class PlayRequest : PlaybackRequest
    {
        public PlayRequest(Track track)
        {
            Track = track;
        }

        public Track Track { get; }
     
        public override string Name
        {
            get
            {
                switch (Track.PlaylistType)
                {
                    case PlaylistType.Stage:
                        return $"Type: {Track.PlaylistType} - Id: {Track.StageId} - {Track.Path}";
                    case PlaylistType.FlowScene:
                        return $"Type: {Track.PlaylistType} - Id: {Track.FlowSceneId} - {Track.Path}";
                    case PlaylistType.CustomScreen:
                        return $"Type: {Track.PlaylistType} - Id: {Track.CustomScreenId} - {Track.Path}";
                    case PlaylistType.Character:
                        return $"Type: {Track.PlaylistType} - Id: {Track.CharacterId} - {Track.Path}";
                    default:
                        return "Unknown";
                }
            }            
        }
    }
}
