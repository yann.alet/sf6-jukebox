﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaPlayerLib.Requests
{
    public class SetPositionRequest : PlaybackRequest
    {
        public SetPositionRequest(int position)
        {
            Position = position;
        }

        public int Position { get; }

        public override string Name => $"Position to {Position}";
    }
}
