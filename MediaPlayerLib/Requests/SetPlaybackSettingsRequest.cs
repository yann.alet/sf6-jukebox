﻿using MediaPlayerDAL.Model;

namespace MediaPlayerLib.Requests
{
    public class SetPlaybackSettingsRequest : PlaybackRequest
    {
        public SetPlaybackSettingsRequest(PlaybackSettings playbackSettings)
        {
            PlaybackSettings = playbackSettings;
        }

        public SetPlaybackSettingsRequest(int playerId, PlaybackSettings playbackSettings)
        {
            PlaybackSettings = playbackSettings;
            PlayerId = playerId;
        }

        public int? PlayerId { get; set; }
        public PlaybackSettings PlaybackSettings { get; }
    }
}
