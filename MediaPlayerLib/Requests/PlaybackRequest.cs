﻿namespace MediaPlayerLib.Requests
{
    public abstract class PlaybackRequest
    {
        public virtual string Name { get; }
    }
}
