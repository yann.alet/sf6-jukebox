﻿using MediaPlayerDAL;
using MediaPlayerDAL.Model;
using MediaPlayerLib.NAudio;
using MediaPlayerLib.Network;
using MediaPlayerLib.Network.Contract;
using MediaPlayerLib.Network.Message;
using MediaPlayerLib.Requests;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;

namespace MediaPlayerLib
{
    public class ThreadedPlayer : IDisposable
    {
        private float volume;
        public event EventHandler<NaudioPlayer.OnReadInfo> OnRead;
        public event EventHandler<int> OnCrossFade;      
        private object _lock = new object();
        private DateTime _lastOverlayDisplay = DateTime.Now;

        public ThreadedPlayer(Settings settings, ILogger logger, bool forceDisabledOverlay = false)
        {
            Logger = logger;
            ForceDisabledOverlay = forceDisabledOverlay;
            Settings = settings;
            Player = new NaudioPlayer(logger, Settings);
            Player.PlaybackCompleted += OutputDevice_PlaybackCompleted;
            Player.OnRead += Player_OnRead;
            Player.OnCrossFade += Player_OnCrossFade;
            PlaybackRequests = new ConcurrentQueue<PlaybackRequest> ();
            OvervalyMessageRequests = new ConcurrentQueue<IMessage> ();
            RequestsProcessorThread = new Thread(Start);
            Volume = settings.Volume;
            CurrentFightState = 0;

            if (Settings.OverlayEnabled == 1 && !ForceDisabledOverlay)
            {
                OverlayServer = new OverlayServer(1212, Logger);
                OverlayServer.Start();
                StartOverlay();
            }

            KeepAlive = true;
            RequestsProcessorThread.Start();
        }

        private void Player_OnCrossFade(object sender, int e)
        {
            OnCrossFade?.Invoke(sender, e);
        }

        NaudioPlayer Player { get; }
        private int OverlayUpdateFrequency => 1000;
        private DateTime LastOverlayUpdate { get; set; } = DateTime.Now;
        private ConcurrentQueue<PlaybackRequest> PlaybackRequests { get; }
        private ConcurrentQueue<IMessage> OvervalyMessageRequests { get; }
        private Thread RequestsProcessorThread { get; }
        public Settings Settings { get; }
        private ILogger Logger { get; }
        public bool ForceDisabledOverlay { get; private set; }

        bool IsOverlayStarted = false;
        private OverlayServer OverlayServer { get; set; }
        PlaylistType CurrentPlaylistType = PlaylistType.None;
        StageId CurrentStageId { get; set; }
        FlowSceneId CurrentFlowSceneId { get; set; }
        CustomScreenId CurrentCustomScreenId { get; set; }
        CharacterId CurrentCharacterId { get; set; }
        Dictionary<StageId, Playlist> StagePlaylists { get; set; }
        Dictionary<FlowSceneId, Playlist> FlowScenePlaylists { get; set; }
        Dictionary<CustomScreenId, Playlist> CustomScreenPlaylists { get; set; }
        Dictionary<CharacterId, Playlist> CharacterPlaylists { get; set; }
        private int CurrentFightState { get; set; }
        private int CurrentGameMode { get; set; }
        private int CurrentFlowScene { get; set; }
        private bool KeepAlive { get; set; }

        public bool IsPlaying => Player.IsPlaying;

        public float Volume
        {
            get => volume;
            set
            {
                volume = value;
                Settings.Volume = value;
                if (Player.IsPlaying)
                {
                    if (!Player.IsCrossFading)
                    {
                        Player.Volume = value;
                    }
                }
            }
        }

        public void Destroy()
        {
            Logger.Log("Begin Destroy");
            lock (_lock)
            {
                KeepAlive = false;
                Monitor.PulseAll(_lock);
            }
            RequestsProcessorThread.Join();
            OverlayServer.Stop();
            Player.Dispose();
            Logger.Log("End Destroy");
        }

        public float[] GetAudioData(int playerId)
        {
            return Player.GetAudioData(playerId);
        }

        public float[] GetAudioData(string path, PlaybackSettings playbackSettings)
        {
            return Player.GetAudioData(path, playbackSettings);
        }

        public void EnableOverlay()
        {
            Logger.Log("EnableOverlay");
            ForceDisabledOverlay = false;
        }

        public void DisableOverlay()
        {
            Logger.Log("DisableOverlay");
            ForceDisabledOverlay = true;
        }

        public void OnFightStateChanged(int fightState)
        {
            CurrentFightState = fightState;
            if (Settings.OverlayEnabled == 1 && !ForceDisabledOverlay)
            {
                lock(_lock)
                {
                    OvervalyMessageRequests.Enqueue(new FightStateChangedMessage(fightState));
                    Monitor.PulseAll(_lock);
                }
            }
        }

        internal void OnGameModeChanged(int gameMode)
        {
            CurrentGameMode = gameMode;
            if (Settings.OverlayEnabled == 1 && !ForceDisabledOverlay)
            {
                lock(_lock)
                {
                    OvervalyMessageRequests.Enqueue(new GameModeChangedMessage(gameMode));
                    Monitor.PulseAll(_lock);
                }
            }
        }

        internal void OnFlowSceneChanged(int flowScene)
        {
            CurrentFlowScene = flowScene;
            if (Settings.OverlayEnabled == 1 && !ForceDisabledOverlay)
            {
                lock (_lock)
                {
                    OvervalyMessageRequests.Enqueue(new FlowSceneChangedMessage(flowScene));
                    Monitor.PulseAll(_lock);
                }                
            }
        }

        private void Start()
        {
            while (KeepAlive)
            {
                lock (_lock)
                {
                    while (PlaybackRequests.Count == 0 && OvervalyMessageRequests.Count == 0 && KeepAlive && (DateTime.Now - _lastOverlayDisplay) > TimeSpan.FromSeconds(5))
                    {
                        Monitor.Wait(_lock);
                    }
                }

                if (Player.IsPlaying)
                {
                    if (!ForceDisabledOverlay && IsOverlayStarted)
                    {
                        SendTrackUpdateMessage(new TrackUpdateMessage() { Track = Player.FileName, Progress = Player.GetProgress() });
                    }
                }

                if (PlaybackRequests.Count > 0)
                {
                    if(PlaybackRequests.TryDequeue(out PlaybackRequest playbackRequest))
                    {
                        var stopRequest = playbackRequest as StopRequest;
                        var playRequest = playbackRequest as PlayRequest;
                        var positionRequest = playbackRequest as SetPositionRequest;
                        var transitionRequest = playbackRequest as TransitionRequest;
                        var playbackSettingsRequest = playbackRequest as SetPlaybackSettingsRequest;
                        var pauseRequest = playbackRequest as PauseRequest;
                        var resumeRequest = playbackRequest as ResumeRequest;

                        if(pauseRequest != null)
                        {
                            PauseInternal();
                        }

                        if(resumeRequest != null)
                        {
                            ResumeInternal();
                        }

                        if (stopRequest != null)
                        {
                            if(stopRequest.FileName != null)
                            {
                                if (Player.FileName == stopRequest.FileName)
                                {
                                    Player.Stop(stopRequest.FileName);
                                }
                            }
                            else
                            {
                                StopInternal(Player);
                            }
                        }
                        
                        if(playRequest != null)
                        {
                            try
                            {
                                PlayInternal(Player, playRequest);
                            }
                            catch (Exception ex)
                            {
                                Logger.Log(ex.ToString());
                            }
                            
                        }

                        if(positionRequest != null)
                        {
                            SetPositionInternal(positionRequest.Position);
                        }

                        if(transitionRequest != null)
                        {
                            Player.CrossFadeWith(transitionRequest.Track.Path, 2, transitionRequest.Track.PlaybackSettings);
                        }

                        if(playbackSettingsRequest != null)
                        {
                            if (playbackSettingsRequest.PlayerId.HasValue)
                            {
                                Player.SetPlaybackSettings(playbackSettingsRequest.PlayerId.Value, playbackSettingsRequest.PlaybackSettings);
                            }
                            else
                            {
                                if (playbackSettingsRequest.PlaybackSettings.Volume.HasValue)
                                {
                                    Volume = playbackSettingsRequest.PlaybackSettings.Volume.Value;
                                }
                            }
                        }

                        // Giving NAudio some breathing room...
                        if (playbackSettingsRequest == null && positionRequest == null)
                        {
                            Thread.Sleep(100);
                        }
                    }
                }

                if(OvervalyMessageRequests.Count > 0)
                {
                    if(OvervalyMessageRequests.TryDequeue(out IMessage message))
                    {
                        OverlayServer.Send(message);
                    }
                }
            }

            Logger.Log("RequestsProcessorThread stopped.");
        }

        private void Player_OnRead(object sender, NaudioPlayer.OnReadInfo e)
        {
            OnRead?.Invoke(sender, e);
        }

        public void QueueInStageNextTrack()
        {
            Logger.Log("QueueInStageNextTrack");
            StagePlaylists[CurrentStageId].QueueInNextTrack();
        }

        public void QueuePlaybackRequest(PlaybackRequest request)
        {
            lock (_lock)
            {
                PlaybackRequests.Enqueue(request);
                Monitor.PulseAll(_lock);
            }
        }

        public void SetPosition(int position)
        {
            QueuePlaybackRequest(new SetPositionRequest(position));
        }     
        
        public void Pause()
        {
            QueuePlaybackRequest(new  PauseRequest());
        }

        public void Resume()
        {
            QueuePlaybackRequest(new ResumeRequest());
        }

        private void SetPositionInternal(int position)
        {
            if (Player.IsPlaying)
            {
                Player.SetPosition(position);
            }
        }

        private Playlist GetCurrentPlaylist()
        {
            switch (CurrentPlaylistType)
            {
                case PlaylistType.Stage:
                    return StagePlaylists[CurrentStageId];
                case PlaylistType.FlowScene:
                    return FlowScenePlaylists[CurrentFlowSceneId];
                case PlaylistType.CustomScreen:
                    return CustomScreenPlaylists[CurrentCustomScreenId];
                case PlaylistType.Character:
                    return CharacterPlaylists[CurrentCharacterId];
                default:
                    return null;
            }
        }     

        private void OutputDevice_PlaybackCompleted(object sender, DisposableSampleProviderEventArgs e)
        {
            Logger.Log($"OutputDevice_PlaybackCompleted");
            var playlist = GetCurrentPlaylist();
            if (playlist != null)
            {
                var currentPlayMode = playlist.InnerPlaylist.Mode;
                switch (currentPlayMode)
                {
                    case PlayMode.Repeat:
                        PlayNextTrack();
                        break;
                    case PlayMode.Once:
                        QueuePlaybackRequest(new StopRequest(e.Source.FileName));
                        if (!GetCurrentPlaylist().IsLastTrack())
                        {
                            PlayNextTrack();
                        }
                        break;
                    case PlayMode.SingleRepeat:
                        switch (CurrentPlaylistType)
                        {
                            case PlaylistType.Stage:
                                QueuePlaybackRequest(new PlayRequest(new Track(Player.FileName, CurrentStageId)));
                                break;
                            case PlaylistType.FlowScene:
                                QueuePlaybackRequest(new PlayRequest(new Track(Player.FileName, CurrentFlowSceneId)));
                                break;
                            case PlaylistType.CustomScreen:
                                QueuePlaybackRequest(new PlayRequest(new Track(Player.FileName, CurrentCustomScreenId)));
                                break;
                            case PlaylistType.Character:
                                QueuePlaybackRequest(new PlayRequest(new Track(Player.FileName, CurrentCharacterId)));
                                break;
                        }
                        break;
                    case PlayMode.SingleOnce:
                        QueuePlaybackRequest(new StopRequest(e.Source.FileName));
                        break;
                    default:
                        break;
                }
            }
        }

        public void PlayNextTrack()
        {
            Logger.Log($"PlayNextTrack for {CurrentPlaylistType}");

            QueuePlaybackRequest(new StopRequest());
            switch (CurrentPlaylistType)
            {
                case PlaylistType.Stage:
                    QueuePlaybackRequest(new PlayRequest(new Track(GetCurrentPlaylist().GetNextTrack(), CurrentStageId)));
                    break;
                case PlaylistType.FlowScene:
                    QueuePlaybackRequest(new PlayRequest(new Track(GetCurrentPlaylist().GetNextTrack(), CurrentFlowSceneId)));
                    break;
                case PlaylistType.CustomScreen:
                    QueuePlaybackRequest(new PlayRequest(new Track(GetCurrentPlaylist().GetNextTrack(), CurrentCustomScreenId)));
                    break;
                case PlaylistType.Character:
                    QueuePlaybackRequest(new PlayRequest(new Track(GetCurrentPlaylist().GetNextTrack(), CurrentCharacterId)));
                    break;
            }
        }

        public void PlayPreviousTrack()
        {
            Logger.Log($"PlayPreviousTrack for {CurrentPlaylistType}");

            QueuePlaybackRequest(new StopRequest());
            switch (CurrentPlaylistType)
            {
                case PlaylistType.Stage:
                    QueuePlaybackRequest(new PlayRequest(new Track(GetCurrentPlaylist().GetPreviousTrack(), CurrentStageId)));
                    break;
                case PlaylistType.FlowScene:
                    QueuePlaybackRequest(new PlayRequest(new Track(GetCurrentPlaylist().GetPreviousTrack(), CurrentFlowSceneId)));
                    break;
                case PlaylistType.CustomScreen:
                    QueuePlaybackRequest(new PlayRequest(new Track(GetCurrentPlaylist().GetPreviousTrack(), CurrentCustomScreenId)));
                    break;
                case PlaylistType.Character:
                    QueuePlaybackRequest(new PlayRequest(new Track(GetCurrentPlaylist().GetPreviousTrack(), CurrentCharacterId)));
                    break;
            }
        }

        public int Play(Track track)
        {
            track.PlaybackSettings.Volume = Volume;
            QueuePlaybackRequest(new PlayRequest(track));
            return Player.GetPlayerId();
        }

        public int Stop()
        {
            QueuePlaybackRequest(new StopRequest());
            return Player.GetPlayerId();
        }

        public int Transition(Track track)
        {
            QueuePlaybackRequest(new TransitionRequest(track));
            return Player.GetTransitionPlayerId();
        }

        public void SetPlaybackSettings(PlaybackSettings settings)
        {
            QueuePlaybackRequest(new SetPlaybackSettingsRequest(settings));
        }

        public void SetPlaybackSettings(int playerId, PlaybackSettings settings)
        {
            QueuePlaybackRequest(new SetPlaybackSettingsRequest(playerId, settings));
        }

        public void OnRoundChanged(int round)
        {
            Logger.Log($"OnRoundChanged {round}");
            var playlist = GetCurrentPlaylist();
            if(playlist == null)
            {
                return;
            }    

            var track = playlist.GetCurrentTrack();
            if (track != null)
            {
                if (round == 0)
                {
                    if (Player.MainTrackSampler.FileName != track.Path)
                    {
                        QueuePlaybackRequest(new TransitionRequest(new Track(track.Path, track.PlaybackSettings)));
                    }
                }
                else
                {
                    var transition = track.Transitions.FirstOrDefault(t => t.TransitionType == GetRoundTransitionType(round));
                    if (transition != null && transition.Tracks.Any())
                    {
                        var transitionTrack = transition.Tracks.First();
                        if (Player.MainTrackSampler.FileName != transitionTrack.Path)
                        {
                            QueuePlaybackRequest(new TransitionRequest(new Track(transitionTrack.Path, transitionTrack.PlaybackSettings)));
                        }
                    }
                    else
                    {
                        transition = track.Transitions.FirstOrDefault(t => t.TransitionType == TransitionType.Round1);
                        if (transition != null && transition.Tracks.Any() && Player.FileName != transition.Tracks.First().Path)
                        {
                            var transitionTrack = transition.Tracks.First();
                            if (Player.MainTrackSampler.FileName != transitionTrack.Path)
                            {
                                QueuePlaybackRequest(new TransitionRequest(new Track(transitionTrack.Path, transitionTrack.PlaybackSettings)));
                            }
                        }
                    }
                }
            }
        }

        private TransitionType GetRoundTransitionType(int round)
        {
            switch(round)
            {
                case 0:
                    return TransitionType.RoundInit;
                case 1: 
                    return TransitionType.Round1;
                case 2:
                    return TransitionType.Round2;
                case 3:
                    return TransitionType.Round3;
                default:
                    return TransitionType.Round1;
            }
        }

        public void OnDanger()
        {
            Logger.Log($"OnDanger");
            var playlist = GetCurrentPlaylist();
            if(playlist == null)
            {
                return;
            }

            var track = playlist.GetCurrentTrack();
            if (track != null)
            {
                var transition = track.Transitions.FirstOrDefault(t => t.TransitionType == TransitionType.Danger);
                if (transition != null && transition.Tracks.Any())
                {
                    QueuePlaybackRequest(new TransitionRequest(new Track(transition.Tracks.First().Path, transition.Tracks.First().PlaybackSettings)));
                }
            }
        }

        public void PlayCharacterTrack(int characterId, int round)
        {
            if (CharacterPlaylists.ContainsKey((CharacterId)characterId))
            {
                Logger.Log($"Play for character {(CharacterId)characterId} - round {round}");
                if (CharacterPlaylists[(CharacterId)characterId].InnerPlaylist.Tracks.Any())
                {
                    var playlist = CharacterPlaylists[(CharacterId)characterId];
                    var track = CharacterPlaylists[(CharacterId)characterId].GetNextTrack();
                    if (track != null)
                    {
                        var transition = track.Transitions.FirstOrDefault(t => t.TransitionType == (TransitionType)round);
                        if (transition != null && transition.Tracks.Any())
                        {
                            Play(new Track(transition.Tracks.First().Path, (CharacterId)characterId));
                        }
                        else
                        {
                            Play(new Track(track, (CharacterId)characterId));
                        }
                    }                    
                }
                else
                {
                    Logger.Log($"Aborting play for {(CharacterId)characterId}, not track found.");
                }
            }
            else
            {
                Logger.Log($"Playlist for character id {(CharacterId)characterId} not found.");
            }
        }

        public void PlayStageTrack(int stageId)
        {
            if (StagePlaylists.ContainsKey((StageId)stageId))
            {
                Logger.Log($"Play for stage {(StageId)stageId}");
                if (StagePlaylists[(StageId)stageId].InnerPlaylist.Tracks.Any())
                {
                    Play(new Track(StagePlaylists[(StageId)stageId].GetNextTrack(), (StageId)stageId));
                }
                else
                {
                    Logger.Log($"Aborting play for {(StageId)stageId}, not track found.");
                }
            }
            else
            {
                Logger.Log($"Playlist for stage id {(StageId)stageId} not found.");
            }
        }

        public void PlayFlowSceneTrack(int flowSceneId)
        {
            if (FlowScenePlaylists.ContainsKey((FlowSceneId)flowSceneId))
            {
                Logger.Log($"Play for flowscene {(FlowSceneId)flowSceneId}");
                if (FlowScenePlaylists[(FlowSceneId)flowSceneId].InnerPlaylist.Tracks.Any())
                {
                    Play(new Track(FlowScenePlaylists[(FlowSceneId)flowSceneId].GetNextTrack(), (FlowSceneId)flowSceneId));
                }
                else
                {
                    Logger.Log($"Aborting play for {(FlowSceneId)flowSceneId}, not track found.");
                }
            }
            else
            {
                Logger.Log($"Playlist for flowscene id {(FlowSceneId)flowSceneId} not found.");
            }
        }

        public void PlayCustomScreenTrack(int customScreenId)
        {
            if (CustomScreenPlaylists.ContainsKey((CustomScreenId)customScreenId))
            {
                Logger.Log($"Play for custom screen {(CustomScreenId)customScreenId}");
                if (CustomScreenPlaylists[(CustomScreenId)customScreenId].InnerPlaylist.Tracks.Any())
                {
                    Play(new Track(CustomScreenPlaylists[(CustomScreenId)customScreenId].GetNextTrack(), (CustomScreenId)customScreenId));
                }
                else
                {
                    Logger.Log($"Aborting play for {(CustomScreenId)customScreenId}, not track found.");
                }
            }
            else
            {
                Logger.Log($"Playlist for custom screen id {(CustomScreenId)customScreenId} not found.");
            }
        }

        private void PlayInternal(NaudioPlayer player, PlayRequest playRequest)
        {
            if (string.IsNullOrEmpty(playRequest.Track.Path))
            {
                Logger.Log("Aborting playback, provided path was empty.");
                return;
            }
            else if (!File.Exists(playRequest.Track.Path))
            {
                Logger.Log($"Aborting playback, file does not exist: {playRequest.Track.Path}");
                return;
            }

            if (player.IsPlaying)
            {
                StopInternal(player);
            }

            if (playRequest.Track.Path != String.Empty || !File.Exists(playRequest.Track.Path))
            {
                CurrentPlaylistType = playRequest.Track.PlaylistType;
                switch (playRequest.Track.PlaylistType)
                {
                    case PlaylistType.Stage:
                        CurrentStageId = playRequest.Track.StageId;
                        break;

                    case PlaylistType.FlowScene:
                        CurrentFlowSceneId = playRequest.Track.FlowSceneId;
                        break;

                    case PlaylistType.CustomScreen:
                        CurrentCustomScreenId = playRequest.Track.CustomScreenId;
                        break;

                    case PlaylistType.Character:
                        CurrentCharacterId = playRequest.Track.CharacterId;
                        break;
                }                

                player.FadeOutDuration = GetFadeOutDuration();
                player.Play(playRequest.Track.Path, playRequest.Track.PlaybackSettings);

                SendTrackUpdateMessage(new TrackUpdateMessage { Track = playRequest.Track.Path, Progress = 0 }, true);
            }
            else
            {
                Logger.Log("PlayInternal - No track to play.");
            }
        }

        private void StopInternal(NaudioPlayer player)
        {
            player.Stop();
        }

        private void PauseInternal()
        {
            Player.Pause();
        }

        private void ResumeInternal()
        {
            Player.Resume();
        }

        private float GetFadeOutDuration()
        {
            float result = 0;
            if (CurrentPlaylistType != PlaylistType.None)
            {
                var currentPlaylist = GetCurrentPlaylist();

                if (currentPlaylist != null)
                {
                    result = currentPlaylist.InnerPlaylist.FadeOutDuration;
                }
            }

            return result;
        }

        private void StartOverlay()
        {
            if (Settings.OverlayEnabled == 1)
            {
                Logger.Log("Starting Overlay");
                var refDir = Directory.GetParent(Assembly.GetExecutingAssembly().Location);
                var overlayPath = Path.Combine(refDir.Parent.FullName, @"SF6JukeboxOverlay\SF6JukeboxOverlay.exe");
                Process.Start(new ProcessStartInfo(overlayPath));
                IsOverlayStarted = true;
            }
        }

        public void ShowOverlay()
        {
            if (Settings.OverlayEnabled == 1 && !ForceDisabledOverlay)
            {
                lock (_lock)
                {
                    OvervalyMessageRequests.Enqueue(new ShowOverlayMessage());
                    _lastOverlayDisplay = DateTime.Now;
                    Monitor.PulseAll(_lock);
                }
            }
        }

        private void SendTrackUpdateMessage(TrackUpdateMessage trackUpdateMessage, bool force = false)
        {
            if(ForceDisabledOverlay)
            {
                return;
            }

            TimeSpan timeElapsedSinceLastUpdate = DateTime.Now - LastOverlayUpdate;
            if(timeElapsedSinceLastUpdate < TimeSpan.FromMilliseconds(OverlayUpdateFrequency) && !force)
            {
                return;
            }

            if (Settings.OverlayEnabled == 1 && !ForceDisabledOverlay)
            {
                lock (_lock)
                {
                    Logger.Log($"Enqueue TrackUpdateMessage {trackUpdateMessage.Track} {trackUpdateMessage.Progress}");
                    OvervalyMessageRequests.Enqueue(new TrackUpdateMessage { Track = trackUpdateMessage.Track, Progress = trackUpdateMessage.Progress });
                    LastOverlayUpdate = DateTime.Now;
                    Monitor.PulseAll(_lock);
                }
            }
        }

        public float GetProgress()
        {
            return IsPlaying ? Player.GetProgress() : 0;
        }

        public void ResetFlowScenePlaylist()
        {
            Logger.Log("ResetFlowScenePlaylist");

            try
            {
                FlowScenePlaylists = new Dictionary<FlowSceneId, Playlist>();
                foreach (var flowScene in Settings.FlowScenes)
                {
                    if (flowScene.IsRandomPlaylist == 1)
                    {
                        FlowScenePlaylists[flowScene.FlowSceneId] = new Playlist(new MediaPlayerDAL.Model.Playlist(Directory.GetFiles(Settings.RandomPlaylistPath, "*.mp3", SearchOption.AllDirectories).ToList())) { Id = flowScene.Id }; ;
                    }
                    else
                    {
                        if (!FlowScenePlaylists.ContainsKey(flowScene.FlowSceneId))
                        {
                            FlowScenePlaylists[flowScene.FlowSceneId] = new Playlist(flowScene.Playlist) { Id = (int)flowScene.Id };
                        }
                    }

                    FlowScenePlaylists[flowScene.FlowSceneId].Shuffle();
                    LogPlaylist(FlowScenePlaylists[flowScene.FlowSceneId]);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.ToString());
            }
        }

        public void ResetStagePlaylist()
        {
            Logger.Log("ResetStagePlaylist");

            try
            {
                StagePlaylists = new Dictionary<StageId, Playlist>();
                foreach (var customScreen in Settings.Stages)
                {
                    if (customScreen.IsRandomPlaylist == 1)
                    {
                        StagePlaylists[customScreen.StageId] = new Playlist(new MediaPlayerDAL.Model.Playlist(Directory.GetFiles(Settings.RandomPlaylistPath, "*.mp3", SearchOption.AllDirectories).ToList())) { Id = customScreen.Id }; ;
                    }
                    else
                    {
                        if (!StagePlaylists.ContainsKey(customScreen.StageId))
                        {
                            StagePlaylists[customScreen.StageId] = new Playlist(customScreen.Playlist) { Id = customScreen.Id };
                        }
                    }

                    StagePlaylists[customScreen.StageId].Shuffle();
                    LogPlaylist(StagePlaylists[customScreen.StageId]);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.ToString());
            }
        }

        public void ResetCharacterPlaylist()
        {
            Logger.Log("ResetCharacterPlaylist");

            try
            {
                CharacterPlaylists = new Dictionary<CharacterId, Playlist>();
                foreach (var character in Settings.Characters)
                {
                    if (character.IsRandomPlaylist == 1)
                    {
                        CharacterPlaylists[character.CharacterId] = new Playlist(new MediaPlayerDAL.Model.Playlist(Directory.GetFiles(Settings.RandomPlaylistPath, "*.mp3", SearchOption.AllDirectories).ToList())) { Id = character.Id }; ;
                    }
                    else
                    {
                        if (!CharacterPlaylists.ContainsKey(character.CharacterId))
                        {
                            CharacterPlaylists[character.CharacterId] = new Playlist(character.Playlist) { Id = character.Id };
                        }
                    }

                    CharacterPlaylists[character.CharacterId].Shuffle();
                    LogPlaylist(CharacterPlaylists[character.CharacterId]);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.ToString());
            }
        }

        public void ResetCustomScreensPlaylist()
        {
            Logger.Log("ResetCustomScreensPlaylist");

            try
            {
                CustomScreenPlaylists = new Dictionary<CustomScreenId, Playlist>();
                foreach (var customScreen in Settings.CustomScreens)
                {
                    if (customScreen.IsRandomPlaylist == 1)
                    {
                        CustomScreenPlaylists[customScreen.CustomScreenId] = new Playlist(new MediaPlayerDAL.Model.Playlist(Directory.GetFiles(Settings.RandomPlaylistPath, "*.mp3", SearchOption.AllDirectories).ToList())) { Id = customScreen.Id }; ;
                    }
                    else
                    {
                        if (!CustomScreenPlaylists.ContainsKey(customScreen.CustomScreenId))
                        {
                            CustomScreenPlaylists[customScreen.CustomScreenId] = new Playlist(customScreen.Playlist) { Id = customScreen.Id };
                        }
                    }

                    CustomScreenPlaylists[customScreen.CustomScreenId].Shuffle();
                    LogPlaylist(CustomScreenPlaylists[customScreen.CustomScreenId]);
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.ToString());
            }
        }


        public void LogPlaylist(Playlist playlist)
        {
            Logger.Log($"-- Dumping playlist with id {playlist.Id} -- ");
            for (int i = 0; i < playlist.InnerPlaylist.Tracks.Count; i++)
            {
                Logger.Log(String.Format("{0}{1}", i == playlist.CurrentTrackIndex ? "-> " : String.Empty, playlist.InnerPlaylist.Tracks[i].Path));
            }
            Logger.Log($"------------------------------------------------------------");
        }

        public void Dispose()
        {
            lock (_lock)
            {
                KeepAlive = false;
                Monitor.PulseAll(_lock);
            }            
            Player.Dispose();
        }
    }
}
