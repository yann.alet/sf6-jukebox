﻿using MediaPlayerDAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaPlayerLib
{
    public class Playlist
    {
        public Playlist(MediaPlayerDAL.Model.Playlist innerPlaylist) 
        {
            //Tracks = new List<string>();
            CurrentTrackIndex = -1;
            InnerPlaylist = innerPlaylist;            
        }

        public int Id { get; set; }
        public int CurrentTrackIndex { get; set; }
        //public List<string> Tracks { get; set; }
        public MediaPlayerDAL.Model.Playlist InnerPlaylist { get; }

        public void Shuffle()
        {
            List<MediaPlayerDAL.Model.Track> tracks = new List<MediaPlayerDAL.Model.Track>(InnerPlaylist.Tracks);
            List<MediaPlayerDAL.Model.Track> shuffledTracks = new List<MediaPlayerDAL.Model.Track>();
            Random r = new Random(DateTime.Now.Millisecond);
            for (int i = 0; i < tracks.Count; i++)
            {
                int trackIndex = r.Next(tracks.Count);
                shuffledTracks.Add(tracks[trackIndex]);
                tracks.RemoveAt(trackIndex);
                i--;
            }

            InnerPlaylist.Tracks = shuffledTracks;
        }

        public MediaPlayerDAL.Model.Track GetCurrentTrack()
        {
            if(InnerPlaylist.Tracks.Any() && CurrentTrackIndex != -1)
            {
                return InnerPlaylist.Tracks[CurrentTrackIndex];
            }
            
            return null;
        }

        public bool IsLastTrack()
        {
            return CurrentTrackIndex == InnerPlaylist.Tracks.Count - 1;
        }

        public void QueueInNextTrack()
        {
            if (InnerPlaylist.Tracks.Count == 0)
            {
                return;
            }

            if (CurrentTrackIndex == InnerPlaylist.Tracks.Count - 1)
            {
                CurrentTrackIndex = 0;
            }
            else
            {
                CurrentTrackIndex++;
            }
        }

        public MediaPlayerDAL.Model.Track GetNextTrack()
        {
            if(InnerPlaylist.Tracks.Count == 0)
            {
                return null;
            }

            if(CurrentTrackIndex == InnerPlaylist.Tracks.Count-1) 
            {
                CurrentTrackIndex = 0;
            }
            else
            {
                CurrentTrackIndex++;
            }

            return InnerPlaylist.Tracks[CurrentTrackIndex];
        }

        public MediaPlayerDAL.Model.Track GetPreviousTrack()
        {
            if (InnerPlaylist.Tracks.Count == 0)
            {
                return null;
            }

            if (CurrentTrackIndex == 0)
            {
                CurrentTrackIndex = InnerPlaylist.Tracks.Count - 1;
            }
            else
            {
                CurrentTrackIndex--;
            }

            return InnerPlaylist.Tracks[CurrentTrackIndex];
        }        
    }
}
