﻿using System.IO;
using System.Net.Sockets;
using System.Net;
using System.Threading;
using System;
using MediaPlayerLib.Network.Contract;
using System.Collections.Generic;
using System.Linq;
using MediaPlayerLib.Network.Message;
using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;

namespace MediaPlayerLib.Network
{
    public class OverlayServer
    {
        public OverlayServer(int port, ILogger logger)
        {
            Port = port;
            Logger = logger;
            Messages = new Queue<IMessage>();
            MessageQueueLocker = new Semaphore(1,1);
            ServerThread = new Thread(StartInternal);
            KeepListening = true;
        }

        public int Port { get; }
        public ILogger Logger { get; }
        public Semaphore MessageQueueLocker { get; }
        private Thread ServerThread { get; }
        public bool KeepListening { get; set; }
        private object _lock = new object();

        public void Log(string message)
        {
            Logger.Log(message);
        }

        Queue<IMessage> Messages { get; }

        public void Send(IMessage message)
        {
            lock (_lock)
            {
                MessageQueueLocker.WaitOne();
                Messages.Enqueue(new HeaderMessage() { Type = message.GetMessageType() });
                Messages.Enqueue(message);
                MessageQueueLocker.Release();
                Monitor.PulseAll(_lock);
            }            
        }

        public void Start()
        {
            ServerThread.Start();
        }

        public void Stop()
        {
            lock (_lock)
            {
                KeepListening = false;
                Monitor.PulseAll(_lock);
            }
            ServerThread.Join();
        }

        public async void StartInternal()
        {
            Log("Starting Overlay Server");
            var ipEndPoint = new IPEndPoint(IPAddress.Loopback, Port);
            TcpListener listener = new TcpListener(ipEndPoint);
            try
            {
                listener.Start();
                Log("Awaiting connection...");
                var acceptClientTask = listener.AcceptTcpClientAsync();
                int overlayServerTimeout = 30000;
                if (await Task.WhenAny(acceptClientTask, Task.Delay(overlayServerTimeout)) != acceptClientTask)
                {
                    Log($"No connection received in {overlayServerTimeout/1000} seconds. Stopping Overlay Server.");
                    return;
                }

                Log("Connected");
                var handler = acceptClientTask.Result;
                using (var stream = handler.GetStream())
                {
                    while (KeepListening)
                    {
                        lock (_lock)
                        {
                            while (!Messages.Any() && KeepListening)
                            {
                                Monitor.Wait(_lock);
                            }
                        }

                        if (Messages.Any())
                        {
                            MessageQueueLocker.WaitOne();
                            var headerMessage = Messages.Dequeue();
                            var innerMessage = Messages.Dequeue();
                            MessageQueueLocker.Release();
                            Send(stream, headerMessage);
                            Send(stream, innerMessage);
                        }
                    }
                }
                handler.Dispose();
            }
            catch (Exception ex)
            {
                Log(ex.ToString());
            }
            finally
            {
                listener.Stop();
                Log("Overlay Server Stopped");
            }
        }

        private void Send(NetworkStream stream, IMessage message)
        {
            Log($"Sending message: {message.GetMessageType()}");
            System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(message.GetType());
            using (var ms = new MemoryStream())
            {
                xmlSerializer.Serialize(ms, message);
                ms.Flush();
                var lengthInBytes = BitConverter.GetBytes(ms.Length);
                stream.Write(lengthInBytes, 0, sizeof(int));
                stream.Write(ms.ToArray(), 0, (int)ms.Length);
            }
        }
    }
}
