﻿using MediaPlayerLib.Network.Message;

namespace MediaPlayerLib.Network.Contract
{
    public interface IMessage
    {
        MessageType GetMessageType();
    }
}
