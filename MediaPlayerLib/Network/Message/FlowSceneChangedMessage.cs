﻿using MediaPlayerLib.Network.Contract;

namespace MediaPlayerLib.Network.Message
{
    public class FlowSceneChangedMessage : IMessage
    {
        public FlowSceneChangedMessage()
        {            
        }

        public FlowSceneChangedMessage(int flowScene)
        {
            FlowScene = flowScene;
        }

        public int FlowScene { get; set; }

        public MessageType GetMessageType()
        {
            return MessageType.FlowSceneChanged;
        }
    }
}
