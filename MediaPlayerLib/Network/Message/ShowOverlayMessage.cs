﻿using MediaPlayerLib.Network.Contract;

namespace MediaPlayerLib.Network.Message
{
    public class ShowOverlayMessage : IMessage
    {
        public MessageType GetMessageType()
        {
            return MessageType.ShowOverlay;
        }
    }
}
