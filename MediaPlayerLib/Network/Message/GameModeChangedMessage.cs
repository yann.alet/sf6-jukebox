﻿using MediaPlayerLib.Network.Contract;

namespace MediaPlayerLib.Network.Message
{
    public class GameModeChangedMessage : IMessage
    {
        public GameModeChangedMessage()
        {            
        }

        public GameModeChangedMessage(int gameMode)
        {
            GameMode = gameMode;
        }

        public int GameMode { get; set; }

        public MessageType GetMessageType()
        {
            return MessageType.GameModeChanged;
        }
    }
}
