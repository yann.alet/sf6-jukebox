﻿using MediaPlayerLib.Network.Contract;

namespace MediaPlayerLib.Network.Message
{
    public class TrackUpdateMessage : IMessage
    {
        public string Track { get; set; }
        public float Progress { get; set; }

        public MessageType GetMessageType()
        {
            return MessageType.TrackUpdate;
        }
    }
}
