﻿using MediaPlayerLib.Network.Contract;

namespace MediaPlayerLib.Network.Message
{
    public class FightStateChangedMessage : IMessage
    {
        public FightStateChangedMessage()
        {            
        }

        public FightStateChangedMessage(int fightState)
        {
            FightState = fightState;
        }

        public int FightState { get; set; }

        public MessageType GetMessageType()
        {
            return MessageType.FightStateChanged;
        }
    }
}
