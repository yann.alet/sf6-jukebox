﻿namespace MediaPlayerLib.Network.Message
{
    public enum MessageType
    {
        Header,
        TrackUpdate,
        ShowOverlay,
        FightStateChanged,
        GameModeChanged,
        FlowSceneChanged
    }
}
