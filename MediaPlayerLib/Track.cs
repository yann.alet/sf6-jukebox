﻿using MediaPlayerDAL;
using MediaPlayerDAL.Model;

namespace MediaPlayerLib
{
    public class Track
    {
        public Track(string path, PlaybackSettings playbackSettings = null)
        {
            Path = path;
            PlaylistType = PlaylistType.None;
            PlaybackSettings = playbackSettings == null ? new PlaybackSettings() : playbackSettings;
        }

        public Track(string path, FlowSceneId flowSceneId, PlaybackSettings playbackSettings = null)
        {
            Path = path;
            FlowSceneId = flowSceneId;
            PlaylistType = PlaylistType.FlowScene;
            PlaybackSettings = playbackSettings == null ? new PlaybackSettings() : playbackSettings;
        }

        public Track(string path, StageId stageId, PlaybackSettings playbackSettings = null)
        {
            Path = path;
            StageId = stageId;
            PlaylistType = PlaylistType.Stage;
            PlaybackSettings = playbackSettings == null ? new PlaybackSettings() : playbackSettings;
        }

        public Track(string path, CustomScreenId customScreenId, PlaybackSettings playbackSettings = null)
        {
            Path = path;
            CustomScreenId = customScreenId;
            PlaylistType = PlaylistType.CustomScreen;
            PlaybackSettings = playbackSettings == null ? new PlaybackSettings() : playbackSettings;
        }

        public Track(string path, CharacterId characterId, PlaybackSettings playbackSettings = null)
        {
            Path = path;
            CharacterId = characterId;
            PlaylistType = PlaylistType.Character;
            PlaybackSettings = playbackSettings == null ? new PlaybackSettings() : playbackSettings;
        }

        public Track(MediaPlayerDAL.Model.Track track, FlowSceneId flowSceneId)
        {
            Path = track.Path;
            FlowSceneId = flowSceneId;
            PlaylistType = PlaylistType.FlowScene;
            PlaybackSettings = track.PlaybackSettings;
        }

        public Track(MediaPlayerDAL.Model.Track track, StageId stageId)
        {
            Path = track.Path;
            StageId = stageId;
            PlaylistType = PlaylistType.Stage;
            PlaybackSettings = track.PlaybackSettings;
        }

        public Track(MediaPlayerDAL.Model.Track track, CustomScreenId customScreenId)
        {
            Path = track.Path;
            CustomScreenId = customScreenId;
            PlaylistType = PlaylistType.CustomScreen;
            PlaybackSettings = track.PlaybackSettings;
        }

        public Track(MediaPlayerDAL.Model.Track track, CharacterId characterId)
        {
            Path = track.Path;
            CharacterId = characterId;
            PlaylistType = PlaylistType.Character;
            PlaybackSettings = track.PlaybackSettings;
        }

        public string Path { get; }
        public PlaylistType PlaylistType { get; }
        public FlowSceneId FlowSceneId { get; }
        public StageId StageId { get; }
        public CustomScreenId CustomScreenId { get; }
        public CharacterId CharacterId { get; }
        public PlaybackSettings PlaybackSettings { get; }
    }
}
