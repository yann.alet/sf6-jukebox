﻿using System;

namespace MediaPlayerLib
{
    public class PlaybackAction
    {
        public PlaybackAction(string name, Action action, PlaybackActionType actionType) 
        {
            Name = name;
            Action = action;
            ActionType = actionType;        
        }

        public enum PlaybackActionType
        {
            Play,
            Stop
        }

        public string Name { get; }
        public Action Action { get; }
        public PlaybackActionType ActionType { get; }

    }
}
