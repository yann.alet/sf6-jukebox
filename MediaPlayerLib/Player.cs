﻿using MediaPlayerDAL.Model;
using MediaPlayerDAL.Service;
using System;

namespace MediaPlayerLib
{
    public class Player
    {
        public Player(Logger logger)
        {
            Logger = logger;
        }

        private Logger Logger { get; }
        private ThreadedPlayer ThreadedPlayer { get; set; }
        private SerializationService SerializationService { get; set; }
        private Settings Settings { get; set; }

        public void Initialize()
        {
            SerializationService = new SerializationService();
            Settings = SerializationService.Load();
            ThreadedPlayer = new ThreadedPlayer(Settings, Logger);
        }

        public void EnableOverlay()
        {
            ThreadedPlayer.EnableOverlay();
        }

        public void DisableOverlay() 
        {
            ThreadedPlayer.DisableOverlay();
        }

        public void OnFightStateChanged(int fightState)
        {
            ThreadedPlayer.OnFightStateChanged(fightState);
        }

        public void OnGameModeChanged(int  gameMode)
        {
            ThreadedPlayer.OnGameModeChanged(gameMode);
        }

        public void OnFlowSceneChanged(int flowScene)
        {
            ThreadedPlayer.OnFlowSceneChanged(flowScene);
        }

        public void OnRoundChanged(int round)
        {
            ThreadedPlayer.OnRoundChanged(round);
        }

        public void Destroy()
        {
            ThreadedPlayer.Destroy();
        }

        public void OnDanger()
        {
            ThreadedPlayer.OnDanger();
        }

        public void IncreaseVolume()
        {
            if (ThreadedPlayer.IsPlaying)
            {
                ThreadedPlayer.Volume = Math.Min(ThreadedPlayer.Volume + 0.005f, 1);                
            }
        }

        public void DecreaseVolume()
        {
            if (ThreadedPlayer.IsPlaying)
            {
                ThreadedPlayer.Volume = Math.Max(ThreadedPlayer.Volume - 0.005f, 0);
            }
        }

        public void PlayCharacterTrack(int characterId, int round)
        {
            ThreadedPlayer.PlayCharacterTrack(characterId, round);
        }

        public void PlayStageTrack(int stageId)
        {
            ThreadedPlayer.PlayStageTrack(stageId);
        }

        public void PlayFlowSceneTrack(int flowSceneId)
        {
            ThreadedPlayer.PlayFlowSceneTrack(flowSceneId);
        }

        public void PlayCustomScreenTrack(int customScreenId)
        {
            ThreadedPlayer.PlayCustomScreenTrack(customScreenId);
        }

        public void Stop()
        {
            ThreadedPlayer.Stop();
        }

        public void SetPosition(int position)
        {
            ThreadedPlayer.SetPosition(position);
        }

        public void QueueInStageNextTrack()
        {
            ThreadedPlayer.QueueInStageNextTrack();
        }

        public void PlayNextTrack()
        {
            ThreadedPlayer.PlayNextTrack();            
        }

        public void PlayPreviousTrack()
        {
            ThreadedPlayer.PlayPreviousTrack();            
        }        

        public void ShowOverlay()
        {
            ThreadedPlayer.ShowOverlay();
        }

        public void ResetFlowScenePlaylist()
        {
            ThreadedPlayer.ResetFlowScenePlaylist();
        }

        public void ResetStagePlaylist()
        {
            ThreadedPlayer.ResetStagePlaylist();
        }

        public void ResetCharacterPlaylist()
        {
            ThreadedPlayer.ResetCharacterPlaylist();
        }

        public void ResetCustomScreensPlaylist()
        {
            ThreadedPlayer.ResetCustomScreensPlaylist();
        }

        public void Transition()
        {
            ThreadedPlayer.Transition(new Track(@"F:\Mix\No scrubs.mp3", MediaPlayerDAL.CharacterId.Kimberly));
        }

        public void SaveSettings()
        {
            Logger.Log("SaveSettings");
            SerializationService.Save(Settings);
        }
    }
}
