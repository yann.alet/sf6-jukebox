﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaPlayerLib
{
    public enum PlaylistType
    {
        None,
        Stage,
        FlowScene,
        CustomScreen,
        Character
    };
}
