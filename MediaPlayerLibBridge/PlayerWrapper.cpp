#include "PlayerWrapper.h"

#include <vcclr.h>


using namespace MediaPlayerLib;
using namespace System;
using namespace System::Runtime::InteropServices;

bool _isInitialized = false;

gcroot<Logger^> _logger = gcnew Logger();
gcroot<Player^> _player;

void(*onLogOccured)(CLogEventArgs);

char* convertString(String^ str)
{
	char* converted = (char*)(void*)Marshal::StringToHGlobalAnsi(str);
	return converted;
}

CLogEventArgs toCLogEventArgs(Object^ obj, EventArgs^ args)
{
	//todo: weird thing happening here, can no longer cast obj back to Player...
	//Player^ castedObj = safe_cast<Player^>(obj);
	LogEventArgs^ castedArgs = safe_cast<LogEventArgs^>(args);
	CLogEventArgs logEventArgs;
	logEventArgs.Message = convertString(castedArgs->Message);
	return logEventArgs;
}

void ensureInitialized()
{
	if (!_isInitialized)
	{
		_player = gcnew Player(_logger);
		_player->Initialize();
		_isInitialized = true;
	}
}

__declspec(dllexport) void playCharacterTrack(int characterId, int round)
{
	ensureInitialized();
	_player->PlayCharacterTrack(characterId, round);
}

__declspec(dllexport) void playStageTrack(int stageId)
{
	ensureInitialized();
	_player->PlayStageTrack(stageId);
}

__declspec(dllexport) void playFlowSceneTrack(int flowSceneId)
{
	ensureInitialized();
	_player->PlayFlowSceneTrack(flowSceneId);
}

__declspec(dllexport) void playCustomScreenTrack(int customScreenId)
{
	ensureInitialized();
	_player->PlayCustomScreenTrack(customScreenId);
}

__declspec(dllexport) void setPosition(int position)
{
	ensureInitialized();
	_player->SetPosition(position);
}

__declspec(dllexport) void onFightStateChanged(int fightState)
{
	ensureInitialized();
	_player->OnFightStateChanged(fightState);
}

__declspec(dllexport) void onFlowSceneChanged(int flowScene)
{
	ensureInitialized();
	_player->OnFlowSceneChanged(flowScene);
}

__declspec(dllexport) void onGameModeChanged(int gameMode)
{
	ensureInitialized();
	_player->OnGameModeChanged(gameMode);
}

__declspec(dllexport) void onRoundChanged(int round)
{
	ensureInitialized();
	_player->OnRoundChanged(round);
}

__declspec(dllexport) void onDanger()
{
	ensureInitialized();
	_player->OnDanger();
}

__declspec(dllexport) void stop()
{
	ensureInitialized();
	_player->Stop();
}

__declspec(dllexport) void increaseVolume()
{
	ensureInitialized();
	_player->IncreaseVolume();
}

__declspec(dllexport) void decreaseVolume()
{
	ensureInitialized();
	_player->DecreaseVolume();
}

__declspec(dllexport) void playNextTrack()
{
	ensureInitialized();
	_player->PlayNextTrack();
}

__declspec(dllexport) void playPreviousTrack()
{
	ensureInitialized();
	_player->PlayPreviousTrack();
}

__declspec(dllexport) void transition()
{
	ensureInitialized();
	_player->Transition();
}

__declspec(dllexport) void resetStagePlaylist()
{
	ensureInitialized();
	_player->ResetStagePlaylist();
}

__declspec(dllexport) void resetFlowScenePlaylist()
{
	ensureInitialized();
	_player->ResetFlowScenePlaylist();
}

__declspec(dllexport) void resetCustomScreenPlaylist()
{
	ensureInitialized();
	_player->ResetCustomScreensPlaylist();
}

__declspec(dllexport) void resetCharacterPlaylist()
{
	ensureInitialized();
	_player->ResetCharacterPlaylist();
}

__declspec(dllexport) void queueInNextTrack()
{
	ensureInitialized();
	_player->QueueInStageNextTrack();
}

__declspec(dllexport) void saveSettings()
{
	ensureInitialized();
	_player->SaveSettings();
}

__declspec(dllexport) void showOverlay()
{
	ensureInitialized();
	_player->ShowOverlay();
}

__declspec(dllexport) void enableOverlay()
{
	ensureInitialized();
	_player->EnableOverlay();
}

__declspec(dllexport) void disableOverlay()
{
	ensureInitialized();
	_player->DisableOverlay();
}

__declspec(dllexport) void destroy()
{
	if (_isInitialized)
	{
		_player->Destroy();
	}	
}

void onLog(Object^ obj, EventArgs^ args)
{
	onLogOccured(toCLogEventArgs(obj, args));
}

__declspec(dllexport) void subscribe_onLog(void(*funcPtr)(CLogEventArgs))
{
	onLogOccured = funcPtr;
	_logger->OnLogRequested += gcnew EventHandler(&onLog);
}