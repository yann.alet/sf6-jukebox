// dllmain.cpp : Defines the entry point for the DLL application.
#include "pch.h"
#include <string>
#include "include/reframework/API.hpp"
#include "Logger.h"
#include "JukeboxAPI.h"
#include "PlayerAPI.h"
#include "Settings.h"
#include "PerfTracker.h"

void on_present()
{
    PERF_TRACKING("on_present", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    JukeboxAPI::GetInstance()->Update();    
}

extern "C" __declspec(dllexport) bool reframework_plugin_initialize(const REFrameworkPluginInitializeParam * param) 
{
    reframework::API::initialize(param);
    
    if (Settings::GetInstance()->IsEnabled())
    {   
        const auto functions = param->functions;
        param->functions->on_present(on_present);
        PlayerAPI::GetInstance()->resetCharacterPlaylist();
        PlayerAPI::GetInstance()->resetStagePlaylist();
        PlayerAPI::GetInstance()->resetFlowScenePlaylist();
        PlayerAPI::GetInstance()->resetCustomScreenPlaylist();  
    }
    else
    {
        LogInfo("Mod is disabled.");
    }

    return true;
}

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
        break;
    case DLL_THREAD_ATTACH:
        break;
    case DLL_THREAD_DETACH:
        break;
    case DLL_PROCESS_DETACH:
        if (lpReserved)
        {
            LogInfo("BEGIN DLL_PROCESS_DETACH");
            if (Settings::GetInstance()->IsEnabled())
            {
                PlayerAPI::GetInstance()->destroy();
            }
            LogInfo("END DLL_PROCESS_DETACH");
        }
        break;
    }
    return TRUE;
}


