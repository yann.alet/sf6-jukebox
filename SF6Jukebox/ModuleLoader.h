#pragma once
#include <Windows.h>
#include <string>

class ModuleLoader
{
public:
	ModuleLoader(ModuleLoader& other) = delete;
	void operator=(const ModuleLoader&) = delete;

	HMODULE GetHandle() { return handle; }
	std::string GetModulePath();

	static ModuleLoader* GetInstance();

protected:
	ModuleLoader();
	static ModuleLoader* instance;

private:
	std::string GetBridgeDllPath();
	HMODULE handle;
};

