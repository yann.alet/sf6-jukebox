#pragma once

#include <string>
#include <Windows.h>
#include <filesystem>

using namespace std;


string GetModulePath()
{
    char path[256];
    HMODULE hm = NULL;

    if (GetModuleHandleEx(GET_MODULE_HANDLE_EX_FLAG_FROM_ADDRESS |
        GET_MODULE_HANDLE_EX_FLAG_UNCHANGED_REFCOUNT,
        (LPCSTR)&GetModulePath, &hm) == 0)
    {
        int ret = GetLastError();
        fprintf(stderr, "GetModuleHandle failed, error = %d\n", ret);
        // Return or however you want to handle an error.
    }
    if (GetModuleFileName(hm, path, sizeof(path)) == 0)
    {
        int ret = GetLastError();
        fprintf(stderr, "GetModuleFileName failed, error = %d\n", ret);
        // Return or however you want to handle an error.
    }
    string dllPath = string(path);
    string pluginParentPath = filesystem::path{ dllPath }.parent_path().string();
    return pluginParentPath;
}

string GetBridgeDllPath()
{
    auto path = filesystem::path{ GetModulePath() } / filesystem::path{ "MediaPlayerLibBridge.dll" };
    return path.string();
}
