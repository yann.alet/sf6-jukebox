#pragma once
#include "PlayerWrapper.h"
#include "Constants.h"
#include "Character.h"
#include <string>

class PlayerAPI
{
public:
	PlayerAPI(PlayerAPI& other) = delete;
	void operator=(const PlayerAPI&) = delete;

	void playCharacterTrack(Fight::PlayerType playerId, int round);
	void playStageTrack(Fight::Stage stageId);
	void playFlowSceneTrack(Game::FlowScene flowSceneId);
	void playCustomScreenTrack(Game::CustomScreen customScreenId);
	void setPosition(int position);
	void onFightStateChanged(int fightState);
	void onGameModeChanged(int gameMode);
	void onFlowSceneChanged(int flowScene);
	void onRoundChanged(int round);
	void onDanger();
	void stopInternal();
	void saveSettings();
	void showOverlay();
	void transition();
	void queueInNextTrack();
	void increaseVolume();
	void decreaseVolume();
	void playNextTrack();
	void playPreviousTrack();
	void resetStagePlaylist();
	void resetFlowScenePlaylist();
	void resetCustomScreenPlaylist();
	void resetCharacterPlaylist();
	void enableOverlay();
	void disableOverlay();
	void destroy();

	static PlayerAPI* GetInstance();
	bool IsPlayingTrack() { return IsPlayingCustomScreenTrack() || IsPlayingFlowSceneTrack() || IsPlayingStageTrack() || IsPlayingCharacterOverride() || IsPlayingCharacterTrack(); }
	Game::FlowScene GetCurrentPlayingFlowScene() { return currentPlayingFlowScene; }
	Game::CustomScreen GetCurrentPlayingCustomScreen() { return currentPlayingCustomScreen; }
	Fight::PlayerType GetCurrentPlayingCharacter() { return currentPlayingCharacter; }

	//todo: possibly off sync, profile cost of .net roundtrip every frame.
	bool IsPlayingCustomScreenTrack() { return isPlayingCustomScreenTrack; }
	bool IsPlayingFlowSceneTrack() { return isPlayingFlowSceneTrack; }
	bool IsPlayingStageTrack() { return isPlayingStageTrack; }
	bool IsPlayingCharacterTrack() { return isPlayingCharacterTrack; }
	bool IsPlaying(Game::CustomScreen screenId) { return currentPlayingCustomScreen == screenId; }
	bool IsPlaying(Fight::PlayerType fightType) { return currentPlayingCharacter == fightType; }
	bool IsPlayingCharacterOverride() { return Character::IsTrackOverrideScreenId(currentPlayingCustomScreen); }
	

protected:

	PlayerAPI() = default;
	static PlayerAPI* instance;

private:
	Game::FlowScene currentPlayingFlowScene = Game::FlowScene::eNone;
	Game::CustomScreen currentPlayingCustomScreen = Game::CustomScreen::CustomScreen_Unknown;
	Fight::PlayerType currentPlayingCharacter = Fight::PlayerType::PlayerType_Unknown;
	bool callParameterLessFunction(std::string functionName);
	bool callFunction(std::string functionName, int parameter);
	bool callFunction2(std::string functionName, int parameter1, int parameter2);

	bool isPlayingCustomScreenTrack = false;
	bool isPlayingFlowSceneTrack = false;
	bool isPlayingStageTrack = false;
	bool isPlayingCharacterTrack = false;
};

