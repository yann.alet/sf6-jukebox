#pragma once
#include "Constants.h"
#include "Character.h"
#include "BattleDescription.h"
#include "Settings.h"
#include "GameAPI.h"
#include "Context.h"
#include <vector>
#include <set>
#include <optional>

class PlayerAPI;
class Settings;

class JukeboxAPI
{
public:
	JukeboxAPI(JukeboxAPI& other) = delete;
	void operator=(const JukeboxAPI&) = delete;

	static JukeboxAPI* GetInstance();

	void Update();



protected:
	JukeboxAPI();
	PlayerAPI* playerAPI;

	static JukeboxAPI* instance;

private:
	void PlayForFlowScene(Game::FlowScene flowScene);
	void HandleVolume(Game::FlowScene flowScene, Game::Mode gameMode, Fight::State state, Fight::Stage stage);
	void HandleMenu(Game::FlowScene flowScene, Fight::State state, Game::Mode gameMode, bool flowSceneUpdated);
	void HandleWorldTour(Game::FlowScene flowScene, Fight::State state, Game::Mode gameMode, Fight::Stage stage, bool flowSceneUpdated);
	
	void HandleFlowScene();
	void HandleFightState(Game::FlowScene flowScene, Fight::State state, Game::Mode gameMode, Fight::Stage stage, bool flowSceneUpdated);
	bool HandleCharacterPlaylist(int round);
	bool HandleStagePlaylist(Fight::Stage stage, int round);
	void HandleShortcuts();
	bool IsFlowSceneEnabled(Game::FlowScene flowScene);
	bool IsStageEnabled(Fight::Stage stage);
	void MuteBGM();
	void UnMuteBGM();

	Game::FlowScene LastFlowScene = Game::FlowScene::eNone;
	Fight::State LastFightState = Fight::State::StageInit;
	Game::Mode LastGameMode = Game::Mode::Mode_Unknown;
	Fight::Stage LastStage = Fight::Stage::Stage_Unkown;

	std::set<Game::FlowScene> FightLoadingScene
	{
		Game::FlowScene::eBattleFighterEmote,
		Game::FlowScene::eTrainingSetup,
		Game::FlowScene::eSpectateAssetReload,
		Game::FlowScene::eTutorialSetup,
		Game::FlowScene::eReadyAsset,
		Game::FlowScene::eSpectateSetup,
		Game::FlowScene::eWorldTourEngageBattleReady
	};

	std::set<Game::FlowScene> MutedScenes
	{
		Game::FlowScene::eBattleFighterLeave,
		Game::FlowScene::eBattleFighterEmote
		//Game::FlowScene::eOnlineTrainingSetup
	};

	// Scenes for which we want custom tracks to continue playing over.
	std::set<Game::FlowScene> IgnoredScenes
	{
		Game::FlowScene::eBattleFighterLeave,
		Game::FlowScene::eBootedTransitionHub,
		Game::FlowScene::eLogin
	};

	std::optional<Settings::PlaylistPriority> CurrentRandomPlaylistPriority = std::nullopt;

	std::string LastBattleTrackName = "";

	Character* lastP1CharacterState = nullptr;
	Character* lastP2CharacterState = nullptr;
	Fight::Stage lastStage = Fight::Stage::Stage_Unkown;
	
	Settings* settings = nullptr;
	GameAPI* gameAPI = nullptr;
	Context CurrentContext;

	float GameVolumeLevel = -20.0f;
	int LastRoundNumber = 0;

	bool WasVolumeUpPressed = false;
	bool WasVolumeDownPressed = false; 
	bool WasNextTrackPressed = false;
	bool WasPreviousTrackPressed = false;
	bool WasShowOverlayPressed = false;
	bool WasRestartPressed = false;
	bool IsCharacterStateOverrideDetected = false;
	bool LastDangerState = false;
};

