#include "pch.h"
#include "JukeboxAPI.h"
#include "PlayerAPI.h"
#include "RefAPI.h"
#include "Logger.h"
#include "PerfTracker.h"
#include "include/reframework/API.hpp"
#include <string>
#include <vector>
#include <random>

JukeboxAPI* JukeboxAPI::instance = nullptr;

JukeboxAPI::JukeboxAPI()
{
    playerAPI = PlayerAPI::GetInstance();
    settings = Settings::GetInstance();
    gameAPI = GameAPI::GetInstance();
    lastP1CharacterState = new Character(Fight::PlayerType::PlayerType_Unknown, 0);
    lastP2CharacterState = new Character(Fight::PlayerType::PlayerType_Unknown, 0);
}

JukeboxAPI* JukeboxAPI::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new JukeboxAPI();
	}

	return instance;
}

void JukeboxAPI::Update()
{
    PERF_TRACKING("JukeboxAPI::Update", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);

    HandleFlowScene();
    HandleShortcuts();
}

bool JukeboxAPI::IsFlowSceneEnabled(Game::FlowScene flowScene)
{
    PERF_TRACKING("JukeboxAPI::IsFlowSceneEnabled", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    return Settings::GetInstance()->FlowSceneEnabledStates.count(flowScene)
        && Settings::GetInstance()->FlowSceneEnabledStates[flowScene];
}

bool JukeboxAPI::IsStageEnabled(Fight::Stage stage)
{
    return settings->StageEnabledStates.count(stage)
        && settings->StageEnabledStates[stage];
}

void JukeboxAPI::HandleFlowScene()
{
    PERF_TRACKING("JukeboxAPI::HandleFlowScene", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    bool flowSceneUpdated = false;
    auto flowScene = gameAPI->GetFlowScene();
    auto gameMode = gameAPI->GetGameMode();
    auto state = gameAPI->GetFightState();
    
    Fight::Stage stage = gameAPI->GetStage(flowScene, gameMode, state);
    
    if (LastFlowScene != flowScene)
    {
        LogInfo("Flowscene updated from " + std::to_string(LastFlowScene) + " to " + std::to_string(flowScene));
        playerAPI->onFlowSceneChanged(flowScene);
        LastFlowScene = flowScene;
        flowSceneUpdated = true;
    }

    if (LastGameMode != gameMode)
    {
        LogInfo("Game mode updated from {} to {}", (int)LastGameMode, (int)gameMode);
        playerAPI->onGameModeChanged(gameMode);
        LastGameMode = gameMode;
    }

    if (LastStage != stage)
    {
        LogInfo("Stage updated from {} to {}", (int)LastStage, (int)stage);
        LastStage = stage;
    }

    Game::Screen screen = gameAPI->GetScreen(flowScene, gameMode, state);

    if (CurrentContext.LastScreen != screen)
    {
        LogInfo("Screen updated from {} to {}", (int)CurrentContext.LastScreen, (int)screen);
        CurrentContext.LastScreen = screen;
    }

    switch (screen)
    {
        case Game::Screen::WorldTour:
            HandleWorldTour(flowScene, state, gameMode, stage, flowSceneUpdated);
            break;

        case Game::Screen::Fight:
            HandleFightState(flowScene, state, gameMode, stage, flowSceneUpdated);
            break;

        case Game::Screen::Menu:
            HandleMenu(flowScene, state, gameMode, flowSceneUpdated);
            break;
    }
}

void JukeboxAPI::HandleVolume(Game::FlowScene flowScene, Game::Mode gameMode, Fight::State state, Fight::Stage stage)
{
    if (settings->MusicOverride == Settings::MusicOverrideMode::Full)
    {
        MuteBGM();
    }
    else
    {
        switch (gameAPI->GetScreen(flowScene, gameMode, state))
        {
            case Game::Screen::WorldTour:
                if (IsFlowSceneEnabled(flowScene) || (flowScene == Game::FlowScene::eWorldTourCharacterCreateShop && IsFlowSceneEnabled(Game::FlowScene::eBattleHubCharacterCreateShop)))
                {
                    MuteBGM();
                }
                else if (gameAPI->FightFlowScenes.count(flowScene) && settings->StageEnabledStates[stage])
                {
                    MuteBGM();
                }                     
                else
                {
                    UnMuteBGM();
                }
                break;

            case Game::Screen::Fight:
				if (state < Fight::State::WinWait)
                {
                    if (CurrentContext.CustomTrackForFight)
                    {
                        MuteBGM();
					}
					else if(playerAPI->IsPlaying(Game::CustomScreen::ResultScreen))
					{
						UnMuteBGM();
					}                    
                }
                if (state == Fight::State::Finish)
                {
                    if (settings->CustomScreenEnabledStates[Game::CustomScreen::ResultScreen])
                    {
                        MuteBGM();
                    }
                    else
                    {
                        UnMuteBGM();
                    }
                }
                if (state == Fight::State::WinWait && gameMode == Game::Mode::Training)
                {
                    if (CurrentContext.CustomTrackForFight)
                    {
                        MuteBGM();
                    }
                    else
                    {
                        UnMuteBGM();
                    }
                }
                break;

            case Game::Screen::Menu:
                if (IsFlowSceneEnabled(flowScene) || MutedScenes.count(flowScene))
                {
                    MuteBGM();
                }
                else
                {
                    UnMuteBGM();
                }
                break;
        }
    }
}

void JukeboxAPI::HandleMenu(Game::FlowScene flowScene, Fight::State state, Game::Mode gameMode, bool flowSceneUpdated)
{
    HandleVolume(flowScene, gameMode, state, Fight::Stage::Stage_Unkown);

    if (FightLoadingScene.count(flowScene))
    {
        if (settings->MusicOverride == Settings::MusicOverrideMode::Mixed && !CurrentContext.IsSpectating)
        {
            gameAPI->DeletePlayedTracks();
            gameAPI->IsPlayingBGM = false;
        }
    }

    if (flowSceneUpdated)
    {
        if (flowScene == Game::FlowScene::eSpectateSetup)
        {
            CurrentContext.IsSpectating = true;
        }

        if (CurrentContext.IsSpectating && !FightLoadingScene.count(flowScene) && flowScene != Game::FlowScene::eBattleMain && flowScene != Game::FlowScene::eSpectateAssetReload)
        {
            CurrentContext.IsSpectating = false;
        }

        if (playerAPI->IsPlayingStageTrack() || playerAPI->IsPlayingCharacterTrack() || playerAPI->IsPlayingCustomScreenTrack())
        {
            // We're no longer fighting, so we should stop playing any fighting track.
            playerAPI->stopInternal();
        }

        PlayForFlowScene(flowScene);
    }
}

void JukeboxAPI::HandleWorldTour(Game::FlowScene flowScene, Fight::State state, Game::Mode gameMode, Fight::Stage stage, bool flowSceneUpdated)
{
    HandleVolume(flowScene, gameMode, state, stage);

    if (gameAPI->FightFlowScenes.count(flowScene))
    {
        HandleFightState(flowScene, state, gameMode, stage, flowSceneUpdated);
    }
    else if (flowSceneUpdated)
    {
        PlayForFlowScene(flowScene);
    }
}

void JukeboxAPI::PlayForFlowScene(Game::FlowScene flowScene)
{
    PERF_TRACKING("JukeboxAPI::PlayForFlowScene", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    // Little hack to avoid creating a variation of the character creation screen in the PlaylistManager.
    if (flowScene == Game::FlowScene::eWorldTourCharacterCreateShop)
    {
        flowScene = Game::FlowScene::eBattleHubCharacterCreateShop;
    }

    if (!IsFlowSceneEnabled(flowScene))
    {
        if (settings->MusicOverride == Settings::MusicOverrideMode::Mixed)
        {
            if (playerAPI->IsPlayingTrack() && !IgnoredScenes.count(flowScene))
            {
                playerAPI->stopInternal();
                playerAPI->disableOverlay();
            }
        }
    }
    else if (playerAPI->GetCurrentPlayingFlowScene() != flowScene)
    {
        LogInfo(std::string("PlayForFlowScene - ") + std::to_string(flowScene));
        playerAPI->playFlowSceneTrack(flowScene);
        playerAPI->enableOverlay();
    }
}

void JukeboxAPI::HandleFightState(Game::FlowScene flowScene, Fight::State state, Game::Mode gameMode, Fight::Stage stage, bool flowSceneUpdated)
{
    PERF_TRACKING("JukeboxAPI::HandleFightState", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    bool fightStateUpdated = false;
    if (state != LastFightState)
    {
        LogInfo("Fight state updated from : " + std::to_string(LastFightState) + " to " + std::to_string(state));
        if (LastFightState == 7 && state == 1 && gameMode == Game::Mode::Spectate)
        {
            // When spectating, after a rematch, there is a weird thing going on where the state goes from 7 to 1,
            // Then a flowscene transition to eSpectateAssetReload, then back to fight state 0.
            // We have to wait until fight state 0 to process things again.
            return;
        }

        fightStateUpdated = true;
        LastFightState = state;
    }  

    HandleVolume(flowScene, gameMode, state, stage);

    if (fightStateUpdated)
    {
        playerAPI->onFightStateChanged(state);
    }

    int currentRound = LastRoundNumber;

    switch (state)
    {
        case Fight::State::StageInit:
        {
            IsCharacterStateOverrideDetected = false;
            CurrentRandomPlaylistPriority = std::nullopt;
            LastRoundNumber = 0;
			if (CurrentContext.IsSpectating)
            {
                gameAPI->ClearSpectateBGM(); 
			}
			else if (!gameAPI->WorldTourScenes.count(flowScene))
            {
                gameAPI->DeletePreloadedTracks();
            }
            break;
        }
        case Fight::State::RoundInit:
        case Fight::State::Appear:
        case Fight::State::Ready:
        case Fight::State::Now:
        {
            if (playerAPI->IsPlaying(Game::CustomScreen::ResultScreen))
            {
				playerAPI->stopInternal();
            }

            currentRound = (state > Fight::State::StageInit && state < Fight::State::Ready) ? LastRoundNumber : gameAPI->GetRound() + 1;

            if (gameMode == Game::Mode::Story || !HandleCharacterPlaylist(currentRound))
            {
                HandleStagePlaylist(stage, currentRound);
            }            

            break;
        }
        case Fight::State::WinWait:
        {
            if (gameMode == Game::Mode::Training)
            {
                // Cut character override after a reset in practice mode.
                if (IsCharacterStateOverrideDetected)
                {
                    if (playerAPI->IsPlayingCharacterOverride())
                    {
                        playerAPI->stopInternal();
                    }
                }
                IsCharacterStateOverrideDetected = false;
            }
            break;
        }
            
        case Fight::State::Finish:
        {
            IsCharacterStateOverrideDetected = false;
            CurrentRandomPlaylistPriority = std::nullopt;
            
            // If we were fighting in World Tour, we want a different track for the next fight.
            if (gameMode == Game::Mode::Story)
            {
                LogInfo(std::string("HandleFightState - World Tour Detected - state: " + std::to_string(state)));
                playerAPI->queueInNextTrack();
            }
            else
            {
                if (settings->CustomScreenEnabledStates[Game::CustomScreen::ResultScreen])
                {
                    if (playerAPI->GetCurrentPlayingCustomScreen() != Game::CustomScreen::ResultScreen)
                    {
                        playerAPI->playCustomScreenTrack(Game::ResultScreen);
                    }
                }
                else if (playerAPI->IsPlayingTrack())
                {
                    playerAPI->stopInternal();
                }

                if (gameAPI->IsPlayingBGM)
                {
					gameAPI->StopBGM();
				}
            }

            break;
        }
        default:
            break;
    }
}

bool JukeboxAPI::HandleCharacterPlaylist(int round)
{
    PERF_TRACKING("JukeboxAPI::HandleCharacterState", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    Character p1Character = gameAPI->GetCharacter(1);
    Character p2Character = gameAPI->GetCharacter(2);
    if (!p1Character.Equals(*lastP1CharacterState))
    {
        lastP1CharacterState = &p1Character;
        LogInfo("P1: {}", std::to_string(lastP1CharacterState->Type));
    }

    if (!p2Character.Equals(*lastP2CharacterState))
    {
        lastP2CharacterState = &p2Character;
        LogInfo("P2: {}", std::to_string(lastP2CharacterState->Type));
    }

    if (!CurrentRandomPlaylistPriority)
    {
        CurrentRandomPlaylistPriority = { Settings::GetInstance()->FightPlaylistPriority };
        if (*CurrentRandomPlaylistPriority == Settings::PlaylistPriority::Random)
        {
            std::random_device rd;
            std::mt19937 gen(rd());
            std::uniform_int_distribution<> distr((int)Settings::PlaylistPriority::Stage, (int)Settings::PlaylistPriority::Opponent);
            CurrentRandomPlaylistPriority = { static_cast<Settings::PlaylistPriority>(distr(gen)) };
            switch (CurrentRandomPlaylistPriority.value())
            {
                case Settings::PlaylistPriority::Opponent:
                    LogInfo("Selected Playlist: Opponent");
                    break;

                case Settings::PlaylistPriority::P1:
                    LogInfo("Selected Playlist: Player 1");
                    break;

                case Settings::PlaylistPriority::P2:
                    LogInfo("Selected Playlist: Player 2");
                    break;

                case Settings::PlaylistPriority::Player:
                    LogInfo("Selected Playlist: Player");
                    break;

                case Settings::PlaylistPriority::Stage:
                    LogInfo("Selected Playlist: Stage");
                    break;

                default:
                    LogInfo("Selected Playlist: Unkown");
                    break;
            }            
        }
    }

    if (p1Character.ShouldOverrideTrack() && p2Character.ShouldOverrideTrack())
    {
        IsCharacterStateOverrideDetected = true;
        if (settings->CustomScreenEnabledStates[p1Character.GetBothTrackOverrideScreenId()])
        {
            if (!playerAPI->IsPlaying(p1Character.GetBothTrackOverrideScreenId()))
            {
                playerAPI->playCustomScreenTrack(p1Character.GetBothTrackOverrideScreenId());
                playerAPI->enableOverlay();
                MuteBGM();
            }
        }
        else 
        {
            if (settings->MusicOverride == Settings::MusicOverrideMode::Mixed)
            {
                UnMuteBGM();
            }

            if (playerAPI->IsPlayingTrack())
            {
                if(settings->MusicOverride == Settings::MusicOverrideMode::Mixed)
                {
                    playerAPI->disableOverlay();
                    playerAPI->stopInternal();
                }
                
                return IsCharacterStateOverrideDetected;
            }            
        }
    }
    else if (p1Character.ShouldOverrideTrack())
    {
        IsCharacterStateOverrideDetected = true;
        if (settings->CustomScreenEnabledStates[p1Character.GetSimpleTrackOverrideScreenId()])
        {
            if (!playerAPI->IsPlaying(p1Character.GetSimpleTrackOverrideScreenId()))
            {
                playerAPI->playCustomScreenTrack(p1Character.GetSimpleTrackOverrideScreenId());
                playerAPI->enableOverlay();
                MuteBGM();
            }            
        }
        else
        {
            if (settings->MusicOverride == Settings::MusicOverrideMode::Mixed)
            {
                UnMuteBGM();
            }

            if (playerAPI->IsPlayingTrack())
            {
                if (settings->MusicOverride == Settings::MusicOverrideMode::Mixed)
                {
                    playerAPI->disableOverlay();
                    playerAPI->stopInternal();
                }
                
                return IsCharacterStateOverrideDetected;
            }
        }
    }
    else if (p2Character.ShouldOverrideTrack())
    {
        IsCharacterStateOverrideDetected = true;
        if (settings->CustomScreenEnabledStates[p2Character.GetSimpleTrackOverrideScreenId()])
        {
            if (!playerAPI->IsPlaying(p2Character.GetSimpleTrackOverrideScreenId()))
            {
                playerAPI->playCustomScreenTrack(p2Character.GetSimpleTrackOverrideScreenId());
                playerAPI->enableOverlay();
                MuteBGM();
            }            
        }
        else
        {
            if (settings->MusicOverride == Settings::MusicOverrideMode::Mixed)
            {
                UnMuteBGM();
            }

            if (playerAPI->IsPlayingTrack())
            {
                if (settings->MusicOverride == Settings::MusicOverrideMode::Mixed)
                {
                    playerAPI->disableOverlay();
                    playerAPI->stopInternal();
                }

                return IsCharacterStateOverrideDetected;
            }
        }
    }
    else if (*CurrentRandomPlaylistPriority == Settings::PlaylistPriority::Stage)
    {
        return IsCharacterStateOverrideDetected;
    }
    if (!IsCharacterStateOverrideDetected)
    {
        bool isInDanger = p1Character.IsInDanger || p2Character.IsInDanger;

        if (isInDanger != LastDangerState)
        {
            LastDangerState = isInDanger;
            if (isInDanger && playerAPI->IsPlayingCharacterTrack())
            {
                playerAPI->onDanger();
            }
        }

        if (LastRoundNumber != round && playerAPI->IsPlayingCharacterTrack())
        {
            playerAPI->onRoundChanged(round);
            LastRoundNumber = round;
        }

        if (*CurrentRandomPlaylistPriority == Settings::PlaylistPriority::P1)
        {
            if (settings->CharacterEnabledStates[p1Character.Type])
            {
                if (!playerAPI->IsPlaying(p1Character.Type))
                {
                    playerAPI->playCharacterTrack(p1Character.Type, round);
                    playerAPI->enableOverlay();
                    CurrentContext.CustomTrackForFight = true;
                    MuteBGM();
                }
            }
            else
            {
                if (!gameAPI->IsPlayingBGM)
                {
                    gameAPI->PlayCharacterBGM(p1Character.Type);
                    CurrentContext.CustomTrackForFight = false;
                    UnMuteBGM();
                    playerAPI->disableOverlay();
                }
            }
            return true;
        }
        else if (*CurrentRandomPlaylistPriority == Settings::PlaylistPriority::P2)
        {
            if (settings->CharacterEnabledStates[p2Character.Type])
            {
                if (!playerAPI->IsPlaying(p2Character.Type))
                {
                    playerAPI->playCharacterTrack(p2Character.Type, round);
                    playerAPI->enableOverlay();
                    CurrentContext.CustomTrackForFight = true;
                    MuteBGM();
                }
            }
            else
            {
                if (!gameAPI->IsPlayingBGM)
                {
                    gameAPI->PlayCharacterBGM(p2Character.Type);
                    CurrentContext.CustomTrackForFight = false;
                    UnMuteBGM();
                    playerAPI->disableOverlay();
                }
            }
            return true;
        }
        else if (*CurrentRandomPlaylistPriority == Settings::PlaylistPriority::Player)
        {
            BattleDescription battleDescription = gameAPI->GetBattleDescription();
            if (settings->CharacterEnabledStates[battleDescription.GetPlayerCharacter()])
            {
                if (!playerAPI->IsPlaying(battleDescription.GetPlayerCharacter()))
                {
                    playerAPI->playCharacterTrack(battleDescription.GetPlayerCharacter(), round);
                    playerAPI->enableOverlay();
                    CurrentContext.CustomTrackForFight = true;
                    MuteBGM();
                }
            }
            else
            {
                if (!gameAPI->IsPlayingBGM)
                {
                    gameAPI->PlayCharacterBGM(battleDescription.GetPlayerCharacter());
                    CurrentContext.CustomTrackForFight = false;
                    UnMuteBGM();
                    playerAPI->disableOverlay();
                }
            }
            return true;
        }
        else if (*CurrentRandomPlaylistPriority == Settings::PlaylistPriority::Opponent)
        {
            BattleDescription battleDescription = gameAPI->GetBattleDescription();
            if (settings->CharacterEnabledStates[battleDescription.GetOpponentCharacter()])
            {
                if (!playerAPI->IsPlaying(battleDescription.GetOpponentCharacter()))
                {
                    playerAPI->playCharacterTrack(battleDescription.GetOpponentCharacter(), round);
                    playerAPI->enableOverlay();
                    CurrentContext.CustomTrackForFight = true;
                    MuteBGM();
                }
            }
            else
            {
                if (!gameAPI->IsPlayingBGM)
                {
                    gameAPI->PlayCharacterBGM(battleDescription.GetOpponentCharacter());
                    CurrentContext.CustomTrackForFight = false;
                    UnMuteBGM();
                    playerAPI->disableOverlay();
                }
            }
            return true;
        }
    }

    return IsCharacterStateOverrideDetected;
}

bool JukeboxAPI::HandleStagePlaylist(Fight::Stage stage, int round)
{
    PERF_TRACKING("JukeboxAPI::HandleStagePlaylist", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    if (lastStage != stage)
    {        
        lastStage = stage;
        LogInfo("Stage: {}", std::to_string(stage));
    }
    if (settings->StageEnabledStates[stage])
    {
        if (!playerAPI->IsPlayingStageTrack() || playerAPI->IsPlayingCharacterOverride())
        {
            playerAPI->playStageTrack(stage);
            playerAPI->enableOverlay();
            CurrentContext.CustomTrackForFight = true;
            return true;
        }        
    }
    else 
    {
        if (stage == Fight::Stage::WorldTour)
        {
            if (playerAPI->IsPlayingTrack())
            {
                playerAPI->stopInternal();
                playerAPI->disableOverlay();
            }
        }
        else if (!gameAPI->IsPlayingBGM)
        {
            gameAPI->PlayStageBGM(stage);
            CurrentContext.CustomTrackForFight = false;
            UnMuteBGM();
            playerAPI->disableOverlay();
            return true;
        }
    }

    bool isInDanger = lastP1CharacterState->IsInDanger || lastP2CharacterState->IsInDanger;
    if (isInDanger != LastDangerState)
    {
        LastDangerState = isInDanger;
        if (isInDanger && playerAPI->IsPlayingStageTrack())
        {
            playerAPI->onDanger();
        }
    }

    if (LastRoundNumber != round && playerAPI->IsPlayingStageTrack())
    {
        playerAPI->onRoundChanged(round);
        LastRoundNumber = round;
    }
    return false;
}

void JukeboxAPI::HandleShortcuts()
{
    PERF_TRACKING("JukeboxAPI::HandleShortcuts", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);

    if (!gameAPI->HasFocus())
    {
        return;
    }

    if (GetAsyncKeyState(settings->Shortcuts.VolumeUp) == 0 && WasVolumeUpPressed)
    {
        playerAPI->saveSettings();
        WasVolumeUpPressed = false;
    }

    if (GetAsyncKeyState(settings->Shortcuts.VolumeDown) == 0 && WasVolumeDownPressed)
    {
        playerAPI->saveSettings();
        WasVolumeDownPressed = false;
    }

    if (GetAsyncKeyState(settings->Shortcuts.NextTrack) & 0x8000)
    {
        WasNextTrackPressed = true;
    }

    if (GetAsyncKeyState(settings->Shortcuts.PreviousTrack) & 0x8000)
    {
        WasPreviousTrackPressed = true;
    }

    if (GetAsyncKeyState(settings->Shortcuts.Restart) & 0x8000)
    {
        WasRestartPressed = true;
    }

    if (GetAsyncKeyState(settings->Shortcuts.ShowOverlay) & 0x8000)
    {
        WasShowOverlayPressed = true;
    }

    if (GetAsyncKeyState(settings->Shortcuts.VolumeUp) & 0x8000)
    {
        playerAPI->increaseVolume();
        WasVolumeUpPressed = true;
    }

    if (GetAsyncKeyState(settings->Shortcuts.VolumeDown) & 0x8000)
    {
        playerAPI->decreaseVolume();
        WasVolumeDownPressed = true;
    }

    if (GetAsyncKeyState(settings->Shortcuts.NextTrack) == 0 && WasNextTrackPressed)
    {
        if (playerAPI->IsPlayingTrack())
        {
            playerAPI->playNextTrack();
        }

        WasNextTrackPressed = false;
    }

    if (GetAsyncKeyState(settings->Shortcuts.PreviousTrack) == 0 && WasPreviousTrackPressed)
    {
        if (playerAPI->IsPlayingTrack())
        {
            playerAPI->playPreviousTrack();
        }

        WasPreviousTrackPressed = false;
    }

    if (GetAsyncKeyState(settings->Shortcuts.Restart) == 0 && WasRestartPressed)
    {
        if (playerAPI->IsPlayingTrack())
        {
            playerAPI->setPosition(0);
        }

        WasRestartPressed = false;
    }

    if (GetAsyncKeyState(settings->Shortcuts.ShowOverlay) == 0 && WasShowOverlayPressed)
    {
        if (Settings::GetInstance()->IsOverlayEnabled() && playerAPI->IsPlayingTrack())
        {
            playerAPI->showOverlay();
        }

        WasShowOverlayPressed = false;
    }
}

void JukeboxAPI::MuteBGM()
{
    PERF_TRACKING("JukeboxAPI::MuteBGM", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    gameAPI->SetMusicVolume(-100);
    if (!CurrentContext.IsMuted)
    {
        CurrentContext.IsMuted = true;
        LogInfo("Muting BGM");
    }
}

void JukeboxAPI::UnMuteBGM()
{
    PERF_TRACKING("JukeboxAPI::UnMuteBGM", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    if (settings->MusicOverride == Settings::MusicOverrideMode::Mixed)
    {
        gameAPI->SetMusicVolume(GameVolumeLevel);

        if (CurrentContext.IsMuted)
        {
            CurrentContext.IsMuted = false;
            LogInfo("UnMuting BGM");
        }
    }
}
