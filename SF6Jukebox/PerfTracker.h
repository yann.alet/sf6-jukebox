#pragma once
#include <chrono>
#include <string>
#include "Logger.h"

class PerfTracker
{
private :
	std::chrono::system_clock::time_point Start;
	std::string Name;
	int WarningThreshold;

public:
	PerfTracker(std::string name, int warningThreshold) 
		: Name(name),
		  WarningThreshold(warningThreshold)
	{
		Start = std::chrono::system_clock::now();
	}

	~PerfTracker()
	{
		auto now = std::chrono::system_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::microseconds>(now - Start).count();
		if (duration > WarningThreshold * 1000)
		{
			Logger::GetInstance()->LogWarning("Slow performance in " + Name + ", time: "+ std::to_string(duration/1000.0f) + " ms");
		}
	}
};

#define DEFAULT_PERFORMANCE_WARNING_THRESHOLD 2
#define PERF_TRACKING(NAME, THRESHOLD_IN_MILLISECONDS) auto tracker = PerfTracker(NAME, THRESHOLD_IN_MILLISECONDS);