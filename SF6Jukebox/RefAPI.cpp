#pragma once

#include "pch.h"
#include "RefAPI.h"
#include "include/reframework/API.hpp"
#include "Logger.h"
#include <wchar.h>
#include <string>
#include <optional>
#include <sstream>


namespace RefAPI
{
	namespace Utils
	{
		std::string ToNarrow(const wchar_t* s, char dfault, const std::locale& loc)
		{
			std::ostringstream stm;

			while (*s != L'\0') {
				stm << std::use_facet< std::ctype<wchar_t> >(loc).narrow(*s++, dfault);
			}
			return stm.str();
		}

		reframework::API::TypeDefinition* find_type_definition(std::string_view type_name)
		{
			auto& api = reframework::API::get();
			const auto tdb = api->tdb();

			if (tdb == nullptr) {
				return nullptr;
			}

			return tdb->find_type(type_name);
		}

		template<typename T>
		T* get_native_field(void* obj, reframework::API::TypeDefinition* t, std::string_view name, bool is_value_type) {
			const auto field = t->find_field(name);

			if (field == nullptr) {
				// spdlog::error("Cannot find {:s}", name.data());
				return nullptr;
			}

			return (T*)field->get_data_raw(obj, is_value_type);
		}

		template<typename T>
		T* get_static_field(std::string_view type_name, std::string_view name, bool is_value_type) {
			const auto t = find_type_definition(type_name);

			if (t == nullptr) {
				// spdlog::error("Cannot find type {:s}", type_name.data());
				return nullptr;
			}

			return get_native_field<T>((void*)nullptr, t, name, is_value_type);
		}
	}

	std::string GetString(reframework::API::ManagedObject* instance)
	{
		auto stringLength = TryCallFunction<int>(instance, "get_Length");
		int size = *stringLength + 1; // +1 for null terminator.
		wchar_t* data = new wchar_t[size];
		memcpy(data, instance + 0x14, sizeof(wchar_t) * size);
		std::string str = Utils::ToNarrow(data);
		delete[] data;
		return str;
	}

	std::optional<std::string> GetGameObjectName(reframework::API::ManagedObject* gameObject)
	{
		auto gameObjectName = RefAPI::TryCallFunction<RefManagedObject*>(gameObject, "get_Name");
		if (gameObjectName)
		{
			return { RefAPI::GetString(*gameObjectName) };
		}
		else
		{
			LogError("Failed to get gameobject name.");
		}

		return std::nullopt;
	}

	std::optional<reframework::API::ManagedObject*> TryCreateInstance(std::string instanceTypeName)
	{
		auto& api = reframework::API::get();
		const auto tdb = api->tdb();
		auto instanceType = tdb->find_type(instanceTypeName);
		if (instanceType)
		{
			auto instance = instanceType->create_instance();
			return { instance };
		}
		
		return std::nullopt;
	}

	/*reframework::InvokeRet invoke_object_func(void* obj, reframework::API::TypeDefinition* t, std::string_view name, const std::vector<void*>& args) {
		const auto method = t->find_method(name);

		if (method == nullptr) {
			return reframework::InvokeRet{};
		}

		return method->invoke(obj, args);
	}

	reframework::InvokeRet invoke_object_func(reframework::API::ManagedObject* obj, std::string_view name, const std::vector<void*>& args) {
		return invoke_object_func((void*)obj, obj->get_type_definition(), name, args);
	}*/

	RefManagedObject* create_managed_string(std::wstring_view str)
	{
		auto& api = reframework::API::get();
		static auto empty_string = *Utils::get_static_field<RefManagedObject*>("System.String", "Empty");
		static std::vector<uint8_t> huge_string_data{};

		if (huge_string_data.empty()) 
		{
			huge_string_data.resize(0x10 + 4 + 2048);
			memset(&huge_string_data[0], 0, huge_string_data.size());
			auto huge_string = (SystemString*)&huge_string_data[0];
			memcpy(huge_string, empty_string, 0x10);
		}

		const auto str_len = str.length();
		auto huge_string = (SystemString*)&huge_string_data[0];		
		huge_string->size = (int32_t)str_len;		
		auto out = TryCallFunction<SystemString*>(reinterpret_cast<RefManagedObject*>(huge_string), "Clone", {});		
		memcpy(out.value()->data, str.data(), str_len * sizeof(wchar_t));
		return reinterpret_cast<RefManagedObject*>(out.value());
	}
}
