#pragma once
#include "Constants.h"
#include "Logger.h"
#include "Character.h"
#include "BattleDescription.h"
#include "include/reframework/API.hpp"
#include <optional>
#include <map>
#include <set>

class GameAPI
{
public:
	GameAPI(GameAPI& other) = delete;
	void operator=(const GameAPI&) = delete;

	static GameAPI* GetInstance();

	void ClearSpectateBGM() const;
	void MuteInteractiveFightBGM() const;
	float GetMusicVolume() const;
	void SetMusicVolume(float volume) const;
	std::optional<std::string> TryGetBattleTrackName() const;
	Game::FlowScene GetFlowScene() const;
	BattleDescription GetBattleDescription() const;
	std::string GetCurrentFlow() const;
	Fight::State GetFightState() const;
	Character GetCharacter(int playerIndex) const;
	int GetRound() const;
	Game::Mode GetGameMode() const;
	Game::Screen GetScreen(Game::FlowScene flowScene, Game::Mode gameMode, Fight::State state);
	Fight::Stage GetStage(Game::FlowScene flowScene, Game::Mode gameMode, Fight::State state) const;
	Fight::TrackType GetTrackType(std::string battleTrackName) const;
	Fight::Stage GetStageFromBattleTrackName(std::string battleTrackName) const;
	Fight::PlayerType GetCharacterFromBattleTrackName(std::string battleTrackName) const;
	bool HasFocus() const;
	void ClearBGM();
	void DeleteBGM();
	void DeletePreloadedTracks();
	void DeletePlayedTracks();
	void StopBGM();
	bool PlayStageBGM(Fight::Stage stage);
	bool PlayCharacterBGM(Fight::PlayerType character);
	bool IsPlayingBGM = false;

	std::set<Game::FlowScene> WorldTourScenes
	{
		Game::FlowScene::eWorldTourCity,
		Game::FlowScene::eWorldTourEngageBattleReady,
		Game::FlowScene::eWorldTourBattle,
		Game::FlowScene::eBattleHubCharacterCreateShop,
		Game::FlowScene::eWorldTourCharacterCreateShop,
		Game::FlowScene::eWorldTourMatchUpBattleReady,
		Game::FlowScene::eWorldTourEngageBattleOut
	};

	std::set<Game::FlowScene> FightFlowScenes
	{
		Game::FlowScene::eTrainingSetup,
		Game::FlowScene::eBattleMain,
		Game::FlowScene::eWorldTourBattle,
		Game::FlowScene::eSpectateAssetReload,
		Game::FlowScene::eWorldTourEngageBattleReady,
		Game::FlowScene::eWorldTourEngageBattleOut
	};	

protected:
	GameAPI();
	bool PlayBGM(int index, reframework::API::ManagedObject* track);
	reframework::API::ManagedObject* LoadBGM(std::wstring_view path);
	static GameAPI* instance;

private:

	std::set<int> _playedTracks;
	std::map<Fight::PlayerType, reframework::API::ManagedObject*> _preLoadedCharacterTracks;
	std::map<Fight::Stage, reframework::API::ManagedObject*> _preLoadedStageTracks;

	std::map<Fight::Stage, std::wstring_view> _stageTracks
	{
		{Fight::Stage::BarmaleySteelworks, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_stage/bgm_battle_ess0500_00.pfb"},
		{Fight::Stage::BathersBeach, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_stage/bgm_battle_ess0900_00.pfb"},
		{Fight::Stage::CarrierByronTaylor, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_stage/bgm_battle_ess0401_00.pfb"},
		{Fight::Stage::Colosseo, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_stage/bgm_battle_ess1200_00.pfb"},
		{Fight::Stage::DhalsimerTemple, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_stage/bgm_battle_ess0700_00.pfb"},
		{Fight::Stage::FeteForaine, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_stage/bgm_battle_ess0600_00.pfb"},
		{Fight::Stage::GenbuTemple, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_stage/bgm_battle_ess0200_00.pfb"},
		{Fight::Stage::KingStreet, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_stage/bgm_battle_ess1000_00.pfb"},
		{Fight::Stage::MetroCityDowntown, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_stage/bgm_battle_ess0400_00.pfb"},
		{Fight::Stage::OldTownMarket, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_stage/bgm_battle_ess0100_00.pfb"},
		{Fight::Stage::RangersHut, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_stage/bgm_battle_ess0800_00.pfb"},
		{Fight::Stage::SuvalhalArena, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_stage/bgm_battle_ess0101_00.pfb"},
		{Fight::Stage::TheMachoRing, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_stage/bgm_battle_ess4000_00.pfb"},
		{Fight::Stage::ThunderfootSettlement, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_stage/bgm_battle_ess1100_00.pfb"},
		{Fight::Stage::TianHongYuan, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_stage/bgm_battle_ess0300_00.pfb"},
		{Fight::Stage::RuinedLab, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_stage/bgm_battle_ess1300_00.pfb" },
		{Fight::Stage::EnmaHollow, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_stage/bgm_battle_ess0201_00.pfb" },
		{Fight::Stage::PaoPaoCafe, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_stage/bgm_battle_ess0410_00.pfb" },
		{Fight::Stage::TrainingRoom, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_stage/bgm_battle_ess0000_00.pfb"},
		{Fight::Stage::WorldTour, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_city_battle/bgm_battle_wtc0400_001.pfb"},
	};

	std::map<Fight::PlayerType, std::wstring_view> _characterTracks
	{
		{ Fight::PlayerType::Ed, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf019.pfb" },
		{ Fight::PlayerType::Aki, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf013.pfb" },
		{ Fight::PlayerType::Blanka, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf015.pfb" },
		{ Fight::PlayerType::Cammy, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf009.pfb" },
		{ Fight::PlayerType::ChunLi, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf004.pfb" },
		{ Fight::PlayerType::DeeJay, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf011.pfb" },
		{ Fight::PlayerType::Dhalsim, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf008.pfb" },
		{ Fight::PlayerType::Guile, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf018.pfb" },
		{ Fight::PlayerType::Honda, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf020.pfb" },
		{ Fight::PlayerType::Jamie, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf021.pfb" },
		{ Fight::PlayerType::JP, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf007.pfb" },
		{ Fight::PlayerType::Juri, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf016.pfb" },
		{ Fight::PlayerType::Ken, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf010.pfb" },
		{ Fight::PlayerType::Kimberly, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf003.pfb" },
		{ Fight::PlayerType::Lily, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf012.pfb" },
		{ Fight::PlayerType::Luke, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf002.pfb" },
		{ Fight::PlayerType::Manon, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf005.pfb" },
		{ Fight::PlayerType::Marisa, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf017.pfb" },
		{ Fight::PlayerType::Rashid, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf014.pfb" },
		{ Fight::PlayerType::Ryu, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf001.pfb" },
		{ Fight::PlayerType::Zangief, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf006.pfb" },
		{ Fight::PlayerType::Akuma, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf022.pfb" },
		{ Fight::PlayerType::Bison, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf026.pfb" },
		{ Fight::PlayerType::Terry, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf027.pfb" },
		{ Fight::PlayerType::Mai, L"product/sound/sound_prefab/snd_prefab_bgm/snd_bgm_fighter/bgm_battle_esf028.pfb" }
	};		
};

