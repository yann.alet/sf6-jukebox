#pragma once
#include "Constants.h"
#include <string>
#include <vector>
struct Character
{
public:
	Character(Fight::PlayerType type, int style) : Type(type), FightStyle(style) 
	{};

	Fight::PlayerType Type;
	short FightStyle;
	bool IsInDanger = false;
	bool Equals(const Character& c) { return c.FightStyle == FightStyle && c.Type == Type; }
	std::string ToString() { return std::string("Type: ") + std::to_string(Type) + std::string(" - FightStyle: ") + std::to_string(FightStyle); }

	//todo make this more generic to support more characters;
	bool ShouldOverrideTrack() { return Type == Fight::PlayerType::Kimberly && FightStyle >= 8;	}
	Game::CustomScreen GetSimpleTrackOverrideScreenId() { return Game::CustomScreen::KimberlyLvl3; }
	Game::CustomScreen GetBothTrackOverrideScreenId() { return Game::CustomScreen::KimberlyLvl3Both; }

	static std::vector<Game::CustomScreen> GetTrackOverrideScreenIds() {
		return std::vector<Game::CustomScreen>
		{Game::CustomScreen::KimberlyLvl3, Game::CustomScreen::KimberlyLvl3Both};
	};

	static bool IsTrackOverrideScreenId(Game::CustomScreen screenId) {
		for (Game::CustomScreen s : GetTrackOverrideScreenIds())
		{
			if (s == screenId)
			{
				return true;
			}
		}
		return false;
	};
};

