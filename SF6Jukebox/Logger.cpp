#include "pch.h"
#include "Logger.h"
#include <Windows.h>
#include "include/reframework/API.hpp"
#include "PlayerWrapper.h"
#include "ModuleLoader.h"


Logger* Logger::instance = nullptr;

Logger::Logger()
{
    RegisterLogEvents();
}


void OnLog(CLogEventArgs args)
{
    Logger::GetInstance()->LogInfo(std::string(args.Message));
}

void Logger::LogInfo(std::string message)
{
	auto& api = reframework::API::get();
    api->log_info((std::string("[SF6Jukebox] - ")+ message).c_str());
}

void Logger::LogWarning(std::string message)
{
	auto& api = reframework::API::get();
	api->log_warn((std::string("[SF6Jukebox] - ") + message).c_str());
}

void Logger::LogError(std::string message)
{
	auto& api = reframework::API::get();
	api->log_error((std::string("[SF6Jukebox] - ") + message).c_str());
}

Logger* Logger::GetInstance()
{
    if (instance == nullptr)
    {
        instance = new Logger();
    }

    return instance;
}

void Logger::RegisterLogEvents()
{
    auto& api = reframework::API::get();
    typedef void(*FunctionPtr)(void*);
    FunctionPtr func = reinterpret_cast<FunctionPtr>(GetProcAddress(ModuleLoader::GetInstance()->GetHandle(), "subscribe_onLog"));
    if (!func)
    {
        LogError("Failed to get the function address subscribe_onLog");
    }
    else
    {
        func(&OnLog);
    }
}
