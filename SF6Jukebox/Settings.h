#pragma once
#include "tinyxml2.h"
#include "Constants.h"
#include <filesystem>
#include <map>

class Settings
{
public:
	Settings(Settings& other) = delete;
	void operator=(const Settings&) = delete;

	static Settings* GetInstance();

	bool IsEnabled() { return mIsEnabled; }
	bool IsOverlayEnabled (){ return mIsOverlayEnabled; }

	struct KeyBindings
	{
		int NextTrack;
		int PreviousTrack;
		int VolumeUp;
		int VolumeDown;
		int Restart;
		int ShowOverlay;

	} Shortcuts;

	enum PlaylistPriority : int
	{
		Random = 0,
		Stage = 1,
		P1 = 2,
		P2 = 3,
		Player = 4,
		Opponent = 5
	} FightPlaylistPriority;

	enum MusicOverrideMode : int
	{
		Mixed = 0,
		Full = 1
	} MusicOverride;

	std::map<Game::FlowScene, bool> FlowSceneEnabledStates;
	std::map<Game::CustomScreen, bool> CustomScreenEnabledStates;
	std::map<Fight::Stage, bool> StageEnabledStates;
	std::map<Fight::PlayerType, bool> CharacterEnabledStates;

protected:
	Settings();

	static Settings* instance;

private:

	enum Version
	{
		Older,
		Unknown,
		v1_21_0,
		v1_22_0,
		v1_23_0,
		v1_24_0,
		v1_25_0,
		v1_25_1,
		v1_25_2,
		v1_26_0,
		v1_26_1,
		v1_26_2,
		v1_26_3,
		v1_26_4,
		v1_26_5,
		v1_26_6,
		v1_26_7,
		v1_26_8,
		v1_26_9,
		v1_26_10,
		v1_26_11,
		v1_26_12,
		v1_26_13
	};

	std::filesystem::path GetFilePath();

	Version GetVersion();
	void LoadDefault();
	void Load_1_21_0();
	void Load_1_22_0();
	void Load_1_23_0();
	void Load_1_24_0();
	void Load_1_25_0();
	void Load_1_25_1();
	void Load_1_25_2();
	void Load_1_26_0();
	void Load_1_26_1();
	void Load_1_26_2();
	void Load_1_26_3();
	void Load_1_26_4();
	void Load_1_26_5();
	void Load_1_26_6();
	void Load_1_26_7();
	void Load_1_26_8();
	void Load_1_26_9();
	void Load_1_26_10();
	void Load_1_26_11();
	void Load_1_26_12();
	void Load_1_26_13();
	bool mIsEnabled = false;
	bool mIsOverlayEnabled = false;
};

