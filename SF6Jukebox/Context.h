#pragma once
#include "Constants.h"

struct Context
{
	bool CustomTrackForFight = false;
	bool IsSpectating = false;
	bool IsMuted = false;
	Game::Screen LastScreen = Game::Screen::Screen_Unkown;
};

