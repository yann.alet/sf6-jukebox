#include "pch.h"
#include "Settings.h"
#include "Logger.h"
#include "ModuleLoader.h"
#include <string>


Settings* Settings::instance = nullptr;

Settings::Settings()
{
	switch (GetVersion())
	{
		case Settings::Version::Unknown:
			LogInfo("Unknown settings version.");
			LoadDefault();
			break;

		case Settings::Version::Older:
			LogInfo("Loading older settings.");
			LoadDefault();
			break;

		case Settings::Version::v1_21_0:
			LogInfo("Loading settings v1.21.0");
			Load_1_21_0();
			break;

		case Settings::Version::v1_22_0:
			LogInfo("Loading settings v1.22.0");
			Load_1_22_0();
			break;

		case Settings::Version::v1_23_0:
			LogInfo("Loading settings v1.23.0");
			Load_1_23_0();
			break;

		case Settings::Version::v1_24_0:
			LogInfo("Loading settings v1.24.0");
			Load_1_24_0();
			break;

		case Settings::Version::v1_25_0:
			LogInfo("Loading settings v1.25.0");
			Load_1_25_0();
			break;

		case Settings::Version::v1_25_1:
			LogInfo("Loading settings v1.25.1");
			Load_1_25_1();
			break;

		case Settings::Version::v1_25_2:
			Logger::GetInstance()->LogInfo("Loading settings v1.25.2");
			Load_1_25_2();
			break;
			
		case Settings::Version::v1_26_0:
			LogInfo("Loading settings v1.26.0");
			Load_1_26_0();
			break;

		case Settings::Version::v1_26_1:
			LogInfo("Loading settings v1.26.1");
			Load_1_26_1();
			break;

		case Settings::Version::v1_26_2:
			LogInfo("Loading settings v1.26.2");
			Load_1_26_2();
			break;

		case Settings::Version::v1_26_3:
			LogInfo("Loading settings v1.26.3");
			Load_1_26_3();
			break;

		case Settings::Version::v1_26_4:
			LogInfo("Loading settings v1.26.4");
			Load_1_26_4();
			break;
		case Settings::Version::v1_26_5:
			LogInfo("Loading settings v1.26.5");
			Load_1_26_5();
			break;
		case Settings::Version::v1_26_6:
			LogInfo("Loading settings v1.26.6");
			Load_1_26_6();
			break;
		case Settings::Version::v1_26_7:
			LogInfo("Loading settings v1.26.7");
			Load_1_26_7();
			break;
		case Settings::Version::v1_26_8:
			LogInfo("Loading settings v1.26.8");
			Load_1_26_8();
			break;
		case Settings::Version::v1_26_9:
			LogInfo("Loading settings v1.26.9");
			Load_1_26_9();
			break;
		case Settings::Version::v1_26_10:
			LogInfo("Loading settings v1.26.10");
			Load_1_26_10();
			break;
		case Settings::Version::v1_26_11:
			LogInfo("Loading settings v1.26.11");
			Load_1_26_11();
			break;
		case Settings::Version::v1_26_12:
			LogInfo("Loading settings v1.26.12");
			Load_1_26_12();
			break;
		case Settings::Version::v1_26_13:
			LogInfo("Loading settings v1.26.13");
			Load_1_26_13();
			break;
	}
}

std::filesystem::path Settings::GetFilePath()
{
	return std::filesystem::path{ ModuleLoader::GetInstance()->GetModulePath() } / std::filesystem::path{ "settings.xml" };	
}

Settings::Version Settings::GetVersion()
{
	std::filesystem::path settingsFilePath = GetFilePath();
	if (std::filesystem::exists(settingsFilePath))
	{
		auto doc = new tinyxml2::XMLDocument();
		doc->LoadFile(settingsFilePath.string().c_str());
		tinyxml2::XMLElement* settingsElement = doc->FirstChildElement("Settings");
		tinyxml2::XMLElement* versionElement = settingsElement->FirstChildElement("Version");
		if (versionElement)
		{
			std::string VersionString = std::string(versionElement->GetText());
			if (VersionString == "1.21.0")
			{
				return Settings::Version::v1_21_0;
			}
			else if (VersionString == "1.22.0")
			{
				return Settings::Version::v1_22_0;
			}
			else if (VersionString == "1.23.0")
			{
				return Settings::Version::v1_23_0;
			}
			else if (VersionString == "1.24.0")
			{
				return Settings::Version::v1_24_0;
			}
			else if (VersionString == "1.25.0")
			{
				return Settings::Version::v1_25_0;
			}
			else if (VersionString == "1.25.1")
			{
				return Settings::Version::v1_25_1;
			}
			else if (VersionString == "1.25.2")
			{
				return Settings::Version::v1_25_2;
		    }
			else if (VersionString == "1.26.0")
			{
				return Settings::Version::v1_26_0;
			}
			else if (VersionString == "1.26.1")
			{
				return Settings::Version::v1_26_1;
			}
			else if (VersionString == "1.26.2")
			{
				return Settings::Version::v1_26_2;
			}
			else if (VersionString == "1.26.3")
			{
				return Settings::Version::v1_26_3;
			}
			else if (VersionString == "1.26.4")
			{
				return Settings::Version::v1_26_4;
			}
			else if (VersionString == "1.26.5")
			{
				return Settings::Version::v1_26_5;
			}
			else if (VersionString == "1.26.6")
			{
				return Settings::Version::v1_26_6;
			}
			else if (VersionString == "1.26.7")
			{
				return Settings::Version::v1_26_7;
			}
			else if (VersionString == "1.26.8")
			{
				return Settings::Version::v1_26_8;
			}
			else if (VersionString == "1.26.9")
			{
				return Settings::Version::v1_26_9;
			}
			else if (VersionString == "1.26.10")
			{
				return Settings::Version::v1_26_10;
			}
			else if (VersionString == "1.26.11")
			{
				return Settings::Version::v1_26_11;
			}
			else if (VersionString == "1.26.12")
			{
				return Settings::Version::v1_26_12;
			}
			else if (VersionString == "1.26.13")
			{
				return Settings::Version::v1_26_13;
			}
			else
			{
				return Settings::Version::Unknown;
			}
		}
		else
		{
			return Settings::Version::Older;
		}
	}
	else
	{
		return Settings::Version::Unknown;
	}
}

void Settings::LoadDefault()
{
	mIsEnabled = true;
	Shortcuts.NextTrack = 33;
	Shortcuts.PreviousTrack = 34;
	Shortcuts.VolumeUp = 187;
	Shortcuts.VolumeDown = 189;

	CharacterEnabledStates[Fight::PlayerType::Terry] = true;
	CharacterEnabledStates[Fight::PlayerType::Bison] = true;
	CharacterEnabledStates[Fight::PlayerType::Aki] = true;
	CharacterEnabledStates[Fight::PlayerType::Ed] = true;
	CharacterEnabledStates[Fight::PlayerType::Akuma] = true;
	CharacterEnabledStates[Fight::PlayerType::Blanka] = true;
	CharacterEnabledStates[Fight::PlayerType::Cammy] = true;
	CharacterEnabledStates[Fight::PlayerType::ChunLi] = true;
	CharacterEnabledStates[Fight::PlayerType::DeeJay] = true;
	CharacterEnabledStates[Fight::PlayerType::Dhalsim] = true;
	CharacterEnabledStates[Fight::PlayerType::Guile] = true;
	CharacterEnabledStates[Fight::PlayerType::Honda] = true;
	CharacterEnabledStates[Fight::PlayerType::Jamie] = true;
	CharacterEnabledStates[Fight::PlayerType::JP] = true;
	CharacterEnabledStates[Fight::PlayerType::Juri] = true;
	CharacterEnabledStates[Fight::PlayerType::Ken] = true;
	CharacterEnabledStates[Fight::PlayerType::Kimberly] = true;
	CharacterEnabledStates[Fight::PlayerType::Lily] = true;
	CharacterEnabledStates[Fight::PlayerType::Luke] = true;
	CharacterEnabledStates[Fight::PlayerType::Manon] = true;
	CharacterEnabledStates[Fight::PlayerType::Marisa] = true;
	CharacterEnabledStates[Fight::PlayerType::Rashid] = true;
	CharacterEnabledStates[Fight::PlayerType::Ryu] = true;
	CharacterEnabledStates[Fight::PlayerType::Zangief] = true;
	CharacterEnabledStates[Fight::PlayerType::Mai] = true;

	StageEnabledStates[Fight::Stage::BarmaleySteelworks] = true;
	StageEnabledStates[Fight::Stage::BathersBeach] = true;
	StageEnabledStates[Fight::Stage::CarrierByronTaylor] = true;
	StageEnabledStates[Fight::Stage::Colosseo] = true;
	StageEnabledStates[Fight::Stage::DhalsimerTemple] = true;
	StageEnabledStates[Fight::Stage::FeteForaine] = true;
	StageEnabledStates[Fight::Stage::GenbuTemple] = true;
	StageEnabledStates[Fight::Stage::KingStreet] = true;
	StageEnabledStates[Fight::Stage::MetroCityDowntown] = true;
	StageEnabledStates[Fight::Stage::OldTownMarket] = true;
	StageEnabledStates[Fight::Stage::RangersHut] = true;
	StageEnabledStates[Fight::Stage::SuvalhalArena] = true;
	StageEnabledStates[Fight::Stage::TheMachoRing] = true;
	StageEnabledStates[Fight::Stage::ThunderfootSettlement] = true;
	StageEnabledStates[Fight::Stage::TianHongYuan] = true;
	StageEnabledStates[Fight::Stage::RuinedLab] = true;
	StageEnabledStates[Fight::Stage::EnmaHollow] = true;
	StageEnabledStates[Fight::Stage::PaoPaoCafe] = true;
	StageEnabledStates[Fight::Stage::TrainingRoom] = true;
	StageEnabledStates[Fight::Stage::WorldTour] = true;
}

void Settings::Load_1_21_0()
{
	auto doc = new tinyxml2::XMLDocument();
	std::filesystem::path settingsFilePath = GetFilePath();
	if (std::filesystem::exists(settingsFilePath))
	{
		doc->LoadFile(settingsFilePath.string().c_str());
		tinyxml2::XMLElement* settingsElement = doc->FirstChildElement("Settings");
		tinyxml2::XMLElement* enabledElement = settingsElement->FirstChildElement("Enabled");
		tinyxml2::XMLElement* shortcutsElement = settingsElement->FirstChildElement("Shortcuts");
		tinyxml2::XMLElement* nextTrackElement = shortcutsElement->FirstChildElement("NextTrack");
		tinyxml2::XMLElement* previousTrackElement = shortcutsElement->FirstChildElement("PreviousTrack");
		tinyxml2::XMLElement* volumeUpElement = shortcutsElement->FirstChildElement("VolumeUp");
		tinyxml2::XMLElement* volumeDownElement = shortcutsElement->FirstChildElement("VolumeDown");
		Shortcuts.NextTrack = atoi(nextTrackElement->GetText());
		Shortcuts.PreviousTrack = atoi(previousTrackElement->GetText());
		Shortcuts.VolumeUp = atoi(volumeUpElement->GetText());
		Shortcuts.VolumeDown = atoi(volumeDownElement->GetText());
		mIsEnabled = atoi(enabledElement->GetText()) == 1;
	}
}

void Settings::Load_1_22_0()
{
	Load_1_21_0();
	auto doc = new tinyxml2::XMLDocument();
	std::filesystem::path settingsFilePath = GetFilePath();
	if (std::filesystem::exists(settingsFilePath))
	{
		doc->LoadFile(settingsFilePath.string().c_str());
		tinyxml2::XMLElement* settingsElement = doc->FirstChildElement("Settings");
		tinyxml2::XMLElement* flowScenesElement = settingsElement->FirstChildElement("FlowScenes");
		tinyxml2::XMLNode* flowSceneElement = flowScenesElement->FirstChildElement("FlowScene");
		do
		{
			auto flowSceneIdElement = flowSceneElement->FirstChildElement("Id");	
			auto flowSceneEnabledElement = flowSceneElement->FirstChildElement("Enabled");
			int flowSceneId = atoi(flowSceneIdElement->GetText());
			int enabled = atoi(flowSceneEnabledElement->GetText());
			FlowSceneEnabledStates[(Game::FlowScene)flowSceneId] = enabled == 1;
		} while ((flowSceneElement = flowSceneElement->NextSibling()) != nullptr);
		
	}
}

void Settings::Load_1_23_0()
{
	Load_1_22_0();
}

void Settings::Load_1_24_0()
{
	Load_1_23_0();
}

void Settings::Load_1_25_0()
{
	Load_1_24_0();
	auto doc = new tinyxml2::XMLDocument();
	std::filesystem::path settingsFilePath = GetFilePath();
	if (std::filesystem::exists(settingsFilePath))
	{
		doc->LoadFile(settingsFilePath.string().c_str());
		tinyxml2::XMLElement* settingsElement = doc->FirstChildElement("Settings");
		tinyxml2::XMLElement* overlayEnabledElement = settingsElement->FirstChildElement("OverlayEnabled");
		tinyxml2::XMLElement* playlistPriorityElement = settingsElement->FirstChildElement("PlaylistPriority");
		tinyxml2::XMLElement* overrideMenusTracksElement = settingsElement->FirstChildElement("OverrideMenusTracks");
		tinyxml2::XMLElement* shortcutsElement = settingsElement->FirstChildElement("Shortcuts");
		tinyxml2::XMLElement* showOverlayElement = shortcutsElement->FirstChildElement("ShowOverlay");		
		tinyxml2::XMLElement* restartElement = shortcutsElement->FirstChildElement("Restart");
		Shortcuts.ShowOverlay = atoi(showOverlayElement->GetText());
		Shortcuts.Restart = atoi(restartElement->GetText());
		mIsOverlayEnabled = atoi(overlayEnabledElement->GetText());
		FightPlaylistPriority = static_cast<PlaylistPriority>(atoi(playlistPriorityElement->GetText()));
	}
}

void Settings::Load_1_25_1()
{
	Load_1_25_0();
}

void Settings::Load_1_25_2()
{
	Load_1_25_0();
}

void Settings::Load_1_26_0()
{
	Load_1_24_0();
	std::vector screens = {"Character", "CustomScreen", "Stage"};
	for (const auto& screen : screens)
	{
		auto doc = new tinyxml2::XMLDocument();
		std::filesystem::path settingsFilePath = GetFilePath();
		if (std::filesystem::exists(settingsFilePath))
		{
			doc->LoadFile(settingsFilePath.string().c_str());
			tinyxml2::XMLElement* settingsElement = doc->FirstChildElement("Settings");
			tinyxml2::XMLElement* overlayEnabledElement = settingsElement->FirstChildElement("OverlayEnabled");
			tinyxml2::XMLElement* playlistPriorityElement = settingsElement->FirstChildElement("PlaylistPriority");
			tinyxml2::XMLElement* musicOverrideModeElement = settingsElement->FirstChildElement("MusicOverrideMode");
			tinyxml2::XMLElement* shortcutsElement = settingsElement->FirstChildElement("Shortcuts");
			tinyxml2::XMLElement* showOverlayElement = shortcutsElement->FirstChildElement("ShowOverlay");
			tinyxml2::XMLElement* restartElement = shortcutsElement->FirstChildElement("Restart");
			Shortcuts.ShowOverlay = atoi(showOverlayElement->GetText());
			Shortcuts.Restart = atoi(restartElement->GetText());
			mIsOverlayEnabled = atoi(overlayEnabledElement->GetText());
			FightPlaylistPriority = static_cast<PlaylistPriority>(atoi(playlistPriorityElement->GetText()));
			MusicOverride = static_cast<Settings::MusicOverrideMode>(atoi(musicOverrideModeElement->GetText()));

			tinyxml2::XMLElement* screensElement = settingsElement->FirstChildElement(std::format("{}{}", screen, "s").c_str());
			tinyxml2::XMLNode* screenElement = screensElement->FirstChildElement(screen);
			do
			{
				auto screenIdElement = screenElement->FirstChildElement("Id");
				auto screenEnabledElement = screenElement->FirstChildElement("Enabled");
				int screenId = atoi(screenIdElement->GetText());
				int enabled = atoi(screenEnabledElement->GetText());
				if (screen == "Character")
				{
					CharacterEnabledStates[(Fight::PlayerType)screenId] = enabled == 1;
				}
				if (screen == "CustomScreen")
				{
					CustomScreenEnabledStates[(Game::CustomScreen)screenId] = enabled == 1;
				}
				if (screen == "Stage")
				{
					StageEnabledStates[(Fight::Stage)screenId] = enabled == 1;
				}
			} while ((screenElement = screenElement->NextSibling()) != nullptr);
		}
	}
}

void Settings::Load_1_26_1()
{
	Load_1_26_0();
}

void Settings::Load_1_26_2()
{
	Load_1_26_1();
}

void Settings::Load_1_26_3()
{
	Load_1_26_2();
}

void Settings::Load_1_26_4()
{
	Load_1_26_3();
}

void Settings::Load_1_26_5()
{
	Load_1_26_4();
}

void Settings::Load_1_26_6()
{
	Load_1_26_5();
}

void Settings::Load_1_26_7()
{
	Load_1_26_6();
}

void Settings::Load_1_26_8()
{
	Load_1_26_7();
}

void Settings::Load_1_26_9()
{
	Load_1_26_8();
}

void Settings::Load_1_26_10()
{
	Load_1_26_9();
}

void Settings::Load_1_26_11()
{
	Load_1_26_10();
}

void Settings::Load_1_26_12()
{
	Load_1_26_11();
}

void Settings::Load_1_26_13()
{
	Load_1_26_12();
}

Settings* Settings::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new Settings();
	}

	return instance;
}
