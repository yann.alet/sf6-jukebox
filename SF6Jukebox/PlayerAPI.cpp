#include "pch.h"
#include "PlayerAPI.h"
#include "include/reframework/API.hpp"
#include "Logger.h"
#include "ModuleLoader.h"
#include "PlayerWrapper.h"
#include "PerfTracker.h"

PlayerAPI* PlayerAPI::instance = nullptr;

bool PlayerAPI::callParameterLessFunction(std::string functionName)
{
    PERF_TRACKING(functionName, DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    auto& api = reframework::API::get();
    typedef void(*FunctionPtr)();
    FunctionPtr func = reinterpret_cast<FunctionPtr>(GetProcAddress(ModuleLoader::GetInstance()->GetHandle(), functionName.c_str()));
    if (!func)
    {
        Logger::GetInstance()->LogError(std::string("Failed to get the function address for: ") + functionName);
        return false;
    }
    else
    {
        func();
        return true;
    }
}

//todo: make this more generic
bool PlayerAPI::callFunction(std::string functionName, int parameter)
{
    PERF_TRACKING(functionName, DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    auto& api = reframework::API::get();
    typedef void(*FunctionPtr)(int);
    FunctionPtr func = reinterpret_cast<FunctionPtr>(GetProcAddress(ModuleLoader::GetInstance()->GetHandle(), functionName.c_str()));
    if (!func)
    {
        Logger::GetInstance()->LogError(std::string("Failed to get the function address for: ") + functionName);
        return false;
    }
    else
    {
        func(parameter);
        return true;
    }
}

bool PlayerAPI::callFunction2(std::string functionName, int parameter1, int parameter2)
{
    PERF_TRACKING(functionName, DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    auto& api = reframework::API::get();
    typedef void(*FunctionPtr)(int, int);
    FunctionPtr func = reinterpret_cast<FunctionPtr>(GetProcAddress(ModuleLoader::GetInstance()->GetHandle(), functionName.c_str()));
    if (!func)
    {
        Logger::GetInstance()->LogError(std::string("Failed to get the function address for: ") + functionName);
        return false;
    }
    else
    {
        func(parameter1, parameter2);
        return true;
    }
}

void PlayerAPI::playCharacterTrack(Fight::PlayerType playerId, int round)
{
    if (callFunction2("playCharacterTrack", playerId, round))
    {
        isPlayingCharacterTrack = true;
        isPlayingStageTrack = false;
        isPlayingFlowSceneTrack = false;
        isPlayingCustomScreenTrack = false;
        currentPlayingCharacter = playerId;
        currentPlayingFlowScene = Game::FlowScene::FlowScene_Unknown;
        currentPlayingCustomScreen = Game::CustomScreen::CustomScreen_Unknown;
    }
}

void PlayerAPI::playStageTrack(Fight::Stage stageId)
{
    if (callFunction("playStageTrack", stageId))
    {
        isPlayingStageTrack = true;
        isPlayingFlowSceneTrack = false;
        isPlayingCustomScreenTrack = false;
        isPlayingCharacterTrack = false;
        currentPlayingFlowScene = Game::FlowScene::FlowScene_Unknown;
        currentPlayingCustomScreen = Game::CustomScreen::CustomScreen_Unknown;
        currentPlayingCharacter = Fight::PlayerType::PlayerType_Unknown;
    }
}

void PlayerAPI::playFlowSceneTrack(Game::FlowScene flowSceneId)
{
    if (callFunction("playFlowSceneTrack", flowSceneId))
    {
        isPlayingStageTrack = false;
        isPlayingFlowSceneTrack = true;
        isPlayingCustomScreenTrack = false;
        isPlayingCharacterTrack = false;
        currentPlayingCustomScreen = Game::CustomScreen::CustomScreen_Unknown;
        currentPlayingCharacter = Fight::PlayerType::PlayerType_Unknown;
        currentPlayingFlowScene = flowSceneId;
    }
}

void PlayerAPI::playCustomScreenTrack(Game::CustomScreen customScreenId)
{
    if (callFunction("playCustomScreenTrack", customScreenId))
    {
        isPlayingStageTrack = false;
        isPlayingFlowSceneTrack = false;
        isPlayingCustomScreenTrack = true;
        isPlayingCharacterTrack = false;
        currentPlayingCustomScreen = customScreenId;
        currentPlayingFlowScene = Game::FlowScene::FlowScene_Unknown;
        currentPlayingCharacter = Fight::PlayerType::PlayerType_Unknown;
    }
}

void PlayerAPI::stopInternal()
{
    if (callParameterLessFunction("stop"))
    {
        currentPlayingCustomScreen = Game::CustomScreen::CustomScreen_Unknown;
        currentPlayingFlowScene = Game::FlowScene::FlowScene_Unknown;
        currentPlayingCharacter = Fight::PlayerType::PlayerType_Unknown;
        isPlayingStageTrack = false;
        isPlayingFlowSceneTrack = false;
        isPlayingCustomScreenTrack = false;
        isPlayingCharacterTrack = false;
    }
}

void PlayerAPI::setPosition(int position)
{
    callFunction("setPosition", position);
}

void PlayerAPI::onFightStateChanged(int fightState)
{
    callFunction("onFightStateChanged", fightState);
}

void PlayerAPI::onGameModeChanged(int gameMode)
{
    callFunction("onGameModeChanged", gameMode);
}

void PlayerAPI::onFlowSceneChanged(int flowScene)
{
    callFunction("onFlowSceneChanged", flowScene);
}

void PlayerAPI::onRoundChanged(int round)
{
    callFunction("onRoundChanged", round);
}

void PlayerAPI::onDanger()
{
    callParameterLessFunction("onDanger");
}

void PlayerAPI::saveSettings()
{
    callParameterLessFunction("saveSettings");
}

void PlayerAPI::showOverlay()
{
    callParameterLessFunction("showOverlay");
}

void PlayerAPI::transition()
{
    callParameterLessFunction("transition");
}

void PlayerAPI::queueInNextTrack()
{
    callParameterLessFunction("queueInNextTrack");
}

void PlayerAPI::increaseVolume()
{
    callParameterLessFunction("increaseVolume");
}

void PlayerAPI::decreaseVolume()
{
    callParameterLessFunction("decreaseVolume");
}

void PlayerAPI::playNextTrack()
{
    callParameterLessFunction("playNextTrack");
}

void PlayerAPI::playPreviousTrack()
{
    callParameterLessFunction("playPreviousTrack");
}

void PlayerAPI::resetStagePlaylist()
{
    callParameterLessFunction("resetStagePlaylist");
}

void PlayerAPI::resetFlowScenePlaylist()
{
    callParameterLessFunction("resetFlowScenePlaylist");
}

void PlayerAPI::resetCustomScreenPlaylist()
{
    callParameterLessFunction("resetCustomScreenPlaylist");
}

void PlayerAPI::resetCharacterPlaylist()
{
    callParameterLessFunction("resetCharacterPlaylist");
}

void PlayerAPI::enableOverlay()
{
    callParameterLessFunction("enableOverlay");
}

void PlayerAPI::disableOverlay()
{
    callParameterLessFunction("disableOverlay");
}

void PlayerAPI::destroy()
{
    callParameterLessFunction("destroy");
}

PlayerAPI* PlayerAPI::GetInstance()
{
    if (instance == nullptr)
    {
        instance = new PlayerAPI();
    }

    return instance;
}
