#pragma once
#include <stdint.h>

namespace Fight
{	
	enum TrackType : int
	{
		StageTrack = 0,
		CharacterTrack = 1
	};

	enum State : uint8_t
	{
		StageInit = 0,
		RoundInit = 1,
		Appear = 2,
		Ready = 3,
		Now = 4,
		WinWait = 5,
		Win = 6,
		Finish = 7
	};

	enum Stage : int
	{
		Stage_Unkown = -1,
		TrainingRoom = 0,
		WorldTour = 10,
		BarmaleySteelworks = 50000,
		BathersBeach = 90000,
		CarrierByronTaylor = 40100,
		Colosseo = 120000,
		DhalsimerTemple = 70000,
		FeteForaine = 60000,
		GenbuTemple = 20000,
		KingStreet = 100000,
		MetroCityDowntown = 40000,
		OldTownMarket = 10000,
		RangersHut = 80000,
		SuvalhalArena = 10100,
		TheMachoRing = 400000,
		ThunderfootSettlement = 110000,
		TianHongYuan = 30000,
		RuinedLab = 130000,
		EnmaHollow = 20100,
		PaoPaoCafe = 41000
	};

	enum PlayerType : int
	{
		NotSet = 0,
		Ryu = 1,
		Luke = 2,
		Kimberly = 3,
		ChunLi = 4,
		Manon = 5,
		Zangief = 6,
		JP = 7,
		Dhalsim = 8,
		Cammy = 9,
		Ken = 10,
		DeeJay = 11,
		Lily = 12,
		Aki = 13,
		Rashid = 14,
		Blanka = 15,
		Juri = 16,
		Marisa = 17,
		Guile = 18,
		Ed = 19,
		Honda = 20,
		Jamie = 21,
		Akuma = 22,
		Bison = 26,
		Terry = 27,
		Mai = 28,
		PlayerType_Unknown = -1
	};
}

namespace Game
{
	enum Screen : int
	{
		Menu = 0,
		WorldTour = 1,
		Fight = 2,
		Screen_Unkown = -1		
	};

	enum CustomScreen : int
	{
		ResultScreen = 0,
		KimberlyLvl3 = 1,
		KimberlyLvl3Both = 2,
		CustomScreen_Unknown = -1,
	};

	enum Mode : int
	{
		Mode_Unknown = -1,
		None = 0,
		Arcade = 1,
		Training = 2,
		Versus_2P = 3,
		Versus_CPU = 4,
		Tutorial = 5,
		Character_Guide = 6,
		Mission = 7,
		DeathMatch = 8,
		Story = 9,
		Story_Training = 10,
		Story_Match = 11,
		Story_Tutorial = 12,
		Story_Spectate = 13,
		Ranked_Match = 14,
		Player_Match = 15,
		Cabinet_Match = 16,
		Custom_Room_Match = 17,
		Online_Training = 18,
		TeamBattle = 19,
		Exam_Cpu_Match = 20,
		Cabinet_Cpu_Match = 21,
		Replay = 22,
		Spectate = 23,
		Mode_Max = 24
	};

	// List in app.constant.scn.Index
	enum FlowScene : int
	{
		eNone = 0,
		eBoot = 1,
		eBootSetup = 2,
		eTitle = 3,
		eESportsMainMenu = 4,
		eModeSelect = 5,
		eReturnModeSelect = 6,
		eBootedTransitionHub = 7,
		eBattleSideSelect = 8,
		eBattleStageSelect = 9,
		eBattleFighterSelect = 10,
		eBattleFighterLeave = 11,
		eBattleFighterVS = 12,
		eBattleFighterEmote = 13,
		eBattleMain = 14, // Fight is starting
		eDeathMatchRuleSelect = 15,
		eDeathMatchMain = 16,
		eTeamBattleRuleSelect = 17,
		eBattleMatchingIn = 18,
		eBattleMatchingOut = 19,
		eBattleAssetReload = 20,
		eTransitionCustomRoom = 21,
		eCustomRoomSetting = 22,
		eCustomRoomInit = 23,
		eCustomRoomInvite = 24,
		eCustomRoomLogin = 25,
		eCustomRoomFromBattleResult = 26,
		eCustomRoomMain = 27,
		eCustomRoomViewingMain = 28,
		eTeamBattleMatchingIn = 29,
		eExamCpuBattleSetup = 30,
		eOnlineCpuBattleSetup = 31,
		eReadyAsset = 32,
		eTrainingSetup = 33,
		eOnlineTrainingSetup = 34,
		eTutorialSetup = 35,
		eTutorialSelect = 36,
		eCharacterGuideSetup = 37,
		eCharacterGuideSelect = 38,
		eComboTrialSetup = 39,
		eComboTrialSelect = 40,
		eReplaySetup = 41,
		eFGReplaySetup = 42,
		eVersusBackReplay = 43,
		eSpectateSetup = 44,
		eSpectateAssetReload = 45,
		eVersusRule = 46,
		eModeSelectBattleHub = 47,
		eModeSelectWorldTour = 48,
		eModeSelectActivity = 49,
		eDeleteSaveDataWorldTour = 50,
		eWorldTourOpening = 51,
		eWorldTourContinue = 52,
		eWorldTourCharacterCreateOpening = 53,
		eWorldTourCharacterCreateShop = 54,
		eWorldTourCity = 55,
		eWorldTourEngageBattleReady = 56,
		eWorldTourEngageBattleOut = 57,
		eWorldTourMatchUpBattleReady = 58,
		eWorldTourMatchUpBattleOut = 59,
		eWorldTourBattle = 60,
		eWorldTourBattleSound = 61,
		eWorldTourMiniGame = 62,
		eWorldTourMiniGameBattle = 63,
		eWorldTourTutorial = 64,
		eWorldTourTutorialBattle = 65,
		eWorldTourOnlineBattle = 66,  // Avatar fight
		eWorldTourSpectateReady = 67,
		eWorldTourTrainingReady = 68,
		eWorldTourOnlineCpuBattleReady = 69,
		eWorldTourTeamBattleReady = 70,
		eWorldTourTeamBattleOut = 71,
		eBattleHubInvite = 72,		
		eBattleHubInit = 73,
		eBattleHubCheckAdmissionEvent = 74,
		eBattleHubCheckLearningAiAdmissionEvent = 75,
		eBattleHubLearningAiNextRival = 76,
		eBattleHubLearningAiCharacterSelect = 77,
		eBattleHubLogin = 78,
		eBattleHubMain = 79,
		eBattleHubAvatarBattleIn = 80,
		eBattleHubAvatarBattleOut = 81,
		eBattleHubCharacterCreateOpening = 82,
		eBattleHubCharacterCreateShop = 83, // Character modification in battlehub
		eBattleHubIntroduction = 84,
		eBattleHubPlayContact = 85,
		eBattleHubPrePlayContact = 86,
		eArcadeFighterSelect = 87,
		eArcadeComicDemo = 88,
		eArcadeStageMove = 89,
		eArcadeResult = 90,
		eArcadeContinue = 91,
		eArcadeStaffRoll = 92,
		eArcadeEndCard = 93,
		eArcadePlayThanks = 94,
		eLogin = 95,
		eGalleryTop = 96,
		eGalleryEventCmn = 97,
		eGalleryEventWT = 98,
		eGalleryEventBH = 99,
		eGalleryEventFG = 100,
		eMameMain = 101,
		eMameSpectate = 102,
		eEsfCmn00 = 103,
		eWtfCmn00 = 104,
		eCntVersusDemo = 105,
		eCommentatorResource = 106,
		eCommentatorResourceWT = 107,
		eFGTrainingResource = 108,
		eFGTutorialResource = 109,
		eFGComboTrialResource = 110,
		eFGCharacterGuideResource = 111,
		eFGReplayResource = 112,
		eBattleStats = 113,
		eCtrlBattleFlowSound = 114,
		eCommentatorSystem = 115,
		eCommentatorSystemWT = 116,
		eReturnHome = 117,
		eFlowMain = 118,
		eInvite = 119,
		eTransitionActivityFlow = 120,
		eTransitionFlowMain = 121,
		eResidentData = 122,
		eResident = 123,

		FlowScene_Unknown = -1
	};
}