#include "pch.h"
#include "GameAPI.h"
#include "RefAPI.h"
#include "PerfTracker.h"

using namespace std::literals;

GameAPI* GameAPI::instance = nullptr;

GameAPI::GameAPI()
{
}

GameAPI* GameAPI::GetInstance()
{
	if (instance == nullptr)
	{
		instance = new GameAPI();
	}

	return instance;
}

void GameAPI::ClearSpectateBGM() const
{
    PERF_TRACKING("GameAPI::MuteSpectateBGM", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    LogInfo("GameAPI::MuteSpectateBGM");
    auto flowMap = RefAPI::TryGetManagedSingletonField<RefManagedObject*>("app.bFlowManager", { "m_flow_work", "_FlowMap" });
    if (flowMap)
    {
        std::string typeName = (*flowMap)->get_type_definition()->get_full_name();
        if (typeName == "app.battle.SpectateFlowMap")
        {
            RefAPI::TryCallFunction<void*>(*flowMap, "StopBGM", {});
        }
    }
}

void GameAPI::MuteInteractiveFightBGM() const
{
    PERF_TRACKING("GameAPI::MuteInteractiveFightBGM", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    auto _LoadedPrefabDic = RefAPI::TryGetManagedSingletonField<RefManagedObject*>("app.sound.SoundManager", { "BGM", "_LoadedPrefabDic" });
    if (_LoadedPrefabDic)
    {
        auto count = RefAPI::TryGetFieldValue<int>(*_LoadedPrefabDic, { "_count" });
        if (count && *count > 0)
        {
            RefAPI::TryCallFunction<void*>(*_LoadedPrefabDic, "Clear", {});
            LogInfo("BGM muted.");
        }
    }
}

float GameAPI::GetMusicVolume() const
{
    PERF_TRACKING("GameAPI::GetMusicVolume", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    auto volume = RefAPI::TryCallManagedSingletonFunction<float>("app.sound.SoundManager", "get_VolumeBGM");
    if (volume)
    {
        return *volume;
    }

    return 0;
}

void GameAPI::SetMusicVolume(float volume) const
{
    PERF_TRACKING("GameAPI::SetMusicVolume", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    //LogInfo("Setting volume to: {}", volume);
    RefAPI::TryCallManagedSingletonFunction<void*>("app.sound.SoundManager", "set_VolumeBGM", volume);
}

std::optional<std::string> GameAPI::TryGetBattleTrackName() const
{
    PERF_TRACKING("GameAPI::GetBattleTrackName", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    auto loadedPrefabDic = RefAPI::TryGetManagedSingletonField<RefManagedObject*>("app.sound.SoundManager", { "BGM", "_LoadedPrefabDic" });
    auto prefabInstanceId = RefAPI::TryGetManagedSingletonField<int>("app.sound.SoundManager", { "BGM", "_PrefabInstanceId" });

    if (loadedPrefabDic.has_value() && prefabInstanceId.has_value())
    {
        int trackKey = prefabInstanceId.value() - 1;
        auto trackGameObject = RefAPI::TryGetDictionaryValue<int, RefManagedObject*>(loadedPrefabDic.value(), trackKey);
        if (trackGameObject.has_value())
        {
            return RefAPI::GetGameObjectName(trackGameObject.value());
        }
    }

    return std::nullopt;
}

Game::FlowScene GameAPI::GetFlowScene() const
{
    PERF_TRACKING("GameAPI::GetFlowScene", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    auto value = RefAPI::TryCallManagedSingletonFunction<uint32_t>("app.bFlowManager", "get_MainFlowID");
    if (!value)
    {
        Logger::GetInstance()->LogError("Could not get flow ID");
        return Game::FlowScene::FlowScene_Unknown;
    }
    else
    {
        return static_cast<Game::FlowScene>(*value);
    }
}

BattleDescription GameAPI::GetBattleDescription() const
{
    PERF_TRACKING("GameAPI::GetBattleDescription", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    BattleDescription battleDescription(Fight::PlayerType::PlayerType_Unknown, Fight::PlayerType::PlayerType_Unknown);
    auto m_flows = RefAPI::TryGetManagedSingletonField<RefManagedObject*>("app.bFlowManager", { "m_flows" });
    if (m_flows)
    {
        auto item = RefAPI::TryGetCollectionItemAt<RefManagedObject*>(*m_flows, 1);
        if (item)
        {
            auto fighterDescs = RefAPI::TryGetFieldValue<RefManagedObject*>(item.value(), { "m_battle_core", "_FighterDescs" });
            if (fighterDescs)
            {
                auto fighterDesc0 = RefAPI::TryGetCollectionItemAt<RefManagedObject*>(*fighterDescs, 0);
                auto fighterDesc1 = RefAPI::TryGetCollectionItemAt<RefManagedObject*>(*fighterDescs, 1);
                if (fighterDesc0 && fighterDesc1)
                {
                    auto fighterId0 = RefAPI::TryGetFieldValue<int>(*fighterDesc0, { "FighterId" });
                    auto fighterId1 = RefAPI::TryGetFieldValue<int>(*fighterDesc1, { "FighterId" });
                    auto padId0 = RefAPI::TryGetFieldValue<int>(*fighterDesc0, { "PadId" });
                    auto padId1 = RefAPI::TryGetFieldValue<int>(*fighterDesc1, { "PadId" });
                    if (fighterId0 && fighterId1 && padId0 && padId1)
                    {
                        if (*padId0 == 0)
                        {
                            battleDescription = BattleDescription(static_cast<Fight::PlayerType>(*fighterId0), static_cast<Fight::PlayerType>(*fighterId1));
                        }
                        else
                        {
                            battleDescription = BattleDescription(static_cast<Fight::PlayerType>(*fighterId1), static_cast<Fight::PlayerType>(*fighterId0));
                        }
                    }
                }
            }
        }
    }

    return battleDescription;
}

std::string GameAPI::GetCurrentFlow() const
{
    PERF_TRACKING("GameAPI::GetBattleDescription", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    BattleDescription battleDescription(Fight::PlayerType::PlayerType_Unknown, Fight::PlayerType::PlayerType_Unknown);
    auto m_flows = RefAPI::TryGetManagedSingletonField<RefManagedObject*>("app.bFlowManager", { "m_flows" });
    if (m_flows)
    {
        auto item = RefAPI::TryGetCollectionItemAt<RefManagedObject*>(*m_flows, 1);
        if (item)
        {
            std::string flowManagerName = item.value()->get_type_definition()->get_full_name();
            if (flowManagerName == "app.battle.bBattleFlow")
            {
                auto bgmType = RefAPI::TryGetFieldValue<RefManagedObject*>(item.value(), { "m_desc"/*, "Rule", "BgmType"*/});
                if (bgmType.has_value())
                {
                    return bgmType.value()->get_type_definition()->get_full_name();
                }
            }

        }
    }
    return std::string();
}

Fight::State GameAPI::GetFightState() const
{
    PERF_TRACKING("GameAPI::GetFightState", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    auto fightState = RefAPI::TryGetStaticFieldValue<uint8_t>("gBattle", { "Game", "fight_st" });
    if (fightState.has_value())
    {
        return static_cast<Fight::State>(fightState.value());
    }

    return Fight::State::StageInit;
}

Character GameAPI::GetCharacter(int playerIndex) const
{
    PERF_TRACKING("GameAPI::GetCharacter", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    Character character(Fight::PlayerType::PlayerType_Unknown, 0);
    auto fightState = GetFightState();

    if (fightState > Fight::State::StageInit && fightState <= Fight::State::Now)
    {
        auto playerType = RefAPI::TryGetStaticFieldValue<RefManagedObject*>("gBattle", { "Player", "mPlayerType" });
        if (*playerType)
        {
            auto v = RefAPI::TryGetArrayItem<RefManagedObject*>(*playerType, playerIndex - 1);
            if (v)
            {
                auto pType = RefAPI::TryGetFieldValue<uint32_t>(*v, { "mValue" });
                if (pType)
                {
                    character = Character(static_cast<Fight::PlayerType>(*pType), 0);
                }
            }
        }
    }

    if (fightState == Fight::State::Now)
    {
        auto result = RefAPI::TryCallStaticFieldFunction<reframework::API::ManagedObject*>("gBattle", "Player", playerIndex == 1 ? "get1P" : "get2P");
        if (result)
        {
            reframework::API::ManagedObject* player = *result;
            auto fightStyle = RefAPI::TryGetFieldValue<short>(player, { "mCheckInfo", "FightStyle" });
            auto vital_max = RefAPI::TryGetFieldValue<int>(player, { "vital_max" });
            auto vital_new = RefAPI::TryGetFieldValue<int>(player, { "vital_new" });
            if (vital_new.has_value() && vital_max.has_value())
            {
                character.IsInDanger = (static_cast<float>(vital_new.value()) / vital_max.value()) <= 0.25f;
            }

            if (fightStyle)
            {
                character.FightStyle = *fightStyle;
            }
        }
    }

    return character;
}

int GameAPI::GetRound() const
{
	PERF_TRACKING("GameAPI::GetRound", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
	auto roundNo = RefAPI::TryGetStaticFieldValue<uint8_t>("gBattle", { "Round", "RoundNo" });
	if (roundNo.has_value())
	{
		return roundNo.value();
	}

	return -1;
}

Game::Mode GameAPI::GetGameMode() const
{
    PERF_TRACKING("GameAPI::GetGameMode", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    auto gameMode = RefAPI::TryGetStaticFieldValue<int>("gBattle", { "Config", "_GameMode" });
    if (gameMode.has_value() && gameMode.value() < Game::Mode::Mode_Max)
    {
        return static_cast<Game::Mode>(gameMode.value());
    }
    else
    {
        return Game::Mode::Mode_Unknown;
    }
}

Game::Screen GameAPI::GetScreen(Game::FlowScene flowScene, Game::Mode gameMode, Fight::State state)
{
    if (WorldTourScenes.count(flowScene))
    {
        return Game::Screen::WorldTour;
    }
    else if (FightFlowScenes.count(flowScene))
    {
        return Game::Screen::Fight;
    }
    else
    {
        return Game::Screen::Menu;
    }
}

void GameAPI::StopBGM()
{
    PERF_TRACKING("GameAPI::StopBGM", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    LogInfo("GameAPI::StopBGM");
    auto _LoadedPrefabDic = RefAPI::TryGetManagedSingletonField<RefManagedObject*>("app.sound.SoundManager", { "BGM", "_LoadedPrefabDic" });
    auto prefabInstanceId = RefAPI::TryGetManagedSingletonField<int>("app.sound.SoundManager", { "BGM", "_PrefabInstanceId" });
    if (_LoadedPrefabDic.has_value())
    {
        if (prefabInstanceId.has_value())
        {
            auto count = RefAPI::TryGetFieldValue<int>(*_LoadedPrefabDic, { "_count" });
            int trackKey = prefabInstanceId.value();
            for (int i = 0; i < trackKey; i++)
            {
                if (RefAPI::DictionaryContainsKey(_LoadedPrefabDic.value(), i))
                {
                    auto trackGameObject = RefAPI::TryGetDictionaryValue<int, RefManagedObject*>(_LoadedPrefabDic.value(), i);
                    if (trackGameObject.has_value())
                    {
                        auto soundContainerApp = RefAPI::TryGetComponent<RefManagedObject*>(trackGameObject.value(), "app.sound.SoundContainerApp");
                        if (soundContainerApp.has_value())
                        {
                            RefAPI::TryCallFunction<void*>(soundContainerApp.value(), "onUnload");
                        }
                    }
                }
            }
        }

        for (int trackIndex : _playedTracks)
        {
            LogInfo("StopBGM - Unloading {}", std::to_string(trackIndex));
            if (RefAPI::DictionaryContainsKey(_LoadedPrefabDic.value(), trackIndex))
            {
                auto trackGameObject = RefAPI::TryGetDictionaryValue<int, RefManagedObject*>(_LoadedPrefabDic.value(), trackIndex);
                if (trackGameObject.has_value())
                {
                    auto soundContainerApp = RefAPI::TryGetComponent<RefManagedObject*>(trackGameObject.value(), "app.sound.SoundContainerApp");
                    if (soundContainerApp.has_value())
                    {
                        RefAPI::TryCallFunction<void*>(soundContainerApp.value(), "onUnload");
                    }
                }
            }
        }
    }

    IsPlayingBGM = false;
}

void GameAPI::ClearBGM()
{
    auto _LoadedPrefabDic = RefAPI::TryGetManagedSingletonField<RefManagedObject*>("app.sound.SoundManager", { "BGM", "_LoadedPrefabDic" });
    if (_LoadedPrefabDic.has_value())
    {
        LogInfo("GameAPI::ClearBGM");
        RefAPI::TryCallFunction<void*>(_LoadedPrefabDic.value(), "Clear");
    }

    IsPlayingBGM = false;
}

void GameAPI::DeleteBGM()
{
    PERF_TRACKING("GameAPI::DeleteBGM", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    auto _LoadedPrefabDic = RefAPI::TryGetManagedSingletonField<RefManagedObject*>("app.sound.SoundManager", { "BGM", "_LoadedPrefabDic" });
    auto prefabInstanceId = RefAPI::TryGetManagedSingletonField<int>("app.sound.SoundManager", { "BGM", "_PrefabInstanceId" });
    if (_LoadedPrefabDic.has_value())
    {
        if (prefabInstanceId.has_value())
        {
            int trackKey = prefabInstanceId.value();
            for (int i = 0; i < trackKey; i++)
            {
                if (RefAPI::DictionaryContainsKey(_LoadedPrefabDic.value(), i))
                {
                    auto trackGameObject = RefAPI::TryGetDictionaryValue<int, RefManagedObject*>(_LoadedPrefabDic.value(), i);
                    if (trackGameObject.has_value())
                    {
                        LogInfo("GameAPI::DeleteBGM - {}", std::to_string(i));
                        RefAPI::TryCallFunction<void*>(trackGameObject.value(), "destroy", { (void*)trackGameObject.value() });
                    }
                }
            }
        }

        for (int trackIndex : _playedTracks)
        {
            if (RefAPI::DictionaryContainsKey(_LoadedPrefabDic.value(), trackIndex))
            {
                auto trackGameObject = RefAPI::TryGetDictionaryValue<int, RefManagedObject*>(_LoadedPrefabDic.value(), trackIndex);
                if (trackGameObject.has_value())
                {
                    LogInfo("GameAPI::DeleteBGM - {}", std::to_string(trackIndex));
                    RefAPI::TryCallFunction<void*>(trackGameObject.value(), "destroy", { (void*)trackGameObject.value() });
                }
            }
        }

        _playedTracks.clear();
        RefAPI::TryCallFunction<void*>(_LoadedPrefabDic.value(), "Clear");
    }

    IsPlayingBGM = false;
}

void GameAPI::DeletePlayedTracks()
{
    PERF_TRACKING("GameAPI::DeletePlayedTracks", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    auto _LoadedPrefabDic = RefAPI::TryGetManagedSingletonField<RefManagedObject*>("app.sound.SoundManager", { "BGM", "_LoadedPrefabDic" });
    for (int trackIndex : _playedTracks)
    {
        if (RefAPI::DictionaryContainsKey(_LoadedPrefabDic.value(), trackIndex))
        {
            auto trackGameObject = RefAPI::TryGetDictionaryValue<int, RefManagedObject*>(_LoadedPrefabDic.value(), trackIndex);
            if (trackGameObject.has_value())
            {
                LogInfo("GameAPI::DeletePlayedTracks - {}", std::to_string(trackIndex));
                RefAPI::TryCallFunction<void*>(_LoadedPrefabDic.value(), "Remove", { (void*)trackIndex });
                RefAPI::TryCallFunction<void*>(trackGameObject.value(), "destroy", { (void*)trackGameObject.value() });
            }
        }
    }

    _playedTracks.clear();
}

void GameAPI::DeletePreloadedTracks()
{
    PERF_TRACKING("GameAPI::DeletePreloadedTracks", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    auto _LoadedPrefabDic = RefAPI::TryGetManagedSingletonField<RefManagedObject*>("app.sound.SoundManager", { "BGM", "_LoadedPrefabDic" });
    auto prefabInstanceId = RefAPI::TryGetManagedSingletonField<int>("app.sound.SoundManager", { "BGM", "_PrefabInstanceId" });
    if (_LoadedPrefabDic.has_value())
    {
        if (prefabInstanceId.has_value())
        {
            int trackKey = prefabInstanceId.value();
            for (int i = 0; i < trackKey; i++)
            {
                if (RefAPI::DictionaryContainsKey(_LoadedPrefabDic.value(), i))
                {
                    auto trackGameObject = RefAPI::TryGetDictionaryValue<int, RefManagedObject*>(_LoadedPrefabDic.value(), i);
                    if (trackGameObject.has_value())
                    {
                        LogInfo("GameAPI::DeletePreloadedTracks - {}", std::to_string(i));
                        RefAPI::TryCallFunction<void*>(_LoadedPrefabDic.value(), "Remove", { (void*)i });
                        RefAPI::TryCallFunction<void*>(trackGameObject.value(), "destroy", { (void*)trackGameObject.value() });
                    }
                }
            }
        }
    }
}

bool GameAPI::PlayStageBGM(Fight::Stage stage)
{
    if (_stageTracks.count(stage))
    {
        int trackKey = 5000;
        trackKey += std::distance(_stageTracks.begin(), _stageTracks.find(stage));
        if (_preLoadedStageTracks.count(stage) == 0) 
        {
            auto bgmTrack = LoadBGM(_stageTracks[stage]);
            if (bgmTrack == nullptr)
            {
                return false;
            }

            _preLoadedStageTracks[stage] = bgmTrack;
        }
		
        RefManagedObject* track = _preLoadedStageTracks[stage];
        
        // We'll have to destroy that track after being played to avoid it being played twice simultaneously next time it's requested.
        _preLoadedStageTracks.erase(stage);

        return PlayBGM(trackKey, track);
    }

    return false;
}

bool GameAPI::PlayCharacterBGM(Fight::PlayerType character)
{
    if (_characterTracks.count(character))
    {
        int trackKey = 5000;
        trackKey += _stageTracks.size();
        trackKey += std::distance(_characterTracks.begin(), _characterTracks.find(character));
        if (_preLoadedCharacterTracks.count(character) == 0)
        {
            auto bgmTrack = LoadBGM(_characterTracks[character]);
            if (bgmTrack == nullptr)
            {
                return false;
            }

			_preLoadedCharacterTracks[character] = bgmTrack;
        }

        RefManagedObject* track = _preLoadedCharacterTracks[character];

        // We'll have to destroy that track after being played to avoid it being played twice simultaneously next time it's requested.
        _preLoadedCharacterTracks.erase(character);

        return PlayBGM(trackKey, track);
    }

    return false;
}

std::string wstring_view_to_string(const std::wstring_view& wstr_view) {
    // Determine the size of the destination buffer
    int size_needed = WideCharToMultiByte(CP_UTF8, 0, wstr_view.data(), (int)wstr_view.size(), NULL, 0, NULL, NULL);
    if (size_needed == 0) {
        throw std::runtime_error("Error in WideCharToMultiByte");
    }

    // Allocate a string to hold the result
    std::string str(size_needed, 0);

    // Perform the conversion
    int result = WideCharToMultiByte(CP_UTF8, 0, wstr_view.data(), (int)wstr_view.size(), &str[0], size_needed, NULL, NULL);
    if (result == 0) {
        throw std::runtime_error("Error in WideCharToMultiByte");
    }

    return str;
}

RefManagedObject* GameAPI::LoadBGM(std::wstring_view path)
{
    std::string bgmPath = wstring_view_to_string(path);
	LogInfo("Loading BGM: {}", bgmPath);
    auto pfb = RefAPI::TryCreateInstance("via.Prefab");
    if (pfb.has_value())
    {
        pfb.value()->add_ref();
        if (pfb.has_value() && pfb.value() != nullptr)
        {
            auto trackPath = RefAPI::create_managed_string(path);
            RefAPI::TryCallFunction<void*>(pfb.value(), "set_Path", trackPath);
            auto exists = RefAPI::TryCallFunction<bool>(pfb.value(), "get_Exist");
            if (exists.has_value() && exists.value())
            {
                RefAPI::TryCallFunction<void*>(pfb.value(), "set_Standby", { (void*)true });
                auto position = RefAPI::TryCreateInstance("via.vec3");
                if (position.has_value())
                {
                    auto pfbi = RefAPI::TryCallFunction<RefManagedObject*>(pfb.value(), "instantiate(via.vec3)", { (void*)position.value() });
                    if (pfbi.has_value() && pfbi.value() != nullptr)
                    {
                        auto name = RefAPI::GetGameObjectName(pfbi.value());
                        if (name.has_value())
                        {
                            LogInfo("Loaded: {}", name.value());
                            return pfbi.value();
                        }
                    }
                }
            }
        }
    }

	LogInfo("Failed to load BGM: {}", bgmPath);
    return nullptr;
}

bool GameAPI::PlayBGM(int trackKey, RefManagedObject* track)
{
    LogInfo("PlayBGM - track key: {}", std::to_string(trackKey));
    auto bgm = RefAPI::TryGetManagedSingletonField<RefManagedObject*>("app.sound.SoundManager", { "BGM" });
    auto _LoadedPrefabDic = RefAPI::TryGetManagedSingletonField<RefManagedObject*>("app.sound.SoundManager", { "BGM", "_LoadedPrefabDic" });
    auto trackGameObject = RefAPI::TryGetDictionaryValue<int, RefManagedObject*>(_LoadedPrefabDic.value(), trackKey);

    if (!trackGameObject.has_value())
    {
        LogInfo("PlayBGM - key {} not found, adding associated track to _LoadedPrefabDic.", std::to_string(trackKey));
        RefAPI::TryCallFunction<void*>(_LoadedPrefabDic.value(), "Add(System.Int32, via.GameObject)", { (void*)trackKey, track });
        auto gameObjectName = RefAPI::GetGameObjectName(track);
        if (gameObjectName.has_value())
        {
            LogInfo("PlayBGM - {}", gameObjectName.value());
            RefAPI::TryCallFunction<void*>(bgm.value(), "playBGM(System.Int32, System.UInt32, System.Boolean)", { (void*)trackKey , (void*)2228386809 , (void*)true });
            _playedTracks.insert(trackKey);
            IsPlayingBGM = true;
            return true;
        }
    }
    else
    {
        RefAPI::TryCallFunction<void*>(bgm.value(), "playBGM(System.Int32, System.UInt32, System.Boolean)", { (void*)trackKey , (void*)2228386809 , (void*)true });
        _playedTracks.insert(trackKey);
        IsPlayingBGM = true;
        return true;
    }

    IsPlayingBGM = false;
    return false;
}


Fight::Stage GameAPI::GetStage(Game::FlowScene flowScene, Game::Mode gameMode, Fight::State state) const
{
    PERF_TRACKING("GameAPI::GetStage", DEFAULT_PERFORMANCE_WARNING_THRESHOLD);
    // If we're in world tour mode, there are no stage ids;
    if (WorldTourScenes.count(flowScene) > 0 || gameMode == Game::Mode::Story || gameMode == Game::Mode::Story_Training)
    {
        return Fight::Stage::WorldTour;
    }

    if (FightFlowScenes.count(flowScene))
    {
        auto stageId = RefAPI::TryCallManagedSingletonFunction<int>("app.battle.stage.ESGimmickManager", "get_StageId");

        switch (stageId.value())
        {
        case Fight::Stage::BarmaleySteelworks:
            return Fight::Stage::BarmaleySteelworks;
        case Fight::Stage::BathersBeach:
            return Fight::Stage::BathersBeach;
        case Fight::Stage::CarrierByronTaylor:
            return Fight::Stage::CarrierByronTaylor;
        case Fight::Stage::Colosseo:
            return Fight::Stage::Colosseo;
        case Fight::Stage::DhalsimerTemple:
            return Fight::Stage::DhalsimerTemple;
        case Fight::Stage::FeteForaine:
            return Fight::Stage::FeteForaine;
        case Fight::Stage::GenbuTemple:
            return Fight::Stage::GenbuTemple;
        case Fight::Stage::KingStreet:
            return Fight::Stage::KingStreet;
        case Fight::Stage::MetroCityDowntown:
            return Fight::Stage::MetroCityDowntown;
        case Fight::Stage::OldTownMarket:
            return Fight::Stage::OldTownMarket;
        case Fight::Stage::RangersHut:
            return Fight::Stage::RangersHut;
        case Fight::Stage::SuvalhalArena:
            return Fight::Stage::SuvalhalArena;
        case Fight::Stage::TheMachoRing:
            return Fight::Stage::TheMachoRing;
        case Fight::Stage::ThunderfootSettlement:
            return Fight::Stage::ThunderfootSettlement;
        case Fight::Stage::TianHongYuan:
            return Fight::Stage::TianHongYuan;
        case Fight::Stage::RuinedLab:
            return Fight::Stage::RuinedLab;
        case Fight::Stage::EnmaHollow:
            return Fight::Stage::EnmaHollow;
        case Fight::Stage::PaoPaoCafe:
            return Fight::Stage::PaoPaoCafe;
        case Fight::Stage::TrainingRoom:
            return Fight::Stage::TrainingRoom;
        default:
            return Fight::Stage::Stage_Unkown;
        }
    }
    
    return Fight::Stage::Stage_Unkown;
}

Fight::TrackType GameAPI::GetTrackType(std::string battleTrackName) const
{
    if (battleTrackName.find("bgm_battle_ess") != std::string::npos)
    {
        return Fight::TrackType::StageTrack;
    }
    else
    {
        return Fight::TrackType::CharacterTrack;
    }
}

Fight::Stage GameAPI::GetStageFromBattleTrackName(std::string battleTrackName) const
{
    int stageId;
    if (battleTrackName == "bgm_battle_ess0000_00")
    {
        stageId = 0;
    }
    else
    {
        size_t index = battleTrackName.find_first_of('0') + 1;
        std::string stageIdStr = battleTrackName.substr(index, 3);
        stageId = atoi(stageIdStr.c_str()) * 100;
    }

    return static_cast<Fight::Stage>(stageId);
}

Fight::PlayerType GameAPI::GetCharacterFromBattleTrackName(std::string battleTrackName) const
{
    int characterId;
    size_t index = battleTrackName.find_first_of('0') + 1;
    std::string characterIdStr = battleTrackName.substr(index);
    characterId = atoi(characterIdStr.c_str());
    return static_cast<Fight::PlayerType>(characterId);
}

bool GameAPI::HasFocus() const
{
    HWND foregroundWindow = GetForegroundWindow();
    if (foregroundWindow == NULL) 
    {
        return false;
    }

    const int bufferSize = 256;
    char windowTitle[bufferSize];
    GetWindowTextA(foregroundWindow, windowTitle, bufferSize);
    return strstr(windowTitle, "Street Fighter 6") != nullptr;
}
