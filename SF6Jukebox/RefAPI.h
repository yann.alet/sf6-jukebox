#pragma once

#include <wchar.h>
#include <string>
#include <optional>
#include "include/reframework/API.hpp"
#include "Logger.h"
#include <shared_mutex>
#include <unordered_map>

typedef reframework::API::ManagedObject RefManagedObject;

namespace RefAPI
{
	class REObject {
	public:
		void* info; // 0x0000
	};
	static_assert(sizeof(REObject) == 0x8);

	class REManagedObject : public REObject {
	public:
		uint32_t referenceCount; // 0x0008
		int16_t N000071AE;       // 0x000C
		char pad_000E[2];        // 0x000E
	};                           // Size: 0x0010
	static_assert(sizeof(REManagedObject) == 0x10);

	class SystemString : public REManagedObject {
	public:
		int32_t size;      // 0x0010
		wchar_t data[256]; // 0x0014
	};                     // Size: 0x0214
	//static_assert(sizeof(SystemString) == 0x214);

	namespace Utils
	{
		std::string ToNarrow(const wchar_t* s, char dfault = '?', const std::locale& loc = std::locale());

		reframework::API::TypeDefinition* find_type_definition(std::string_view type_name);

		template<typename T>
		T* get_native_field(void* obj, reframework::API::TypeDefinition* t, std::string_view name, bool is_value_type);

		template<typename T>
		T* get_static_field(std::string_view type_name, std::string_view name, bool is_value_type = false);
	}	

	std::string GetString(reframework::API::ManagedObject* instance);

	std::optional<std::string> GetGameObjectName(reframework::API::ManagedObject* gameObject);

	template <class T>
	std::optional<T> TryGetResult(const reframework::InvokeRet& invokeRet)
	{
		if (!invokeRet.exception_thrown)
		{
			if constexpr (std::is_same_v<T, void*>)
			{
				return std::nullopt;
			}
			else if constexpr (std::is_same_v<T, SystemString*>)
			{
				return { static_cast<T>(invokeRet.ptr) };
			}
			else if constexpr (std::is_same_v<T, reframework::API::ManagedObject*>)
			{
				return { static_cast<T>(invokeRet.ptr) };
			}
			else if constexpr (std::is_same_v<T, uint32_t> || std::is_same_v<T, int>)
			{
				return { static_cast<T>(invokeRet.dword) };
			}
			else if constexpr (std::is_same_v<T, std::array<uint8_t, 128>>)
			{
				return { static_cast<T>(invokeRet.bytes) };
			}
			else if constexpr (std::is_same_v<T, uint8_t>)
			{
				return { static_cast<T>(invokeRet.byte) };
			}
			else if constexpr (std::is_same_v<T, uint16_t>)
			{
				return { static_cast<T>(invokeRet.word) };
			}
			else if constexpr (std::is_same_v<T, float>)
			{
				return { static_cast<T>(invokeRet.f) };
			}
			else if constexpr (std::is_same_v<T, uint64_t>)
			{
				return { static_cast<T>(invokeRet.qword) };
			}
			else if constexpr (std::is_same_v<T, double>)
			{
				return { static_cast<T>(invokeRet.d) };
			}
			else
			{
				LogError("Type not supported: " + std::string(typeid(T).name()));
			}
		}
		return std::nullopt;
	}

	template <class T>
	std::optional<T> TryCallFunction(reframework::API::ManagedObject* instance, std::string functionName, std::vector<void*> functionParams)
	{
		auto fieldTypeDefinition = instance->get_type_definition();
		if (fieldTypeDefinition)
		{
			auto method = fieldTypeDefinition->find_method(functionName);
			if (method)
			{
				auto r = method->invoke(instance, functionParams);
				if (!r.exception_thrown)
				{
					return TryGetResult<T>(r);
				}
				else
				{
					LogError("Failed to invoke: " + functionName);
				}
			}
			else
			{
				LogError("Failed to find method: " + functionName);
			}
		}

		return std::nullopt;
	}

	template <class T>
	bool TryCallFunction3(reframework::API::ManagedObject* instance, std::string functionName, std::vector<void*> functionParams)
	{
		auto fieldTypeDefinition = instance->get_type_definition();
		if (fieldTypeDefinition)
		{
			auto method = fieldTypeDefinition->find_method(functionName);
			if (method)
			{
				auto r = method->invoke(instance, functionParams);
				if (!r.exception_thrown)
				{
					return true;
				}
				else
				{
					LogError("Failed to invoke: " + functionName);
				}
			}
			else
			{
				LogError("Failed to find method: " + functionName);
			}
		}

		return false;
	}

	template <class T>
	std::optional<T> TryGetCollectionItemAt(reframework::API::ManagedObject* instance, uint64_t index)
	{
		return RefAPI::TryCallFunction<T>(instance, "get_Item", { (void*)index });
	}

	template <class T>
	std::optional<T> TryCallManagedSingletonFunction(std::string singletonName, std::string methodName, std::vector<void*> args)
	{
		auto& api = reframework::API::get();
		auto singleton = api->get_managed_singleton(singletonName);
		if (singleton != nullptr)
		{
			auto result = singleton->invoke(methodName, args);
			if (result.exception_thrown)
			{
				LogError("Exception when attempting to call: " + methodName);
			}
			else
			{
				return TryGetResult<T>(result);
			}
		}
		else
		{
			LogError("Failed to get singleton: " + singletonName);
		}

		return std::nullopt;
	}


	template<class T, typename... Args>
	std::optional<T> TryCallManagedSingletonFunction(std::string singletonName, std::string methodName, Args... args)
	{
		const auto& api = reframework::API::get();
		const auto tdb = api->tdb();
		auto singleton = api->get_managed_singleton(singletonName);
		if (singleton != nullptr)
		{
			auto method = tdb->find_method(singletonName, methodName);
			auto vm_ctx = api->get_vm_context();
			if constexpr (std::is_same_v<T, void*>)
			{
				method->call<T>(vm_ctx, singleton, args...);
				return std::nullopt;
			}
			else
			{
				return { method->call<T>(vm_ctx, singleton, args...) };
			}			
		}
		else
		{
			LogError("Failed to get singleton: " + singletonName);
			return std::nullopt;
		}
	}

	template<class T, typename... Args>
	std::optional<T> TryCallFunction(reframework::API::ManagedObject* owner, std::string methodName, Args... args)
	{
		if (owner != nullptr)
		{
			const auto& api = reframework::API::get();
			const auto tdb = api->tdb();
			auto method = tdb->find_method(owner->get_type_info()->get_name(), methodName);
			if (method)
			{
				auto vm_ctx = api->get_vm_context();
				if constexpr (std::is_same_v<T, void*>)
				{
					method->call<T>(vm_ctx, owner, args...);
					return std::nullopt;
				}
				else
				{
					return { method->call<T>(vm_ctx, owner, args...) };
				}
			}
			else
			{
				LogError("Failed to find method {} on {}", methodName, owner->get_type_info()->get_name());
			}
		}
		else
		{
			LogError("TryCallFunction failed, managed object is null, method name: {}.", methodName);
		}

		return std::nullopt;
	}

	template <class T>
	std::optional<T> TryCallManagedSingletonFunction(std::string singletonName, std::string methodName)
	{
		return TryCallManagedSingletonFunction<T>(singletonName, methodName, {});
	}

	template <class T>
	std::optional<T> TryGetFieldValue(const reframework::API::ManagedObject* owner, std::vector<std::string> fieldPath)
	{
		if (fieldPath.size() == 1)
		{
			auto resultPtr = owner->get_field<T>(fieldPath[0]);
			if (resultPtr)
			{
				T r = static_cast<T>(*resultPtr);
				return { r };
			}
			else
			{
				LogError("Failed get_field for: " + fieldPath[0]);
				return std::nullopt;
			}
		}
		else
		{
			auto o = owner->get_field<reframework::API::ManagedObject*>(fieldPath[0]);
			fieldPath.erase(fieldPath.begin());
			return TryGetFieldValue<T>(*o, fieldPath);
		}
	}

	template <class T>
	std::optional<T> TryGetValueTypeFieldValue(const reframework::API::ManagedObject* owner, std::vector<std::string> fieldPath)
	{
		if (fieldPath.size() == 1)
		{
			auto resultPtr = owner->get_field<T>(fieldPath[0], true);
			if (resultPtr)
			{
				T r = static_cast<T>(*resultPtr);
				return { r };
			}
			else
			{
				LogError("Failed get_field for: " + fieldPath[0]);
				return std::nullopt;
			}
		}
		else
		{
			auto o = owner->get_field<reframework::API::ManagedObject*>(fieldPath[0]);
			fieldPath.erase(fieldPath.begin());
			return TryGetFieldValue<T>(*o, fieldPath);
		}
	}

	template <class T>
	std::optional<T> TryGetManagedSingletonField(std::string singletonName, std::vector<std::string> fieldPath)
	{
		auto& api = reframework::API::get();
		auto singleton = api->get_managed_singleton(singletonName);
		return TryGetFieldValue<T>(singleton, fieldPath);
	}

	template <class T>
	std::optional<T> TryGetStaticFieldValue(std::string className, std::vector<std::string> fieldPath)
	{
		auto& api = reframework::API::get();
		const auto tdb = api->tdb();
		const auto classType = tdb->find_type(className);
		const auto field = classType->find_field(fieldPath[0]);		
		const auto fieldValue = field->get_data<reframework::API::ManagedObject*>();
		
		if (fieldValue)
		{
			fieldPath.erase(fieldPath.begin());
			return TryGetFieldValue<T>(fieldValue, fieldPath);
		}
		else
		{
			return std::nullopt;
		}
	}

	template <class T>
	std::optional<T> TryCallStaticFieldFunction(std::string className, std::string fieldName, std::string methodName, std::vector<void*> args)
	{
		auto& api = reframework::API::get();
		const auto tdb = api->tdb();
		const auto classType = tdb->find_type(className);
		const auto field = classType->find_field(fieldName);		
		const auto fieldValue = field->get_data<reframework::API::ManagedObject*>();
		if (fieldValue)
		{
			auto fieldTypeDefinition = fieldValue->get_type_definition();
			if (fieldTypeDefinition)
			{
				auto method = fieldTypeDefinition->find_method(methodName);
				if (method)
				{
					auto r = method->invoke(fieldValue, args);					
					return TryGetResult<T>(r);
				}
				else
				{
					LogError("Failed to find method: " + methodName);
				}
			}
			else
			{
				LogError("Failed to get field type definition for field: " + fieldName);
			}
		}
		else
		{
			LogError("Failed to get field value : " + fieldName);
		}

		return std::nullopt;
	}

	template <class T>
	std::optional<T> TryCallStaticFieldFunction(std::string className, std::string fieldName, std::string methodName)
	{
		return TryCallStaticFieldFunction<T>(className, fieldName, methodName, {});
	}

	template <class T>
	std::optional<T> TryGetArrayItem(reframework::API::ManagedObject* array, uint64_t itemIndex)
	{
		auto& api = reframework::API::get();
		const auto tdb = api->tdb();
		auto arrayDefinition = tdb->find_type("System.Array");
		auto getValueMethod = arrayDefinition->find_method("GetValue(System.Int32)");
		auto result = getValueMethod->invoke(array, { (void*)itemIndex });
		if (!result.exception_thrown)
		{
			return TryGetResult<T>(result);
		}
		else
		{
			LogError("Failed to get field value at index: " + itemIndex);
		}

		return std::nullopt;
	}

	template <class KeyType>
	bool DictionaryContainsKey(reframework::API::ManagedObject* instance, KeyType key)
	{
		auto containsKey = RefAPI::TryCallFunction<bool>(instance, "ContainsKey", key);
		return containsKey.has_value() && containsKey.value();
	}

	template <class KeyType, class ValueType>
	std::optional<ValueType> TryGetDictionaryValue(reframework::API::ManagedObject* instance, KeyType key)
	{
		auto containsKey = RefAPI::TryCallFunction<bool>(instance, "ContainsKey", key);
		if (containsKey && *containsKey)
		{
			auto item = RefAPI::TryCallFunction<ValueType>(instance, "get_Item", key);
			if (item)
			{
				return { *item };
			}
			else
			{
				LogError("Failed to get dic value");
			}
		}
		else
		{
			LogError("Key not found in dictionary");
		}

		return std::nullopt;
	}

	template <class T>
	std::optional<T> TryGetComponent(reframework::API::ManagedObject* owner, std::string componentTypeName)
	{
		auto& api = reframework::API::get();
		const auto tdb = api->tdb();
		const auto ownerType = owner->get_type_definition();
		const auto componentType = api->tdb()->find_type(componentTypeName);
		const auto method = ownerType->find_method("getComponent(System.Type)");
		auto result = method->invoke(owner, { (void*)componentType->get_runtime_type() });
		if (!result.exception_thrown)
		{
			return TryGetResult<T>(result);
		}
		else
		{
			return std::nullopt;
		}		
	}

	RefManagedObject* create_managed_string(std::wstring_view str);

	std::optional<reframework::API::ManagedObject*> TryCreateInstance(std::string instanceTypeName);
}