﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace MediaPlayerDAL.Versioning.v1_22_0
{
    public class FlowScene
    {
        public FlowSceneId Id { get; set; }
        public string Name { get; set; }
        public List<Track> Tracks { get; set; }
        public int IsRandomPlaylist { get; set; }
        public int Enabled { get; set; }
    }

    [Serializable]
    public enum FlowSceneId : int
    {
        [XmlEnum("0")]
        eNone = 0,
        [XmlEnum("1")]
        eBoot = 1,
        [XmlEnum("2")]
        eBootSetup = 2,
        [XmlEnum("3")]
        eTitle = 3,
        [XmlEnum("4")]
        eESportsMainMenu = 4,
        [XmlEnum("5")]
        eModeSelect = 5,
        [XmlEnum("6")]
        eReturnModeSelect = 6,
        [XmlEnum("7")]
        eBootedTransitionHub = 7,
        [XmlEnum("8")]
        eBattleSideSelect = 8,
        [XmlEnum("9")]
        eBattleStageSelect = 9,
        [XmlEnum("10")]
        eBattleFighterSelect = 10,
        [XmlEnum("11")]
        eBattleFighterLeave = 11,
        [XmlEnum("12")]
        eBattleFighterVS = 12,
        [XmlEnum("13")]
        eBattleFighterEmote = 13,
        [XmlEnum("14")]
        eBattleMain = 14,
        [XmlEnum("15")]
        eDeathMatchRuleSelect = 15,
        [XmlEnum("16")]
        eDeathMatchMain = 16,
        [XmlEnum("17")]
        eTeamBattleRuleSelect = 17,
        [XmlEnum("18")]
        eBattleMatchingIn = 18,
        [XmlEnum("19")]
        eBattleMatchingOut = 19,
        [XmlEnum("20")]
        eBattleAssetReload = 20,
        [XmlEnum("21")]
        eTransitionCustomRoom = 21,
        [XmlEnum("22")]
        eCustomRoomSetting = 22,
        [XmlEnum("23")]
        eCustomRoomInit = 23,
        [XmlEnum("24")]
        eCustomRoomInvite = 24,
        [XmlEnum("25")]
        eCustomRoomLogin = 25,
        [XmlEnum("26")]
        eCustomRoomFromBattleResult = 26,
        [XmlEnum("27")]
        eCustomRoomMain = 27,
        [XmlEnum("28")]
        eCustomRoomViewingMain = 28,
        [XmlEnum("29")]
        eTeamBattleMatchingIn = 29,
        [XmlEnum("30")]
        eExamCpuBattleSetup = 30,
        [XmlEnum("31")]
        eOnlineCpuBattleSetup = 31,
        [XmlEnum("32")]
        eReadyAsset = 32,
        [XmlEnum("33")]
        eTrainingSetup = 33,
        [XmlEnum("34")]
        eOnlineTrainingSetup = 34,
        [XmlEnum("35")]
        eTutorialSetup = 35,
        [XmlEnum("36")]
        eTutorialSelect = 36,
        [XmlEnum("37")]
        eCharacterGuideSetup = 37,
        [XmlEnum("38")]
        eCharacterGuideSelect = 38,
        [XmlEnum("39")]
        eComboTrialSetup = 39,
        [XmlEnum("40")]
        eComboTrialSelect = 40,
        [XmlEnum("41")]
        eReplaySetup = 41,
        [XmlEnum("42")]
        eFGReplaySetup = 42,
        [XmlEnum("43")]
        eVersusBackReplay = 43,
        [XmlEnum("44")]
        eSpectateSetup = 44,
        [XmlEnum("45")]
        eSpectateAssetReload = 45,
        [XmlEnum("46")]
        eVersusRule = 46,
        [XmlEnum("47")]
        eModeSelectBattleHub = 47,
        [XmlEnum("48")]
        eModeSelectWorldTour = 48,
        [XmlEnum("49")]
        eModeSelectActivity = 49,
        [XmlEnum("50")]
        eDeleteSaveDataWorldTour = 50,
        [XmlEnum("51")]
        eWorldTourOpening = 51,
        [XmlEnum("52")]
        eWorldTourContinue = 52,
        [XmlEnum("53")]
        eWorldTourCharacterCreateOpening = 53,
        [XmlEnum("54")]
        eWorldTourCharacterCreateShop = 54,
        [XmlEnum("55")]
        eWorldTourCity = 55,
        [XmlEnum("56")]
        eWorldTourEngageBattleReady = 56,
        [XmlEnum("57")]
        eWorldTourEngageBattleOut = 57,
        [XmlEnum("58")]
        eWorldTourMatchUpBattleReady = 58,
        [XmlEnum("59")]
        eWorldTourMatchUpBattleOut = 59,
        [XmlEnum("60")]
        eWorldTourBattle = 60,
        [XmlEnum("61")]
        eWorldTourBattleSound = 61,
        [XmlEnum("62")]
        eWorldTourMiniGame = 62,
        [XmlEnum("63")]
        eWorldTourMiniGameBattle = 63,
        [XmlEnum("64")]
        eWorldTourTutorial = 64,
        [XmlEnum("65")]
        eWorldTourTutorialBattle = 65,
        [XmlEnum("66")]
        eWorldTourOnlineBattle = 66,
        [XmlEnum("67")]
        eWorldTourSpectateReady = 67,
        [XmlEnum("68")]
        eWorldTourTrainingReady = 68,
        [XmlEnum("69")]
        eWorldTourOnlineCpuBattleReady = 69,
        [XmlEnum("70")]
        eBattleHubInvite = 70,
        [XmlEnum("71")]
        eBattleHubInit = 71,
        [XmlEnum("72")]
        eBattleHubCheckAdmissionEvent = 72,
        [XmlEnum("73")]
        eBattleHubLogin = 73,
        [XmlEnum("74")]
        eBattleHubMain = 74,
        [XmlEnum("75")]
        eBattleHubAvatarBattleIn = 75,
        [XmlEnum("76")]
        eBattleHubAvatarBattleOut = 76,
        [XmlEnum("77")]
        eBattleHubCharacterCreateOpening = 77,
        [XmlEnum("78")]
        eBattleHubCharacterCreateShop = 78,
        [XmlEnum("79")]
        eBattleHubIntroduction = 79,
        [XmlEnum("80")]
        eBattleHubPlayContact = 80,
        [XmlEnum("81")]
        eBattleHubPrePlayContact = 81,
        [XmlEnum("82")]
        eArcadeFighterSelect = 82,
        [XmlEnum("83")]
        eArcadeComicDemo = 83,
        [XmlEnum("84")]
        eArcadeStageMove = 84,
        [XmlEnum("85")]
        eArcadeResult = 85,
        [XmlEnum("86")]
        eArcadeContinue = 86,
        [XmlEnum("87")]
        eArcadeStaffRoll = 87,
        [XmlEnum("88")]
        eArcadeEndCard = 88,
        [XmlEnum("89")]
        eArcadePlayThanks = 89,
        [XmlEnum("90")]
        eLogin = 90,
        [XmlEnum("91")]
        eGalleryTop = 91,
        [XmlEnum("92")]
        eGalleryEventCmn = 92,
        [XmlEnum("93")]
        eGalleryEventWT = 93,
        [XmlEnum("94")]
        eGalleryEventBH = 94,
        [XmlEnum("95")]
        eGalleryEventFG = 95,
        [XmlEnum("96")]
        eMameMain = 96,
        [XmlEnum("97")]
        eMameSpectate = 97,
        [XmlEnum("98")]
        eEsfCmn00 = 98,
        [XmlEnum("99")]
        eWtfCmn00 = 99,
        [XmlEnum("100")]
        eCntVersusDemo = 100,
        [XmlEnum("101")]
        eCommentatorResource = 101,
        [XmlEnum("102")]
        eCommentatorResourceWT = 102,
        [XmlEnum("103")]
        eFGTrainingResource = 103,
        [XmlEnum("104")]
        eFGTutorialResource = 104,
        [XmlEnum("105")]
        eFGComboTrialResource = 105,
        [XmlEnum("106")]
        eFGCharacterGuideResource = 106,
        [XmlEnum("107")]
        eCtrlBattleFlowSound = 107,
        [XmlEnum("108")]
        eCommentatorSystem = 108,
        [XmlEnum("109")]
        eCommentatorSystemWT = 109,
        [XmlEnum("110")]
        eReturnHome = 110,
        [XmlEnum("111")]
        eFlowMain = 111,
        [XmlEnum("112")]
        eInvite = 112,
        [XmlEnum("113")]
        eTransitionActivityFlow = 113,
        [XmlEnum("114")]
        eTransitionFlowMain = 114,
        [XmlEnum("115")]
        eResidentData = 115,
        [XmlEnum("116")]
        eResident = 116
    };
}
