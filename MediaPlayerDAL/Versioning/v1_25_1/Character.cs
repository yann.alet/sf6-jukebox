﻿using System.Xml.Serialization;

namespace MediaPlayerDAL.Versioning.v1_25_1
{
    public class Character : BaseScreen
    {
        [XmlIgnore]
        public CharacterId CharacterId { get; set; }

        public override int Id
        {
            get
            {
                return (int)CharacterId;
            }
            set
            {
                CharacterId = (CharacterId)value;
            }
        }
    }
}
