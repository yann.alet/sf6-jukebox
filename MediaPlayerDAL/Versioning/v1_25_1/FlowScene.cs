﻿using System.Xml.Serialization;

namespace MediaPlayerDAL.Versioning.v1_25_1
{
    public class FlowScene : BaseScreen
    {
        [XmlIgnore]
        public FlowSceneId FlowSceneId { get; set; }
        public int Enabled { get; set; }

        public override int Id 
        { 
            get
            {
                return (int)FlowSceneId;
            }
            set
            {
                FlowSceneId = (FlowSceneId)value;
            }
        }        
    }
}
