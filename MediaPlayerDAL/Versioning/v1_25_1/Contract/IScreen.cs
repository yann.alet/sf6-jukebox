﻿using MediaPlayerDAL.Versioning.v1_25_1;

namespace MediaPlayerDAL.Versioning.v1_25_1.Contract
{
    public interface IScreen
    {
        int Id { get; set; }
        string Name { get; set; }
        Playlist Playlist { get; set; }
        int IsRandomPlaylist { get; set; }
    }
}
