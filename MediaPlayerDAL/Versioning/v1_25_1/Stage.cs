﻿using System.Xml.Serialization;

namespace MediaPlayerDAL.Versioning.v1_25_1
{
    public class Stage : BaseScreen
    {
        [XmlIgnore]
        public StageId StageId { get; set; }

        public override int Id
        {
            get
            {
                return (int)StageId;
            }
            set
            {
                StageId = (StageId)value;
            }
        }
    }
}
