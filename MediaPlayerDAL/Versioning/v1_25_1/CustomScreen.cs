﻿using System.Xml.Serialization;

namespace MediaPlayerDAL.Versioning.v1_25_1
{
    public class CustomScreen : BaseScreen
    {
        [XmlIgnore]
        public CustomScreenId CustomScreenId { get; set; }

        public override int Id
        {
            get
            {
                return (int)CustomScreenId;
            }
            set
            {
                CustomScreenId = (CustomScreenId)value;
            }
        }
    }
}
