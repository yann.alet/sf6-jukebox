﻿namespace MediaPlayerDAL.Versioning.v1_25_1
{
    public class KeyBindings
    {
        public int NextTrack { get; set; }
        public int PreviousTrack { get; set; }
        public int VolumeUp { get; set; }
        public int VolumeDown { get; set; }
        public int Restart { get; set; }
        public int ShowOverlay { get; set; }
    }
}
