﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaPlayerDAL.Model
{
    public class Export
    {
        public Export()
        {
            Stages = new List<Stage>();
            FlowScenes = new List<FlowScene>();
            CustomScreens = new List<CustomScreen>();
            Characters = new List<Character>();
        }
        public string Version { get; set; }
        public List<Stage> Stages { get; set; }
        public List<FlowScene> FlowScenes { get; set; }
        public List<CustomScreen> CustomScreens { get; set; }
        public List<Character> Characters { get; set; }
    }
}
