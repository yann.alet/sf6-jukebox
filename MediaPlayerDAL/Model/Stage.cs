﻿using System.Xml.Serialization;

namespace MediaPlayerDAL.Model
{
    public class Stage : BaseScreen
    {
        [XmlIgnore]
        public StageId StageId { get; set; }

        public override int Id
        {
            get
            {
                return (int)StageId;
            }
            set
            {
                StageId = (StageId)value;
            }
        }
    }
}
