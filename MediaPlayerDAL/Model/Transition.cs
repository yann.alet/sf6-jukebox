﻿using System.Collections.Generic;

namespace MediaPlayerDAL.Model
{
    public class Transition
    {
        public Transition()
        {
            TransitionType = TransitionType.RoundInit;
            Tracks = new List<Track>();
        }

        public TransitionType TransitionType { get; set; }
        public List<Track> Tracks { get; set; }        
    }
}
