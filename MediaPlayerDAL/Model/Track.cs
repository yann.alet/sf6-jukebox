﻿using System.Collections.Generic;

namespace MediaPlayerDAL.Model
{
    public class Track
    {
        public Track()
        {
            Transitions = new List<Transition>();
        }

        public string Path { get; set; }
        public List<Transition> Transitions { get; set; }
        public PlaybackSettings PlaybackSettings { get; set; }

        public Track Clone()
        {
            Track clone = new Track();
            clone.Path = Path;
            if(PlaybackSettings != null)
            {
                clone.PlaybackSettings = PlaybackSettings.Clone();
            }

            if(Transitions != null)
            {
                clone.Transitions = new List<Transition>();
                foreach(var transition in Transitions)
                {
                    var t = new Transition();
                    t.TransitionType = transition.TransitionType;
                    foreach(var track  in transition.Tracks)
                    {
                        t.Tracks.Add(track.Clone());
                    }
                    clone.Transitions.Add(t);
                }
            }

            return clone;
        }
    }
}
