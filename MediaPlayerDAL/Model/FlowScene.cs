﻿using System.Xml.Serialization;

namespace MediaPlayerDAL.Model
{
    public class FlowScene : BaseScreen
    {
        [XmlIgnore]
        public FlowSceneId FlowSceneId { get; set; }        

        public override int Id 
        { 
            get
            {
                return (int)FlowSceneId;
            }
            set
            {
                FlowSceneId = (FlowSceneId)value;
            }
        }        
    }
}
