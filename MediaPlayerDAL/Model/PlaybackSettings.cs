﻿using System;
using System.Xml.Serialization;

namespace MediaPlayerDAL.Model
{
    public class PlaybackSettings
    {
        public PlaybackSettings()
        {
        }

        public PlaybackSettings(float gain)
        {
            Gain = gain;
        }

        [XmlIgnore]
        public float? Volume { get; set; }

        [XmlIgnore]
        public bool? IsInfinite { get; set; }

        public float? Gain { get; set; }

        [XmlIgnore]
        public TimeSpan? StartupSilence { get; set; }

        [XmlIgnore]
        public TimeSpan? StartupTrimming { get; set; }

        [XmlIgnore]
        public TimeSpan? EndTrimming { get; set; }

        [XmlElement("StartupSilence")]
        public string StartupSilenceString
        {
            get { return StartupSilence?.ToString(); }
            set { StartupSilence = !String.IsNullOrEmpty(value) ? TimeSpan.Parse(value) : TimeSpan.Zero; }
        }

        [XmlElement("StartupTrimming")]
        public string StartupTrimmingString
        {
            get { return StartupTrimming?.ToString(); }
            set { StartupTrimming = !String.IsNullOrEmpty(value) ? TimeSpan.Parse(value) : TimeSpan.Zero; }
        }

        [XmlElement("EndTrimming")]
        public string EndTrimmingString
        {
            get { return EndTrimming?.ToString(); }
            set { EndTrimming = !String.IsNullOrEmpty(value) ? TimeSpan.Parse(value) : TimeSpan.Zero; }
        }

        public bool TransitionIsSynced { get; set; }

        public PlaybackSettings Clone()
        {
            var clone = new PlaybackSettings();
            clone.Volume = Volume;
            clone.StartupTrimming = StartupTrimming;
            clone.StartupSilence = StartupSilence;
            clone.TransitionIsSynced = TransitionIsSynced;
            clone.EndTrimming = EndTrimming;
            clone.Gain = Gain;
            clone.IsInfinite = IsInfinite;
            return clone;
        }
    }
}
