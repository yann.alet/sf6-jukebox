﻿using System.Collections.Generic;

namespace MediaPlayerDAL.Model
{
    public class Settings
    {
        public string Version { get; set; }
        public int Enabled { get; set; }
        public string RandomPlaylistPath { get; set; }
        public float Volume { get; set; }
        public int OverlayEnabled { get; set; }
        public MusicOverrideMode MusicOverrideMode { get; set; }
        public PlaylistPriority PlaylistPriority { get; set; }
        public KeyBindings Shortcuts { get; set; }
        public List<Stage> Stages { get; set; }
        public List<FlowScene> FlowScenes { get; set; }
        public List<CustomScreen> CustomScreens { get; set; }
        public List<Character> Characters { get; set; }
    }
}
