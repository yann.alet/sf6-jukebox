﻿using MediaPlayerDAL.Service;
using System.Collections.Generic;

namespace MediaPlayerDAL.Model
{
    public enum PlayMode
    {
        Repeat,
        Once,
        SingleRepeat,
        SingleOnce
    };

    public class Playlist
    {
        public Playlist()
        {
            Tracks = new List<Track>();
        }

        public Playlist(List<string> tracks)
        {
            Tracks = new List<Track>();

            foreach (var track in tracks)
            {
                Tracks.Add(new Track() { Path = track, PlaybackSettings = SerializationService.GetDefaultPlaybackSettings() });
            }
        }

        public PlayMode Mode { get; set; }
        public float FadeOutDuration { get; set; }
        public List<Track> Tracks { get; set; }
    }
}
