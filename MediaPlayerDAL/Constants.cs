﻿using System;
using System.Xml.Serialization;

namespace MediaPlayerDAL
{
    public enum Version
    {
        Unknown,
        v_pre_1_21_0,
        v_1_21_0,
        v_1_22_0,
        v_1_23_0,
        v_1_24_0,
        v_1_25_0,
        v_1_25_1,
        v_1_25_2,
        v_1_26_0,
        v_1_26_1,
        v_1_26_2,
        v_1_26_3,
        v_1_26_4,
        v_1_26_5,
        v_1_26_6,
        v_1_26_7,
        v_1_26_8,
        v_1_26_9,
        v_1_26_10,
        v_1_26_11,
        v_1_26_12,
        v_1_26_13
    }

    [Serializable]
    public enum TransitionType
    {
        [XmlEnum("0")]
        RoundInit = 0,
        [XmlEnum("1")]
        Round1 = 1,
        [XmlEnum("2")]
        Round2 = 2,
        [XmlEnum("3")]
        Round3 = 3,
        [XmlEnum("20")]
        Danger = 20
    }

    [Serializable]
    public enum PlaylistPriority
    {
        [XmlEnum("0")]
        Random = 0,
        [XmlEnum("1")]
        Stage = 1,
        [XmlEnum("2")]
        P1 = 2,
        [XmlEnum("3")]
        P2 = 3,
        [XmlEnum("4")]
        Player = 4,
        [XmlEnum("5")]
        Opponent = 5        
    }

    [Serializable]
    public enum MusicOverrideMode
    {
        [XmlEnum("0")]
        Mixed = 0,
        [XmlEnum("1")]
        Full = 1        
    }

    public enum GameMode : int
    {
        Arcade = 1,
        Practice = 2,
        Versus = 3,
        SpecialMatch = 8,
        WorldTour = 9,
        WorldTourTraining = 10,
        Casual = 15,
        BattleHub = 16,
        Spectate = 22,
        Mode_Unknown = -1
    };

    public enum FightState : int
    {
        StageInit = 0,
        RoundInit = 1,
        Appear = 2,
        Ready = 3,
        Now = 4,
        WinWait = 5,
        Win = 6,
        Finish = 7
    };

    public enum CharacterId : int
    {
        NotSet = 0,
        Ryu = 1,
        Luke = 2,
        Kimberly = 3,
        ChunLi = 4,
        Manon = 5,
        Zangief = 6,
        JP = 7,
        Dhalsim = 8,
        Cammy = 9,
        Ken = 10,
        DeeJay = 11,
        Lily = 12,
        Aki = 13,
        Rashid = 14,
        Blanka = 15,
        Juri = 16,
        Marisa = 17,
        Guile = 18,
        Ed = 19,
        Honda = 20,
        Jamie = 21,
        Akuma = 22,
        Bison = 26,
        Terry = 27,
        Mai = 28,
        PlayerType_Unknown = -1
    };

    public enum CustomScreenId : int
    {
        ResultScreen = 0,
        KimberlyLvl3 = 1,
        KimberlyLvl3Both = 2,
        Unknown = -1,
    }

    [Serializable]
    public enum StageId : int
    {
        WorldTour = 10,
        BarmaleySteelworks = 50000,
        BathersBeach = 90000,
        CarrierByronTaylor = 40100,
        Colosseo = 120000,
        DhalsimerTemple = 70000,
        FeteForaine = 60000,
        GenbuTemple = 20000,
        KingStreet = 100000,
        MetroCityDowntown = 40000,
        OldTownMarket = 10000,
        RangersHut = 80000,
        SuvalhalArena = 10100,
        TheMachoRing = 400000,
        ThunderfootSettlement = 110000,
        TianHongYuan = 30000,
        RuinedLab = 130000,
        EnmaHollow = 20100,
        PaoPaoCafe = 41000,
        TrainingRoom = 0,
        Unknown = -1,
    }

    [Serializable]
    public enum FlowSceneId : int
    {
        eNone = 0,
        eBoot = 1,
        eBootSetup = 2,
        eTitle = 3,
        eESportsMainMenu = 4,
        eModeSelect = 5,
        eReturnModeSelect = 6,
        eBootedTransitionHub = 7,
        eBattleSideSelect = 8,
        eBattleStageSelect = 9,
        eBattleFighterSelect = 10,
        eBattleFighterLeave = 11,
        eBattleFighterVS = 12,
        eBattleFighterEmote = 13,
        eBattleMain = 14, // Fight is starting
        eDeathMatchRuleSelect = 15,
        eDeathMatchMain = 16,
        eTeamBattleRuleSelect = 17,
        eBattleMatchingIn = 18,
        eBattleMatchingOut = 19,
        eBattleAssetReload = 20,
        eTransitionCustomRoom = 21,
        eCustomRoomSetting = 22,
        eCustomRoomInit = 23,
        eCustomRoomInvite = 24,
        eCustomRoomLogin = 25,
        eCustomRoomFromBattleResult = 26,
        eCustomRoomMain = 27,
        eCustomRoomViewingMain = 28,
        eTeamBattleMatchingIn = 29,
        eExamCpuBattleSetup = 30,
        eOnlineCpuBattleSetup = 31,
        eReadyAsset = 32,
        eTrainingSetup = 33,
        eOnlineTrainingSetup = 34,
        eTutorialSetup = 35,
        eTutorialSelect = 36,
        eCharacterGuideSetup = 37,
        eCharacterGuideSelect = 38,
        eComboTrialSetup = 39,
        eComboTrialSelect = 40,
        eReplaySetup = 41,
        eFGReplaySetup = 42,
        eVersusBackReplay = 43,
        eSpectateSetup = 44,
        eSpectateAssetReload = 45,
        eVersusRule = 46,
        eModeSelectBattleHub = 47,
        eModeSelectWorldTour = 48,
        eModeSelectActivity = 49,
        eDeleteSaveDataWorldTour = 50,
        eWorldTourOpening = 51,
        eWorldTourContinue = 52,
        eWorldTourCharacterCreateOpening = 53,
        eWorldTourCharacterCreateShop = 54,
        eWorldTourCity = 55,
        eWorldTourEngageBattleReady = 56,
        eWorldTourEngageBattleOut = 57,
        eWorldTourMatchUpBattleReady = 58,
        eWorldTourMatchUpBattleOut = 59,
        eWorldTourBattle = 60,
        eWorldTourBattleSound = 61,
        eWorldTourMiniGame = 62,
        eWorldTourMiniGameBattle = 63,
        eWorldTourTutorial = 64,
        eWorldTourTutorialBattle = 65,
        eWorldTourOnlineBattle = 66,  // Avatar fight
        eWorldTourSpectateReady = 67,
        eWorldTourTrainingReady = 68,
        eWorldTourOnlineCpuBattleReady = 69,
        eWorldTourTeamBattleReady = 70,
        eWorldTourTeamBattleOut = 71,
        eBattleHubInvite = 72,
        eBattleHubInit = 73,
        eBattleHubCheckAdmissionEvent = 74,
        eBattleHubCheckLearningAiAdmissionEvent = 75,
        eBattleHubLearningAiNextRival = 76,
        eBattleHubLearningAiCharacterSelect = 77,
        eBattleHubLogin = 78,
        eBattleHubMain = 79,
        eBattleHubAvatarBattleIn = 80,
        eBattleHubAvatarBattleOut = 81,
        eBattleHubCharacterCreateOpening = 82,
        eBattleHubCharacterCreateShop = 83, // Character modification in battlehub
        eBattleHubIntroduction = 84,
        eBattleHubPlayContact = 85,
        eBattleHubPrePlayContact = 86,
        eArcadeFighterSelect = 87,
        eArcadeComicDemo = 88,
        eArcadeStageMove = 89,
        eArcadeResult = 90,
        eArcadeContinue = 91,
        eArcadeStaffRoll = 92,
        eArcadeEndCard = 93,
        eArcadePlayThanks = 94,
        eLogin = 95,
        eGalleryTop = 96,
        eGalleryEventCmn = 97,
        eGalleryEventWT = 98,
        eGalleryEventBH = 99,
        eGalleryEventFG = 100,
        eMameMain = 101,
        eMameSpectate = 102,
        eEsfCmn00 = 103,
        eWtfCmn00 = 104,
        eCntVersusDemo = 105,
        eCommentatorResource = 106,
        eCommentatorResourceWT = 107,
        eFGTrainingResource = 108,
        eFGTutorialResource = 109,
        eFGComboTrialResource = 110,
        eFGCharacterGuideResource = 111,
        eFGReplayResource = 112,
        eBattleStats = 113,
        eCtrlBattleFlowSound = 114,
        eCommentatorSystem = 115,
        eCommentatorSystemWT = 116,
        eReturnHome = 117,
        eFlowMain = 118,
        eInvite = 119,
        eTransitionActivityFlow = 120,
        eTransitionFlowMain = 121,
        eResidentData = 122,
        eResident = 123,
    };
}
