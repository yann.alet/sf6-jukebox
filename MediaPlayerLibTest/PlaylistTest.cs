
using MediaPlayerLib;
using MediaPlayerLib.Network;
using MediaPlayerLib.Network.Message;
using Microsoft.VisualStudio.TestPlatform.CommunicationUtilities;

namespace MediaPlayerLibTest
{
    public class PlaylistTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Shuffle()
        {
            Playlist playlist = new Playlist(null);
            var Tracks = new List<string>();
            int TracksCount = 100;
            for(int i = 0; i < TracksCount; i++)
            {
                Tracks.Add(i.ToString());
            }
            
            playlist.Tracks.AddRange(Tracks);
            playlist.Shuffle();

            bool same = true;
            for (int i = 0; i < TracksCount; i++)
            {
                same &= (playlist.Tracks[i] == Tracks[i]);
            }

            Assert.IsTrue(!same);
        }
    }
}