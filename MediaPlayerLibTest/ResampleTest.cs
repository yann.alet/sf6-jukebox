﻿using MediaPlayerLib.NAudio;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaPlayerLibTest
{
    public class ResampleTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Resample()
        {
            string sourceTrackPath = @"D:\Program Files (x86)\Steam\steamapps\common\Street Fighter 6\reframework\CustomTracks\Other\Killer Instinct The Complete Soundtrack [MP3]\56. KI2 Gold - Combo.mp3";
            string testFolder = Path.Combine(Path.GetTempPath(), "JukeboxTest");
            string backupFolder = Path.Combine(testFolder, "Backup");
            string trackToResample = Path.Combine(testFolder, Path.GetFileName(sourceTrackPath));
            if(File.Exists(trackToResample))
            {
                File.Delete(trackToResample);
            }
            if(!Directory.Exists(testFolder))
            {
                Directory.CreateDirectory(testFolder);
            }
            if(!Directory.Exists(backupFolder))
            {
                Directory.CreateDirectory(backupFolder);
            }

            File.Copy(sourceTrackPath, trackToResample);

            /*Resampler resampler = new Resampler(backupFolder, 44100);
            var audioFileReader = new AudioFileReader(trackToResample);
            Assert.That(audioFileReader.WaveFormat.SampleRate, Is.EqualTo(48000));
            audioFileReader.Dispose();
            resampler.Resample(new List<string>() { trackToResample });
            audioFileReader = new AudioFileReader(trackToResample);
            Assert.That(audioFileReader.WaveFormat.SampleRate, Is.EqualTo(44100));
            audioFileReader.Dispose();
            var sourceTrackTag = TagLib.File.Create(sourceTrackPath).Tag;
            var resampledTrackTag = TagLib.File.Create(trackToResample).Tag;
            Assert.That(resampledTrackTag.Title, Is.EqualTo(sourceTrackTag.Title));
            Directory.Delete(testFolder, true);*/
        }
    }
}
