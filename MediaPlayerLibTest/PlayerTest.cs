﻿using MediaPlayerDAL.Service;
using MediaPlayerLib;

namespace MediaPlayerLibTest
{
    public class PlayerTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void Play()
        {
            SerializationService serialization = new SerializationService();
            var settings = serialization.Load();
            settings.Volume = 1;
            settings.OverlayEnabled = 0;
            ThreadedPlayer player = new ThreadedPlayer(settings, new DebugLogger());
            var playbackSettings = new MediaPlayerDAL.Model.PlaybackSettings();
            playbackSettings.StartupTrimming = TimeSpan.FromSeconds(32);
            //playbackSettings.EndTrimming = TimeSpan.FromSeconds(60);
            var track = new Track(@"F:\Mix\Enemy(PaglaSongs).mp3", playbackSettings);
            player.Play(track);
            //Thread.Sleep(1000 * 32);
            Thread.Sleep(1000 * 10000);
        }
    }
}
