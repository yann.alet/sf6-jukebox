﻿namespace SF6JukeboxOverlay.Model
{
    public class TrackUpdate
    {
        public string Track { get; set; }
        public float Progress { get; set; }

        public override string ToString()
        {
            return $"Track: {Track}, Progress: {Progress}";
        }
    }
}
