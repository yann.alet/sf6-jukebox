﻿using MediaPlayerDAL;

namespace SF6JukeboxOverlay.Model
{
    public class GameModeUpdate
    {
        public GameModeUpdate(int gameMode)
        {
            GameMode = (GameMode)gameMode;
        }

        public GameMode GameMode { get; }
    }
}
