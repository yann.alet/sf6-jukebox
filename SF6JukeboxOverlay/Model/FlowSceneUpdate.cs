﻿using MediaPlayerDAL;

namespace SF6JukeboxOverlay.Model
{
    public class FlowSceneUpdate
    {
        public FlowSceneUpdate(int flowScene)
        {
            FlowScene = (FlowSceneId)flowScene;
        }

        public FlowSceneId FlowScene { get; }
    }
}
