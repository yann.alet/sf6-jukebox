﻿using MediaPlayerDAL;

namespace SF6JukeboxOverlay.Model
{
    public class FightStateUpdate
    {
        public FightStateUpdate(int fightState)
        {
            FightState = (FightState)fightState;
        }

        public FightState FightState { get; }
    }
}
