﻿using MediaPlayerLib.Network.Contract;
using System;
using System.IO;
using System.Text;
using System.Threading;

namespace SF6JukeboxOverlay
{
    public class FileLogger : ILogger
    {
        Semaphore Locker = new Semaphore(1, 1);

        public FileLogger(string filePath)
        {
            FilePath = filePath;
            if(File.Exists(FilePath)) 
            { 
                File.Delete(FilePath);
            }
        }

        public string FilePath { get; }

        public void Log(string message)
        {
            Locker.WaitOne();
            string formattedDate = DateTime.Now.ToString("[yyyy-MM-dd HH:mm:ss.fff]");
            message = $"{formattedDate} : {message}\r\n";
            using(var fs = new FileStream(FilePath, FileMode.Append))
            {
                var bytes = Encoding.UTF8.GetBytes(message);
                fs.Write(bytes, 0, bytes.Length);
                fs.Flush();
            }
            Locker.Release();
        }
    }
}
