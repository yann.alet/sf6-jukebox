﻿using MediaPlayerLib.Network.Contract;
using SF6JukeboxOverlay.GlobalEvents;
using SF6JukeboxOverlay.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Windows;
using System.Threading;
using MediaPlayerDAL;

namespace SF6JukeboxOverlay.ViewModel
{
    public abstract class BaseSkinViewModel : BaseViewModel
    {
        public BaseSkinViewModel(OverlayClientService overlayClientService, ILogger logger)
        {
            OverlayClientService = overlayClientService;
            Logger = logger;
            OverlayClientService.OnTrackUpdate += OverlayClientService_OnTrackUpdate;
            OverlayClientService.OnFightStateUpdate += OverlayClientService_OnFightStateUpdate;
            OverlayClientService.OnGameModeUpdate += OverlayClientService_OnGameModeUpdate;
            OverlayClientService.OnFlowSceneUpdate += OverlayClientService_OnFlowSceneUpdate;
            OverlayClientService.OnShowOverlay += OnShowOverlay;
            OverlayClientService.OnNetworkError += OverlayClientService_OnNetworkError;
            OverlayClientService.Start();
            IsFirstRound = true;
        }

        private float progress;
        private string artist;
        private string album;
        private string title;
        private BitmapImage cover;
        private Visibility coverVisibility;

        private OverlayClientService OverlayClientService { get; }
        private string LastPlayedTrack { get; set; }
        protected FightState LastFightState { get; set; }
        protected GameMode LastGameMode { get; set; }
        protected FlowSceneId LastFlowScene { get; set; }
        private ILogger Logger { get; }

        public string Title
        {
            get => title;
            set
            {
                title = value;
                OnPropertyChanged();
            }
        }
        public string Album
        {
            get => album;
            set
            {
                album = value;
                OnPropertyChanged();
            }
        }
        public string Artist
        {
            get => artist;
            set
            {
                artist = value;
                OnPropertyChanged();
            }
        }

        public float Progress
        {
            get => progress;
            set
            {
                progress = value;
                OnPropertyChanged();
            }
        }

        public BitmapImage Cover
        {
            get => cover;
            set
            {
                cover = value;
                OnPropertyChanged();
            }
        }

        public Visibility CoverVisibility
        {
            get => coverVisibility;
            set
            {
                coverVisibility = value;
                OnPropertyChanged();
            }
        }

        public bool IsFirstRound { get; set; }


        private void OverlayClientService_OnNetworkError(object sender, string e)
        {
            OverlayClientService.Stop();
            Logger.Log(e);
        }

        public virtual void OnShowOverlay(object sender, System.EventArgs e)
        {
            EventAggregator.Publish(new ShowOverlayEvent());
        }

        private void OverlayClientService_OnFlowSceneUpdate(object sender, Model.FlowSceneUpdate e)
        {
            Logger.Log($"On Flow Scene Update: {e.FlowScene}");

            if (LastFlowScene != e.FlowScene)
            {
                LastFlowScene = e.FlowScene;
            }
        }

        private void OverlayClientService_OnGameModeUpdate(object sender, Model.GameModeUpdate e)
        {
            Logger.Log($"On Game Mode Update: {e.GameMode}");

            if (e.GameMode == 0)
            {
                IsFirstRound = true;
            }

            if (LastGameMode != e.GameMode)
            {
                LastGameMode = e.GameMode;
            }            
        }

        private void OverlayClientService_OnFightStateUpdate(object sender, Model.FightStateUpdate e)
        {
            Logger.Log($"On Fight State Update: {e.FightState}");

            if (e.FightState > FightState.Ready && e.FightState < FightState.Finish)
            {
                IsFirstRound = false;
            }

            if(e.FightState == FightState.Finish)
            {
                IsFirstRound = true;
            }

            if (LastFightState != e.FightState)
            {
                if (IsFirstRound && e.FightState == FightState.Ready)
                {
                    EventAggregator.Publish(new ShowOverlayEvent()); ;
                }
                LastFightState = e.FightState;
            }
        }

        private void OverlayClientService_OnTrackUpdate(object sender, Model.TrackUpdate e)
        {
            if (LastPlayedTrack != e.Track)
            {
                Logger.Log($"On Track Update: {e}");
                TagLib.File file = TagLib.File.Create(e.Track);
                Progress = e.Progress * 100;
                Title = String.IsNullOrEmpty(file.Tag.Title) ? Path.GetFileNameWithoutExtension(e.Track) : file.Tag.Title;
                Album = String.IsNullOrEmpty(file.Tag.Album) ? "Unknown" : file.Tag.Album; ;
                if (file.Tag.AlbumArtists.Any())
                {
                    Artist = file.Tag.AlbumArtists[0];
                }

                if (file.Tag.Pictures.Any())
                {
                    CoverVisibility = Visibility.Visible;
                    Cover = GetBitmapImageFromByteArray(file.Tag.Pictures.First().Data.Data);
                }
                else
                {
                    CoverVisibility = Visibility.Collapsed;
                }

                if ((LastFightState > 0 && LastFightState < FightState.Finish && (LastFlowScene == FlowSceneId.eWorldTourBattle || LastFlowScene == FlowSceneId.eBattleMain))
                    || LastFlowScene == FlowSceneId.eWorldTourCity)
                {
                    EventAggregator.Publish(new ShowOverlayEvent()); ;
                }

                LastPlayedTrack = e.Track;
            }

            Progress = e.Progress * 100;
        }

        private BitmapImage GetBitmapImageFromByteArray(byte[] data)
        {
            try
            {
                using (var mStream = new MemoryStream())
                {
                    byte[] pData = data;
                    mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
                    mStream.Flush();
                    mStream.Position = 0;
                    BitmapImage bitmapimage = new BitmapImage();
                    bitmapimage.BeginInit();
                    bitmapimage.StreamSource = mStream;
                    bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapimage.EndInit();
                    mStream.Dispose();
                    return bitmapimage;
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex.ToString());
                return null;
            }
        }
    }
}
