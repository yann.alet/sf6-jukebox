﻿using System.Windows.Media.Imaging;
using System.Windows;
using SF6JukeboxOverlay.WindowsInterop;
using System.Diagnostics;
using System;
using System.IO;

namespace SF6JukeboxOverlay.ViewModel
{
    internal class MockMainViewModel : BaseViewModel
    {
        public MockMainViewModel()
        {
            Title = "The Way U Move";
            Artist = "Faye Newborough & Chris Sutherland";
            Album = "Killer Instinct: The Complete Soundtrack";
            Progress = 30.0f;
            Process[] processes = Process.GetProcessesByName("StreetFighter6");
            //Process[] processes = Process.GetProcessesByName("vlc");
            //Process[] processes = Process.GetProcessesByName("GBVS-Win64-Shipping");
            
            if (processes.Length > 0)
            {
                Process sf6Process = processes[0];
                IntPtr ptr = sf6Process.MainWindowHandle;
                //var bitmap = User32.PrintWindow(ptr);
                User32.GetWindowRect(ptr, out RECT region);
                float startRatioX = 0.2f;
                float startRatioY = 0.92f;
                float endRatioX = 0.8f;
                float endRatioY = 0.98f;
                int top = Convert.ToInt32(region.Top + region.Height * startRatioY);
                int left = Convert.ToInt32(region.Left + region.Width * startRatioX);
                int bottom = Convert.ToInt32(region.Top + region.Height * endRatioY);
                int right = Convert.ToInt32(region.Left + region.Width * endRatioX);
                region = new RECT(left, top, right, bottom);
                var image =User32.GetDesktopImage(region);
                byte[] bitmapData;
                using (MemoryStream ms = new MemoryStream())
                {
                    image.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                    bitmapData = ms.ToArray();
                }

                Cover = GetBitmapImageFromByteArray(bitmapData);


                /*string capturePath = @"d:\sf6_capture.bmp";
                if(File.Exists(capturePath))
                {
                    File.Delete(capturePath );
                }

                bitmap.Save(capturePath);*/
            }
        }

        private BitmapImage GetBitmapImageFromByteArray(byte[] data)
        {
            try
            {
                using (var mStream = new MemoryStream())
                {
                    byte[] pData = data;
                    mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
                    mStream.Flush();
                    mStream.Position = 0;
                    BitmapImage bitmapimage = new BitmapImage();
                    bitmapimage.BeginInit();
                    bitmapimage.StreamSource = mStream;
                    bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapimage.EndInit();
                    mStream.Dispose();
                    return bitmapimage;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private float progress;
        private string artist;
        private string album;
        private string title;
        private BitmapImage cover;
        private Visibility coverVisibility;
        
        public string Title
        {
            get => title;
            set
            {
                title = value;
                OnPropertyChanged();
            }
        }
        public string Album
        {
            get => album;
            set
            {
                album = value;
                OnPropertyChanged();
            }
        }
        public string Artist
        {
            get => artist;
            set
            {
                artist = value;
                OnPropertyChanged();
            }
        }

        public float Progress
        {
            get => progress;
            set
            {
                progress = value;
                OnPropertyChanged();
            }
        }

        public BitmapImage Cover
        {
            get => cover;
            set
            {
                cover = value;
                OnPropertyChanged();
            }
        }

        public Visibility CoverVisibility
        {
            get => coverVisibility;
            set
            {
                coverVisibility = value;
                OnPropertyChanged();
            }
        }        
    }
}
