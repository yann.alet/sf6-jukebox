﻿using SF6JukeboxOverlay.Service;
using MediaPlayerLib.Network.Contract;

namespace SF6JukeboxOverlay.ViewModel
{
    public class MainWindowViewModel : BaseSkinViewModel
    {
        public MainWindowViewModel(OverlayClientService overlayClientService, ILogger logger) : base(overlayClientService, logger)
        {
        }
    }
}
