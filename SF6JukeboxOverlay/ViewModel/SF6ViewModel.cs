﻿using MediaPlayerLib.Network.Contract;
using SF6JukeboxOverlay.Service;
using SF6JukeboxOverlay.WindowsInterop;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace SF6JukeboxOverlay.ViewModel
{
    public class SF6ViewModel : BaseSkinViewModel
    {
        public SF6ViewModel(OverlayClientService overlayClientService, ILogger logger) : base(overlayClientService, logger)
        {
            UpdateBackgroud();
        }

        private void UpdateBackgroud()
        {
            Process[] processes = Process.GetProcessesByName("StreetFighter6");

            if (processes.Length > 0)
            {
                var point = ((MainWindow)Application.Current.MainWindow).ComputeTopLeft();
                Process sf6Process = processes[0];
                IntPtr ptr = sf6Process.MainWindowHandle;
                User32.GetWindowRect(ptr, out RECT region);
                float startRatioX = 0.2f;
                float startRatioY = 0.92f;
                float endRatioX = 0.8f;
                float endRatioY = 0.98f;
                int top = (int)point.Y;
                int left = (int)point.X;
                int bottom = top + 45;
                int right = left + 600;
                region = new RECT(left, top, right, bottom);
                var image = User32.GetDesktopImage(region);
                byte[] bitmapData;
                using (MemoryStream ms = new MemoryStream())
                {
                    image.Save(ms, ImageFormat.Bmp);
                    bitmapData = ms.ToArray();
                }

                BackgroundImage = GetBitmapImageFromByteArray(bitmapData);          
            }
        }

        public override void OnShowOverlay(object sender, EventArgs e)
        {
            base.OnShowOverlay(sender, e);
            if (LastFightState == 0)
            {
                BackgroundImage = null;
            }
            else
            {
                UpdateBackgroud();
            }
        }

        
        BitmapImage backgroundImage;

        public BitmapImage BackgroundImage
        {
            get => backgroundImage;
            set
            {
                backgroundImage = value;
                OnPropertyChanged();
            }
        }

        private BitmapImage GetBitmapImageFromByteArray(byte[] data)
        {
            try
            {
                using (var mStream = new MemoryStream())
                {
                    byte[] pData = data;
                    mStream.Write(pData, 0, Convert.ToInt32(pData.Length));
                    mStream.Flush();
                    mStream.Position = 0;
                    BitmapImage bitmapimage = new BitmapImage();
                    bitmapimage.BeginInit();
                    bitmapimage.StreamSource = mStream;
                    bitmapimage.CacheOption = BitmapCacheOption.OnLoad;
                    bitmapimage.EndInit();
                    mStream.Dispose();
                    return bitmapimage;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
