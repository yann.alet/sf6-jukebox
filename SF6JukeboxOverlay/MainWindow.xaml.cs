﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;
using System.Windows.Media.Animation;
using System.Windows.Threading;
using SF6JukeboxOverlay.Service;
using SF6JukeboxOverlay.GlobalEvents;
using SF6JukeboxOverlay.ViewModel;
using System.Windows.Media;

namespace SF6JukeboxOverlay
{

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr FindWindow(string strClassName, string strWindowName);

        [DllImport("user32.dll")]
        public static extern bool GetWindowRect(IntPtr hwnd, ref Rect rectangle);

        [DllImport("user32.dll")]
        public static extern int SetForegroundWindow(IntPtr hwnd);

        public struct Rect
        {
            public int Left { get; set; }
            public int Top { get; set; }
            public int Right { get; set; }
            public int Bottom { get; set; }
        }

        FileLogger FileLogger { get; set; }
        Thread ProcessWatcherThread { get; set; }
        DispatcherTimer OutroTimer { get; set; }
        Storyboard IntroStoryboard { get; set; }
        Storyboard OutroStoryboard { get; set; }

        public bool DemoMode => false;

        public MainWindow(FileLogger logger)
        {
            InitializeComponent();
            FileLogger = logger;
            IntroStoryboard = Resources["ShowSB"] as Storyboard;
            OutroStoryboard = Resources["HideSB"] as Storyboard;
            if (DemoMode)
            {
                DataContext = new MockMainViewModel();
                ShowActivated = true;
                Topmost = true;
                this.MouseDown += MainWindow_MouseDown;
            }
            else
            {
                DataContext = new MainWindowViewModel(new OverlayClientService(logger), logger);
                //DataContext = new SF6ViewModel(new OverlayClientService(logger), logger);
                EventAggregator.Subscribe<ShowOverlayEvent>(OnShowOverlayEvent);
                OutroStoryboard.Completed += OutroStoryboard_Completed;
                ProcessWatcherThread = new Thread(ProcessWatcher);
                ProcessWatcherThread.Start();
                OutroTimer = new DispatcherTimer();
                OutroTimer.Interval = TimeSpan.FromSeconds(5);
                OutroTimer.Tick += OutroTimer_Tick;            
                PlaceOffscreen();
            }
        }

        private void MainWindow_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void OnShowOverlayEvent(ShowOverlayEvent @event)
        {
            ShowOverlay();
        }

        private void OutroStoryboard_Completed(object sender, EventArgs e)
        {
            OutroTimer.Stop();
            PlaceOffscreen();
        }

        private void OutroTimer_Tick(object sender, EventArgs e)
        {
            PlayOutro();
        }

        void PlayIntro()
        {
            IntroStoryboard.Begin();            
        }

        void PlayOutro()
        {
            FileLogger.Log("PlayOutro");
            OutroStoryboard.Begin();
        }

        void PlaceOffscreen()
        {
            FileLogger.Log("PlaceOffscreen");
            Top = -1000;
            Opacity = 0;
        }

        void ShowOverlay()
        {
            FileLogger.Log("ShowOverlay");
            Center();
            PlayIntro();            
            OutroTimer.Start();
        }

        private void ProcessWatcher(object obj)
        {
            bool keepWatching = true;
            while (keepWatching)
            {
                if(Process.GetProcessesByName("StreetFighter6").Length == 0)
                {
                    if (Application.Current != null)
                    {
                        Application.Current.Dispatcher.Invoke(new Action(() =>
                        {
                            Close();
                        }));
                    }
                    keepWatching = false;
                }

                Thread.Sleep(100);
            }
        }

        public Point ComputeTopLeft()
        {
            FileLogger.Log("ComputeTopLeft");
            Point topLeft = new Point();
            Process[] processes = Process.GetProcessesByName("StreetFighter6");
            if (processes.Length > 0)
            {
                var scalling = GetWindowsScaling();
                Process sf6Process = processes[0];
                IntPtr ptr = sf6Process.MainWindowHandle;
                Rect rect = new Rect();
                GetWindowRect(ptr, ref rect);
                rect = new Rect()
                {
                    Left = Convert.ToInt32(((float)rect.Left)/ scalling.X),
                    Top = Convert.ToInt32(((float)rect.Top)/ scalling.Y),
                    Right = Convert.ToInt32(((float)rect.Right)/ scalling.X),
                    Bottom = Convert.ToInt32(((float)rect.Bottom)/ scalling.Y)
                };
                var windowWidth = Application.Current.MainWindow.ActualWidth;
                var windowHeight = Application.Current.MainWindow.ActualHeight;
                var sf6Width = rect.Right - rect.Left;
                
                topLeft.Y = rect.Bottom - (windowHeight / 2) - 50.0f;
                topLeft.X = rect.Left + (sf6Width / 2) - (windowWidth / 2);
            }
            else
            {
                topLeft.Y = 300;
                topLeft.X = 300;
            }

            
            return topLeft;
        }

        private Point GetWindowsScaling()
        {
            PresentationSource source = PresentationSource.FromVisual(this);
            if (source != null)
            {
                Matrix transformToDevice = source.CompositionTarget.TransformToDevice;
                double dpiX = transformToDevice.M11 * 96;
                double dpiY = transformToDevice.M22 * 96;

                // Calculate the scaling factor
                double scaleX = dpiX / 96.0;
                double scaleY = dpiY / 96.0;

                return new Point(scaleX, scaleY);
            }

            return new Point(1, 1);
        }

        private void Center()
        {
            FileLogger.Log("Center at:");
            Topmost = true;
            var topLeft = ComputeTopLeft();
            FileLogger.Log($"Top: {topLeft.Y}, Left: {topLeft.X}");
            Top = topLeft.Y;
            Left = topLeft.X;
        }
    }
}
