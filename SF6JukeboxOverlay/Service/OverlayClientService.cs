﻿using MediaPlayerLib.Network;
using MediaPlayerLib.Network.Contract;
using MediaPlayerLib.Network.Message;
using SF6JukeboxOverlay.Model;
using System;
using System.Windows;

namespace SF6JukeboxOverlay.Service
{
    public class OverlayClientService
    {
        public event EventHandler<GameModeUpdate> OnGameModeUpdate;
        public event EventHandler<FlowSceneUpdate> OnFlowSceneUpdate;
        public event EventHandler<FightStateUpdate> OnFightStateUpdate;
        public event EventHandler<TrackUpdate> OnTrackUpdate;
        public event EventHandler OnShowOverlay;
        public event EventHandler<string> OnNetworkError;

        public OverlayClientService(ILogger logger)
        {
            OverlayClient = new OverlayClient(1212, logger);
            OverlayClient.OnTrackUpdateMessage += OverlayClient_OnTrackUpdateMessage; ;
            OverlayClient.OnFightStateChangedMessage += OverlayClient_OnFightStateChangedMessage;
            OverlayClient.OnFlowSceneChangedMessage += OverlayClient_OnFlowSceneChangedMessage;
            OverlayClient.OnGameModeChangedMessage += OverlayClient_OnGameModeChangedMessage;
            OverlayClient.OnShowOverlayMessage += OverlayClient_OnShowOverlayMessage; ;
            OverlayClient.OnNetworkError += OverlayClient_OnNetworkError; ;
        }

        OverlayClient OverlayClient { get; }

        public void Start()
        {
            OverlayClient.Start();
        }

        public void Stop()
        {
            OverlayClient.Stop();
        }

        private void OverlayClient_OnNetworkError(object sender, string e)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                OnNetworkError?.Invoke(sender, e);
            }));
        }

        private void OverlayClient_OnShowOverlayMessage(object sender, ShowOverlayMessage e)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                OnShowOverlay?.Invoke(this, EventArgs.Empty);
            }));
        }

        private void OverlayClient_OnTrackUpdateMessage(object sender, TrackUpdateMessage e)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                OnTrackUpdate?.Invoke(this, new TrackUpdate { Track = e.Track, Progress = e.Progress });
            }));
        }

        private void OverlayClient_OnFightStateChangedMessage(object sender, FightStateChangedMessage e)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                OnFightStateUpdate?.Invoke(this, new FightStateUpdate(e.FightState));
            }));
        }

        private void OverlayClient_OnGameModeChangedMessage(object sender, GameModeChangedMessage e)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                OnGameModeUpdate?.Invoke(this, new GameModeUpdate(e.GameMode));
            }));
        }

        private void OverlayClient_OnFlowSceneChangedMessage(object sender, FlowSceneChangedMessage e)
        {
            Application.Current.Dispatcher.Invoke(new Action(() =>
            {
                OnFlowSceneUpdate?.Invoke(this, new FlowSceneUpdate(e.FlowScene));
            }));
        }
    }
}
