﻿using MediaPlayerDAL.Service;
using MediaPlayerLib;
using MediaPlayerLib.Network.Contract;
using PlaylistManager.Contract;
using PlaylistManager.GlobalEvent;
using PlaylistManager.Service;
using PlaylistManager.View;
using PlaylistManager.View.TrackEditor;
using PlaylistManager.ViewModel;
using PlaylistManager.ViewModel.Export;
using PlaylistManager.ViewModel.TrackEditor;
using System;
using System.Windows;
using System.Windows.Media.Effects;

namespace PlaylistManager
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IFreezableWindow
    {
        private LoadingScreenService _loadingScreenService;
        private MessageBoxService _messageService;

        public MainWindow()
        {
            InitializeComponent();
            _loadingScreenService = new LoadingScreenService();
            _messageService = new MessageBoxService();
            WindowStartupLocation = WindowStartupLocation.CenterScreen;
            Title = $"{Title} - v{SerializationService.LatestVersion}";
            EventAggregator.Subscribe<MessageBoxEvent>(OnMessageBoxEvent);
            EventAggregator.Subscribe<TrackEditorRequestedEvent>(OnTrackEditorRequested);
            EventAggregator.Subscribe<ExportPlaylistRequestedEvent>(OnExportPlaylistRequested);
            EventAggregator.Subscribe<ImportPlaylistRequestedEvent>(OnImportPlaylistRequested);
            this.Closed += MainWindow_Closed;
            this.Loaded += MainWindow_Loaded;
        }        
        
        public void BlurBackground()
        {
            RootGrid.Effect = new BlurEffect() { Radius = 10 };
            DarkenBorder.Visibility = Visibility.Visible;
        }

        public void UnBlurBackground()
        {
            RootGrid.Effect = null;
            DarkenBorder.Visibility = Visibility.Collapsed;
        }

        ILogger Logger
        {
            get
            {
                return ((MainWindowViewModel)DataContext).Logger;
            }
        }

        private MainWindowViewModel ViewModel
        {
            get
            {
                return DataContext as MainWindowViewModel;
            }
        }

        private TrackEditorViewModel GetTrackEditorViewModel(TrackViewModel trackViewModel)
        {
            var vm = new TrackEditorViewModel(_messageService, _loadingScreenService, ((MainWindowViewModel)DataContext).PlayerService, trackViewModel);
            vm.Volume = ViewModel.Settings.Volume;
            return vm;
        }

        private ExportPlaylistViewModel GetExportPlaylistViewModel()
        {
            var vm = (MainWindowViewModel)DataContext;
            return new ExportPlaylistViewModel(vm.SettingsModel, _messageService, _loadingScreenService);            
        }

        private ImportPlaylistViewModel GetImportPlaylistViewModel(string playlistFilePath)
        {
            var vm = (MainWindowViewModel)DataContext;
            return new ImportPlaylistViewModel(playlistFilePath, vm.SettingsModel, _messageService, _loadingScreenService);
        }

        private void OnExportPlaylistRequested(ExportPlaylistRequestedEvent e)
        {
            BlurBackground();
            var exportPlaylistView = new ExportPlaylistView();
            exportPlaylistView.DataContext = GetExportPlaylistViewModel();
            exportPlaylistView.Show();
            _loadingScreenService.SetOwner(exportPlaylistView);
            _messageService.SetOwner(exportPlaylistView);
            exportPlaylistView.Closed += ExportPlaylistView_Closed;
        }

        private void OnImportPlaylistRequested(ImportPlaylistRequestedEvent e)
        {
            BlurBackground();
            var importPlaylistView = new ImportPlaylistView();
            var vm = GetImportPlaylistViewModel(e.PlaylistFilePath);
            importPlaylistView.DataContext = vm;
            importPlaylistView.Show();
            _loadingScreenService.SetOwner(importPlaylistView);
            _messageService.SetOwner(importPlaylistView);
            importPlaylistView.Closed += ImportPlaylistView_Closed;
        }

        private void ImportPlaylistView_Closed(object sender, EventArgs e)
        {
            UnBlurBackground();
            _loadingScreenService.SetOwner(this);
            _messageService.SetOwner(this);
        }

        private void ExportPlaylistView_Closed(object sender, EventArgs e)
        {
            UnBlurBackground();
            _loadingScreenService.SetOwner(this);
            _messageService.SetOwner(this);
        }

        private void OnTrackEditorRequested(TrackEditorRequestedEvent e)
        {
            BlurBackground();
            var trackEditorView = new TrackEditorView();
            var vm = GetTrackEditorViewModel(e.Track);
            trackEditorView.DataContext = vm;
            trackEditorView.Show();
            _loadingScreenService.SetOwner(trackEditorView);
            _messageService.SetOwner(trackEditorView);
            _loadingScreenService.Show("Loading tracks...", () => vm.Init());
            trackEditorView.Closed += TrackEditorView_Closed;
        }

        private void TrackEditorView_Closed(object sender, EventArgs e)
        {
            if (sender != null)
            {
                var trackEditorView = (TrackEditorView)sender;
                var vm = (TrackEditorViewModel)trackEditorView.DataContext;
                trackEditorView.Closed -= TrackEditorView_Closed;
                UnBlurBackground();
                _loadingScreenService.SetOwner(this);
                _messageService.SetOwner(this);
                if(vm.SaveOnClose)
                {
                    ViewModel.Save();                    
                }
            }
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            _loadingScreenService.SetOwner(this);
            _messageService.SetOwner(this);
            _loadingScreenService.Show("Loading...", () =>
            {
                var mvm = new MainWindowViewModel(_loadingScreenService, _messageService);
                mvm.Load();
                BringIntoView();
                DataContext = mvm;                
            });
        }        

        private void MainWindow_Closed(object sender, System.EventArgs e)
        {
            ((MainWindowViewModel)DataContext).Dispose();
        }

        private void OnMessageBoxEvent(MessageBoxEvent messageBoxEvent)
        {
            var messageBoxView = new MessageBoxView();
            var vm = new MessageBoxViewModel(messageBoxEvent.Title, messageBoxEvent.Message, messageBoxEvent.Type);
            vm.Title = messageBoxEvent.Title;
            vm.Message = messageBoxEvent.Message;            
            switch (messageBoxEvent.Type)
            {
                case MessageBoxType.Ok:
                    vm.OnOk = messageBoxEvent.OnOk;
                    break;

                case MessageBoxType.YesNo:
                    vm.OnYes = messageBoxEvent.OnYes;
                    vm.OnNo = messageBoxEvent.OnNo;
                    break;

                case MessageBoxType.YesNoCancel:
                    vm.OnYes = messageBoxEvent.OnYes;
                    vm.OnNo = messageBoxEvent.OnNo;
                    vm.OnCancel = messageBoxEvent.OnCancel;
                    break;
            }
            messageBoxView.DataContext = vm;
            if (messageBoxEvent.Owner != null)
            {
                messageBoxView.Owner = messageBoxEvent.Owner;
            }
            messageBoxView.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            messageBoxView.ShowDialog();
            BringIntoView();
            Focus();
        }
    }
}
