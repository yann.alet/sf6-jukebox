﻿using System.Runtime.InteropServices;

namespace PlaylistManager.Native.User32
{
    public class Api
    {
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetCursorPos(out POINT point);

        [DllImport("User32.dll")]
        public static extern bool SetCursorPos(int X, int Y);
    }
}
