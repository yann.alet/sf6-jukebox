﻿using System.Runtime.InteropServices;
using System;

namespace PlaylistManager.Native.User32
{
    [StructLayout(LayoutKind.Sequential)]
    public struct POINT
    {
        public Int32 X;
        public Int32 Y;
    }
}