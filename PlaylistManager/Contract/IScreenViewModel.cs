﻿using MediaPlayerDAL.Contract;
using PlaylistManager.Service;
using PlaylistManager.ViewModel;
using System.Collections.ObjectModel;

namespace PlaylistManager.Contract
{
    public interface IScreenViewModel<T> where T : IScreen
    {
        string Name { get; }
        T Model { get; }
        ObservableCollection<TrackViewModel> Playlist { get; set; }
        PlayerService PlayerService { get; }
        RelayCommand AddTrackCommand { get; set; }
        RelayCommand RemoveTrackCommand { get; set; }
        bool IsRandomPlaylist { get; set; }
        string ThumbnailPath { get; }
        bool IsValid { get; }
        bool SupportsDynamicTracks { get; }
        T ToModel();
    }
}
