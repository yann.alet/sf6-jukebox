﻿namespace PlaylistManager.Contract
{
    public interface IFreezableWindow
    {
        void BlurBackground();
        void UnBlurBackground();
    }
}
