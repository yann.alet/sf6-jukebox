﻿using PlaylistManager.ViewModel.TrackEditor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace PlaylistManager.View.TrackEditor
{
    /// <summary>
    /// Interaction logic for TimeNeedleView.xaml
    /// </summary>
    public partial class TimeNeedleView : UserControl
    {
        DispatcherTimer UpdateTime { get; set; }
        DateTime PlayTime { get; set; }
        double CurrentTime { get; set; }

        public TimeNeedleView()
        {
            InitializeComponent();
            Loaded += TimeNeedleView_Loaded;
            UpdateTime = new DispatcherTimer();
            UpdateTime.Interval = TimeSpan.FromMilliseconds(16);
            UpdateTime.Tick += UpdateTime_Tick;
            UpdateTime.IsEnabled = false;
        }

        private void UpdateTime_Tick(object sender, EventArgs e)
        {
            double totalTime = (DateTime.Now - PlayTime).TotalMilliseconds;
            double offset = totalTime / PixelLengthInMilliSeconds;
            Margin = new Thickness(offset, Margin.Top, Margin.Right, Margin.Bottom);
        }

        public TrackEditorViewModel ViewModel { get; set; }

        private void TimeNeedleView_Loaded(object sender, RoutedEventArgs e)
        {
            ViewModel = (TrackEditorViewModel)DataContext;
            if (ViewModel != null)
            {
                ViewModel.OnPlay += ViewModel_OnPlay;
                ViewModel.OnStop += ViewModel_OnStop;
                ViewModel.OnPause += ViewModel_OnPause;
                ViewModel.OnResume += ViewModel_OnResume;
                ViewModel.OnSetPosition += ViewModel_OnSetPosition;
            }
        }

        private void ViewModel_OnResume(object sender, EventArgs e)
        {
            ViewModel_OnSetPosition(null, (int)CurrentTime);
            UpdateTime.IsEnabled = true;
        }

        private void ViewModel_OnPause(object sender, EventArgs e)
        {
            UpdateTime.IsEnabled = false;
            CurrentTime = (DateTime.Now - PlayTime).TotalMilliseconds;
        }

        private void ViewModel_OnSetPosition(object sender, int e)
        {
            PlayTime = DateTime.Now - TimeSpan.FromMilliseconds(e);
        }

        private void ViewModel_OnStop(object sender, EventArgs e)
        {
            UpdateTime.Stop();
            Margin = new Thickness(0, Margin.Top, Margin.Right, Margin.Bottom);
        }

        private void ViewModel_OnPlay(object sender, EventArgs e)
        {
            PlayTime = DateTime.Now;
            UpdateTime.Start();
        }

        public int PixelLengthInMilliSeconds
        {
            get { return (int)GetValue(PixelLengthInMilliSecondsProperty); }
            set { SetValue(PixelLengthInMilliSecondsProperty, value); }
        }

        public static readonly DependencyProperty PixelLengthInMilliSecondsProperty =
            DependencyProperty.Register("PixelLengthInMilliSeconds", typeof(int), typeof(TimeNeedleView), new PropertyMetadata(100));        
    }
}
