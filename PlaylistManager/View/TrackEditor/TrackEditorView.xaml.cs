﻿using PlaylistManager.Contract;
using PlaylistManager.ViewModel;
using PlaylistManager.ViewModel.TrackEditor;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Effects;

namespace PlaylistManager.View.TrackEditor
{
    /// <summary>
    /// Interaction logic for TrackEditorView.xaml
    /// </summary>
    public partial class TrackEditorView : Window, IFreezableWindow
    {
        public TrackEditorView()
        {
            InitializeComponent();
            this.Closing += TrackEditorView_Closing;
            this.Closed += TrackEditorView_Closed;
        }

        private void TrackEditorView_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ViewModel.OnClosing(e);            
        }

        private void TrackEditorView_Closed(object sender, System.EventArgs e)
        {
            if (ViewModel.PlayerService.IsPlaying)
            {
                ViewModel.PlayerService.Stop();
            }
        }

        public TrackEditorViewModel ViewModel
        {
            get
            {
                return DataContext as TrackEditorViewModel;
            }
        }

        private void WaveItemsControl_MouseWheel(object sender, MouseWheelEventArgs e)
        {
            float ScaleFactor = ViewModel.Scale;
            float step = ScaleFactor < 3 ? ScaleFactor / 10 : 1;
            ScaleFactor = e.Delta > 0 ? ScaleFactor + step : ScaleFactor - step;
            ViewModel.Scale = ScaleFactor;
        }

        private void Window_Drop(object sender, DragEventArgs e)
        {
            if (ViewModel.SupportsDynamicTracks && e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                string[] files = (string[])e.Data.GetData(DataFormats.FileDrop);

                ViewModel.AddTracks(files.Where(f => f.ToLower().EndsWith(".mp3")).ToList());
            }
        }

        public void BlurBackground()
        {
            RootGrid.Effect = new BlurEffect() { Radius = 10 };
            DarkenBorder.Visibility = Visibility.Visible;
        }

        public void UnBlurBackground()
        {
            RootGrid.Effect = null;
            DarkenBorder.Visibility = Visibility.Collapsed;
        }
    }
}
