﻿using PlaylistManager.ViewModel.TrackEditor;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PlaylistManager.View.TrackEditor
{
    /// <summary>
    /// Interaction logic for TimeRulerView.xaml
    /// </summary>
    public partial class TimeRulerView : UserControl
    {
        public TimeRulerView()
        {
            InitializeComponent();
            this.Loaded += TimeRulerView_Loaded;
            this.SizeChanged += TimeRulerView_SizeChanged;
            this.MouseDown += TimeRulerView_MouseDown;
            Splits = new Stack<int>();
        }

        private TrackEditorViewModel ViewModel
        {
            get
            {
                return DataContext as TrackEditorViewModel;
            }
        }

        private void TimeRulerView_MouseDown(object sender, MouseButtonEventArgs e)
        {
            int positionInMilliSeconds = Convert.ToInt32(e.GetPosition(this).X * PixelLengthInMilliSeconds);
            ViewModel.SeekTo(positionInMilliSeconds);
        }

        public int PixelLengthInMilliSeconds
        {
            get { return (int)GetValue(PixelLengthInMilliSecondsProperty); }
            set { SetValue(PixelLengthInMilliSecondsProperty, value); }
        }

        public int MarkerSpacingInMilliSeconds
        {
            get { return (int)GetValue(MarkerSpacingInMilliSecondsProperty); }
            set { SetValue(MarkerSpacingInMilliSecondsProperty, value); }
        }

        public static readonly DependencyProperty PixelLengthInMilliSecondsProperty =
            DependencyProperty.Register("PixelLengthInMilliSeconds", typeof(int), typeof(TimeRulerView), new PropertyMetadata(100));

        public static readonly DependencyProperty MarkerSpacingInMilliSecondsProperty =
            DependencyProperty.Register("MarkerSpacingInMilliSeconds", typeof(int), typeof(TimeRulerView), new PropertyMetadata(5000));


        private void TimeRulerView_Loaded(object sender, RoutedEventArgs e)
        {
            Redraw();
        }

        private void TimeRulerView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Redraw();
        }

        Stack<int> Splits { get; set; }

        private void Redraw()
        {
            if(PixelLengthInMilliSeconds == 0)
            {
                return;
            }

            WriteableBitmap writeableBmp = BitmapFactory.New((int)ActualWidth, (int)ActualHeight);
            var markersPositions = new List<int>();
            using (writeableBmp.GetBitmapContext())
            {
                long markerSpacing = MarkerSpacingInMilliSeconds / PixelLengthInMilliSeconds;
                if(markerSpacing < 120)
                {
                    Splits.Push(MarkerSpacingInMilliSeconds);
                    markerSpacing *= 2;
                    MarkerSpacingInMilliSeconds *= 2;                    
                }
                else if(Splits.Any() && markerSpacing > 240)
                {
                    Splits.Pop();
                    markerSpacing /= 2;
                    MarkerSpacingInMilliSeconds /= 2;
                }
                int intervals = 4;
                for (int i = 0; i < ActualWidth; i++)
                {
                    if (i % markerSpacing == 0)
                    {
                        writeableBmp.DrawLine(i, (int)ActualHeight, i, (int)ActualHeight - 20, Colors.Gray);
                        markersPositions.Add(i);
                    }
                }
                
                for(int i = 0; i < markersPositions.Count-1; i++)
                {
                    float distance = markersPositions[i+1] - markersPositions[i];
                    int spacing = Convert.ToInt32(distance / (intervals+1));
                    for(int j = 0; j < intervals+1; j++)
                    {
                        writeableBmp.DrawLine(markersPositions[i] + spacing * j, (int)ActualHeight, markersPositions[i] + spacing * j, (int)ActualHeight - 5, Colors.Gray);
                    }
                }

            }

            var w = writeableBmp.PixelWidth;
            var h = writeableBmp.PixelHeight;
            var stride = writeableBmp.BackBufferStride;
            var pixelPtr = writeableBmp.BackBuffer;

            var bm2 = new Bitmap(w, h, stride, System.Drawing.Imaging.PixelFormat.Format32bppArgb, pixelPtr);

            writeableBmp.Lock();

            foreach (var markerPosition in markersPositions)
            {
                using (var g = Graphics.FromImage(bm2))
                {
                    var time = TimeSpan.FromMilliseconds(markerPosition * PixelLengthInMilliSeconds);
                    g.DrawString(time.ToString("mm':'ss'.'fff"), new Font("Tahoma", 8), System.Drawing.Brushes.White, markerPosition + 3, (int)ActualHeight - 22);
                }
            }

            writeableBmp.AddDirtyRect(new Int32Rect(0, 0, w,h));
            writeableBmp.Unlock();

            TimeRulerImg.Source = writeableBmp;
        }
    }
}
