﻿using PlaylistManager.GlobalEvent;
using PlaylistManager.Service;
using System;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Threading;

namespace PlaylistManager.View
{
    /// <summary>
    /// Interaction logic for LoadingView.xaml
    /// </summary>
    public partial class LoadingView : Window
    {
        public LoadingView()
        {
            InitializeComponent();
        }
    }
}
