﻿using PlaylistManager.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PlaylistManager.View
{
    /// <summary>
    /// Interaction logic for ImportPlaylistView.xaml
    /// </summary>
    public partial class ImportPlaylistView : Window, IFreezableWindow
    {
        public ImportPlaylistView()
        {
            InitializeComponent();
        }

        public void BlurBackground()
        {
            RootGrid.Effect = new BlurEffect() { Radius = 10 };
            DarkenBorder.Visibility = Visibility.Visible;
        }

        public void UnBlurBackground()
        {
            RootGrid.Effect = null;
            DarkenBorder.Visibility = Visibility.Collapsed;
        }
    }
}
