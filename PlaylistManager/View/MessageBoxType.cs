﻿namespace PlaylistManager.View
{
    public enum MessageBoxType
    {
        Ok,
        YesNo,
        YesNoCancel,
    }
}
