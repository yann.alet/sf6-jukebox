﻿using PlaylistManager.ViewModel.TrackEditor;
using System;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace PlaylistManager.View
{
    public class TrimmableGrid : Grid
    {
        private enum SnapMode
        {
            None,
            SnapIn,
            SnapOut
        }

        public TrimmableGrid()
        {
            Loaded += TrimmableGrid_Loaded;            
        }

        private void AddTrimmingControls()
        {
            LeftTrimmingBorder = new Border { Background = Brushes.Transparent, Width = 20, Cursor = Cursors.SizeWE, HorizontalAlignment = HorizontalAlignment.Left };
            RightTrimmingBorder = new Border { Background = Brushes.Transparent, Width = 20, Cursor = Cursors.SizeWE, HorizontalAlignment = HorizontalAlignment.Right };
            Children.Add(LeftTrimmingBorder);
            Children.Add(RightTrimmingBorder);
            LeftTrimmingBorder.MouseDown += LeftTrimmingBorder_MouseDown;
            RightTrimmingBorder.MouseDown += RightTrimmingBorder_MouseDown;
        }        

        private Border LeftTrimmingBorder { get; set; }
        private Border RightTrimmingBorder { get; set; }
        private ItemsControl ParentItemsControl { get; set; }
        private TrackWaveViewModel ViewModel { get; set; }
        private Image WaveImage { get; set; }
        private SnapMode CurrentSnapMode { get; set; }
        private bool _isMouseDown;
        private bool _isTrimmingLeft;
        private bool _isTrimmingRight;
        private bool _isPanning;
        private double _mouseDownPosition;
        private double _lastLeftTrimValue = 0;
        private double _lastPanningValue = 0;
        private double _innerOffset = 0;
        private double _mouseDownWidth = 0;
        private double _lastSnapDelta = 0;

        private void TrimmableGrid_Loaded(object sender, RoutedEventArgs e)
        {
            ParentItemsControl = FindParent<ItemsControl>(this);
            if (ParentItemsControl != null)
            {
                ViewModel = (TrackWaveViewModel)DataContext;
                if(!ViewModel.Track.IsValid)
                {
                    return;
                }

                ParentItemsControl.MouseMove += Parent_MouseMove;
            }

            MouseUp += TrimmableGrid_MouseUp;
            CurrentSnapMode = SnapMode.SnapIn;
            WaveImage = Children.OfType<Image>().Single();
            WaveImage.SizeChanged += WaveImage_SizeChanged;
            WaveImage.MouseDown += WaveImage_MouseDown;
            AddTrimmingControls();

            RefreshSizeAndMargin();
            _lastLeftTrimValue = WaveImage.Margin.Left;
            _lastPanningValue = Margin.Left;
        }

        private void RefreshSizeAndMargin()
        {
            if (ViewModel != null)
            {
                Margin = new Thickness(ViewModel.StartOffsetInMilliSeconds / ViewModel.PixelLengthInMilliSeconds, 0, 0, 0);
                double totalLenght = ViewModel.GetTrackLength().TotalMilliseconds;
                double currentLength = totalLenght - ViewModel.EndTrimmingInMilliSeconds - ViewModel.StartTrimmingInMilliSeconds;
                Width = currentLength / ViewModel.PixelLengthInMilliSeconds;
                WaveImage.Margin = new Thickness(-ViewModel.StartTrimmingInMilliSeconds / ViewModel.PixelLengthInMilliSeconds, 0, 0, 0);
            }
        }

        private void WaveImage_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            // we're scalling.
            RefreshSizeAndMargin();
        }

        private void Parent_MouseMove(object sender, MouseEventArgs e)
        {
            if (_isMouseDown)
            {
                var parentRelativePosition = e.GetPosition(ParentItemsControl);
                double X = parentRelativePosition.X;
                Thickness newMargin = new Thickness();
                if (_isPanning)
                {
                    double delta = X - _mouseDownPosition;
                    newMargin = new Thickness(_lastPanningValue + delta, 0, 0, 0);
                    newMargin.Left = Math.Max(0, newMargin.Left);
                    Margin = newMargin;
                }
                else if (_isTrimmingLeft)
                {
                    double delta = X - _mouseDownPosition;
                    if (_lastLeftTrimValue - delta <= 0)
                    {
                        newMargin = new Thickness(X - _innerOffset, 0, 0, 0);
                        WaveImage.Margin = new Thickness(_lastLeftTrimValue - delta, 0, 0, 0);
                        newMargin.Left = Math.Max(0, newMargin.Left);
                        Margin = newMargin;
                        Width = _mouseDownWidth - delta;
                    }
                    else
                    {
                        return;
                    }
                }
                else if(_isTrimmingRight)
                {
                    double delta = X - _mouseDownPosition;
                    if (_mouseDownWidth + delta <= (WaveImage.Width + WaveImage.Margin.Left))
                    {
                        if (CurrentSnapMode == SnapMode.SnapIn)
                        {
                            HandleSnapIn(delta);
                        }
                        else if (CurrentSnapMode == SnapMode.SnapOut)
                        {
                            HandleSnapOut(Convert.ToInt32(delta));
                        }
                    }
                    else
                    {
                        return;
                    }
                }                
            }
        }

        private void HandleSnapIn(double delta)
        {
            Width = _mouseDownWidth + delta;
            foreach (var snapPoint in ViewModel.SnapPoints)
            {
                double snappingLength = snapPoint.TotalMilliseconds;
                double lengthInMs = (Width + Margin.Left) * ViewModel.PixelLengthInMilliSeconds;
                double snappingThreshold = 1000;
                double deltaLength = lengthInMs - snappingLength;
                if (Math.Abs(deltaLength) < snappingThreshold)
                {
                    Native.User32.Api.GetCursorPos(out var pos);
                    double snapOffset = (deltaLength / ViewModel.PixelLengthInMilliSeconds);
                    Native.User32.Api.SetCursorPos(Convert.ToInt32(pos.X - snapOffset), pos.Y);
                    Width -= snapOffset;
                    _lastSnapDelta = delta;
                    CurrentSnapMode = SnapMode.SnapOut;
                }
            }
        }

        private void HandleSnapOut(double delta)
        {
            double lengthInMs = (delta - _lastSnapDelta) * ViewModel.PixelLengthInMilliSeconds;
            int snappingThreshold = 1100;
            if (Math.Abs(lengthInMs) > snappingThreshold)
            {
                CurrentSnapMode = SnapMode.SnapIn;
                Width = _mouseDownWidth + delta;
            }
        }

        private void TrimmableGrid_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if(!_isPanning && !_isTrimmingLeft && !_isTrimmingRight)
            {
                return;
            }

            ViewModel.StartOffsetInMilliSeconds = Convert.ToInt32(Margin.Left * ViewModel.PixelLengthInMilliSeconds);
            if(_isTrimmingLeft)
            {
                ViewModel.StartTrimmingInMilliSeconds = Convert.ToInt32(WaveImage.Margin.Left * -1 * ViewModel.PixelLengthInMilliSeconds);
            }

            if(_isTrimmingRight)
            {
                ViewModel.EndTrimmingInMilliSeconds = Convert.ToInt32((WaveImage.ActualWidth + WaveImage.Margin.Left - ActualWidth) * ViewModel.PixelLengthInMilliSeconds);
            }

            _isMouseDown = false;
            _isTrimmingLeft = false;
            _isTrimmingRight = false;
            _isPanning = false;            
            _lastLeftTrimValue = WaveImage.Margin.Left;
            _lastPanningValue = Margin.Left;
            if (CapturingControl != null)
            {
                CapturingControl.ReleaseMouseCapture();
            }
        }

        UIElement CapturingControl { get; set; }

        private void HandleMouseDown(object sender, MouseButtonEventArgs e)
        {
            _mouseDownWidth = ActualWidth;
            CapturingControl = (UIElement)sender;
            CapturingControl.CaptureMouse();
            _isMouseDown = true;
            _mouseDownPosition = e.GetPosition(ParentItemsControl).X;
            _innerOffset = e.GetPosition(this).X;
        }

        private void WaveImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _isPanning = true;
            HandleMouseDown(sender, e);
        }

        private void RightTrimmingBorder_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _isTrimmingRight = true;
            HandleMouseDown(sender, e);
        }

        private void LeftTrimmingBorder_MouseDown(object sender, MouseButtonEventArgs e)
        {
            _isTrimmingLeft = true;
            HandleMouseDown(sender, e);
        }

        public static T FindParent<T>(DependencyObject child) where T : DependencyObject
        {
            DependencyObject parentObject = VisualTreeHelper.GetParent(child);
            if (parentObject == null)
            {
                return null;
            }

            T parent = parentObject as T;
            if (parent != null)
            {
                return parent;
            }
            else
            {
                return FindParent<T>(parentObject);
            }
        }
    }
}

