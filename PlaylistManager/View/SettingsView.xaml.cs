﻿using PlaylistManager.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace PlaylistManager.View
{
    /// <summary>
    /// Interaction logic for Settings.xaml
    /// </summary>
    public partial class SettingsView : UserControl
    {
        public SettingsView()
        {
            InitializeComponent();
        }

        private void OnNextTrackShortcutKeyDown(object sender, KeyEventArgs e)
        {
            var vm = DataContext as SettingsViewModel;
            if (vm != null)
            {
                vm.SetNextTrackKey(e.Key);
                var toggleButton = (ToggleButton)sender;
                toggleButton.IsChecked = false;
            }

            e.Handled = true;
        }

        private void OnPreviousTrackShortcutKeyDown(object sender, KeyEventArgs e)
        {
            var vm = DataContext as SettingsViewModel;
            if (vm != null)
            {
                vm.SetPreviousTrackKey(e.Key);
                var toggleButton = (ToggleButton)sender;
                toggleButton.IsChecked = false;
            }

            e.Handled = true;
        }

        private void OnVolumeUpShortcutKeyDown(object sender, KeyEventArgs e)
        {
            var vm = DataContext as SettingsViewModel;
            if (vm != null)
            {
                vm.SetVolumeUpKey(e.Key);
                var toggleButton = (ToggleButton)sender;
                toggleButton.IsChecked = false;
            }

            e.Handled = true;
        }

        private void OnVolumeDownShortcutKeyDown(object sender, KeyEventArgs e)
        {
            var vm = DataContext as SettingsViewModel;
            if (vm != null)
            {
                vm.SetVolumeDownKey(e.Key);
                var toggleButton = (ToggleButton)sender;
                toggleButton.IsChecked = false;
            }

            e.Handled = true;
        }

        private void OnShowOverlayShortcutKeyDown(object sender, KeyEventArgs e)
        {
            var vm = DataContext as SettingsViewModel;
            if (vm != null)
            {
                vm.SetShowOverlayKey(e.Key);
                var toggleButton = (ToggleButton)sender;
                toggleButton.IsChecked = false;
            }

            e.Handled = true;
        }

        private void OnRestartShortcutKeyDown(object sender, KeyEventArgs e)
        {
            var vm = DataContext as SettingsViewModel;
            if (vm != null)
            {
                vm.SetRestartKey(e.Key);
                var toggleButton = (ToggleButton)sender;
                toggleButton.IsChecked = false;
            }

            e.Handled = true;
        }
    }
}
