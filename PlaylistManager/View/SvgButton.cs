﻿using SharpVectors.Converters;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace PlaylistManager.View
{
    public class SvgButton : Button
    {
        private SvgCanvas _defaultSvgCanvas;

        public SvgButton()
        {
            _defaultSvgCanvas = new SvgCanvas();
            _defaultSvgCanvas.HorizontalAlignment = HorizontalAlignment.Center;
            _defaultSvgCanvas.VerticalAlignment = VerticalAlignment.Center;
            Loaded += SvgButton_Loaded;
            Background = System.Windows.Media.Brushes.Transparent;
            BorderBrush = System.Windows.Media.Brushes.Transparent;
            Content = _defaultSvgCanvas;            
            Template = CreateControlTemplate();
            MouseEnter += (s, e) => UpdateSvg();
            MouseLeave += (s, e) => UpdateSvg();
        }

        private ControlTemplate CreateControlTemplate()
        {
            var border = new FrameworkElementFactory(typeof(Border));
            border.SetBinding(Border.BackgroundProperty, new Binding("Background") { Source = this });
            border.SetBinding(Border.BorderBrushProperty, new Binding("BorderBrush") { Source = this });
            border.SetBinding(Border.BorderThicknessProperty, new Binding("BorderThickness") { Source = this });

            var contentPresenter = new FrameworkElementFactory(typeof(ContentPresenter));
            contentPresenter.SetValue(ContentPresenter.HorizontalAlignmentProperty, HorizontalAlignment.Center);
            contentPresenter.SetValue(ContentPresenter.VerticalAlignmentProperty, VerticalAlignment.Center);

            border.AppendChild(contentPresenter);

            var controlTemplate = new ControlTemplate(typeof(Button));
            controlTemplate.Triggers.Add(new Trigger { Property = IsMouseOverProperty, Value = true, Setters = { new Setter { Property = BackgroundProperty, Value = System.Windows.Media.Brushes.LightGray } } });
            controlTemplate.VisualTree = border;

            return controlTemplate;
        }

        private void SvgButton_Loaded(object sender, RoutedEventArgs e)
        {
            UpdateSvg();
        }

        protected void UpdateSvg()
        {
            if (MouseOverSvg != null)
            {
                _defaultSvgCanvas.Source = IsMouseOver ? MouseOverSvg : DefaultSvg;
            }
            else
            {
                _defaultSvgCanvas.Source = DefaultSvg;
            }
        }

        protected void UpdateSvgScale()
        {
            _defaultSvgCanvas.RenderTransform = new System.Windows.Media.ScaleTransform(SvgScale, SvgScale);
            _defaultSvgCanvas.RenderTransformOrigin = new Point(0.5, 0.5);
        }

        public Uri DefaultSvg
        {
            get { return (Uri)GetValue(DefaultSvgProperty); }
            set { SetValue(DefaultSvgProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DefaultSvg.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DefaultSvgProperty =
            DependencyProperty.Register("DefaultSvg", typeof(Uri), typeof(SvgButton), new PropertyMetadata(null, OnDefaultSvgChanged));

        private static void OnDefaultSvgChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var svgButton = d as SvgButton;
            if (svgButton != null)
            {
                svgButton.UpdateSvg();
            }
        }

        public Uri MouseOverSvg
        {
            get { return (Uri)GetValue(MouseOverSvgProperty); }
            set { SetValue(MouseOverSvgProperty, value); }
        }

        // Using a DependencyProperty as the backing store for MouseOverSvg.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty MouseOverSvgProperty =
            DependencyProperty.Register("MouseOverSvg", typeof(Uri), typeof(SvgButton), new PropertyMetadata(null));



        public float SvgScale
        {
            get { return (float)GetValue(svgScaleProperty); }
            set { SetValue(svgScaleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for svgScale.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty svgScaleProperty =
            DependencyProperty.Register("SvgScale", typeof(float), typeof(SvgButton), new PropertyMetadata(1.0f, OnSvgScaleChanged));

        private static void OnSvgScaleChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var svgButton = d as SvgButton;
            if (svgButton != null)
            {
                svgButton.UpdateSvgScale();
            }
        }
    }
}
