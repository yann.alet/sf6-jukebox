﻿using PlaylistManager.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace PlaylistManager.View
{
    /// <summary>
    /// Interaction logic for MessageBoxView.xaml
    /// </summary>
    public partial class MessageBoxView : Window
    {
        public MessageBoxView()
        {
            InitializeComponent();
            this.KeyUp += MessageBoxView_KeyUp;
            this.Closed += MessageBoxView_Closed;
            this.Closing += MessageBoxView_Closing;
            this.Owner = Application.Current.MainWindow;
            this.Loaded += MessageBoxView_Loaded;
        }

        private void MessageBoxView_Loaded(object sender, RoutedEventArgs e)
        {
            var vm = DataContext as MessageBoxViewModel;
            if(vm != null)
            {
                vm.Owner = this;
            }
        }

        private void MessageBoxView_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Application.Current.MainWindow.BringIntoView();
        }

        private void MessageBoxView_Closed(object sender, EventArgs e)
        {
            Application.Current.MainWindow.BringIntoView();
        }

        private void MessageBoxView_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Escape || e.Key == Key.Enter) 
            {
                Close();
            }
        }
    }
}
