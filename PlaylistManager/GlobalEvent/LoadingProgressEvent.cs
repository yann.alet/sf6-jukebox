﻿namespace PlaylistManager.GlobalEvent
{
    public class LoadingProgressEvent
    {
        public LoadingProgressEvent(float progress, string taskTitle, string taskDescription)
        {
            Progress = progress;
            TaskTitle = taskTitle;
            TaskDescription = taskDescription;
        }

        public float Progress { get; }
        public string TaskTitle { get; }
        public string TaskDescription { get; }
    }
}
