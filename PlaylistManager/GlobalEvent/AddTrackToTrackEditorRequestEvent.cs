﻿namespace PlaylistManager.GlobalEvent
{
    public class AddTrackToTrackEditorRequestEvent
    {
        public AddTrackToTrackEditorRequestEvent(int playerId, float[] waveData)
        {
            PlayerId = playerId;
            WaveData = waveData;
        }

        public int PlayerId { get; }
        public float[] WaveData { get; }
    }
}
