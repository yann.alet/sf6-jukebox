﻿using PlaylistManager.View;
using System;
using System.Windows;

namespace PlaylistManager.GlobalEvent
{
    public class MessageBoxEvent
    {
        public MessageBoxEvent(string message, string title, MessageBoxType type = MessageBoxType.Ok)
        {
            Message = message;
            Title = title;
            Type = type;
        }

        public Action OnOk { get; set; }
        public Action OnYes { get; set; }
        public Action OnNo { get; set; }
        public Action OnCancel { get; set; }
        public Window Owner { get; set; }

        public string Message { get; }
        public string Title { get; }
        public MessageBoxType Type { get; }
    }
}
