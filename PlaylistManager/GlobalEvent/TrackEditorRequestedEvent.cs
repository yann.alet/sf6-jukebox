﻿using PlaylistManager.ViewModel;

namespace PlaylistManager.GlobalEvent
{
    public class TrackEditorRequestedEvent
    {
        public TrackEditorRequestedEvent(TrackViewModel trackViewModel)
        {
            Track = trackViewModel;
        }

        public TrackViewModel Track { get; set; }
    }
}
