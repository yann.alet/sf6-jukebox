﻿using MediaPlayerDAL.Contract;
using MediaPlayerDAL.Model;
using Microsoft.Win32;
using PlaylistManager.GlobalEvent;
using PlaylistManager.Service;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Threading;

namespace PlaylistManager.ViewModel.Export
{
    public class ExportPlaylistViewModel : BaseViewModel
    {
        public ExportPlaylistViewModel(Settings settings, MessageBoxService messageBoxService, LoadingScreenService loadingScreenService)
        {
            ExportCommand = new RelayCommand(OnExport, CanExport);
            PlaylistContainers = new ObservableCollection<PlaylistContainerViewModel>();
            if (settings.FlowScenes.Any(f => f.Playlist.Tracks.Any()))
            {
                PlaylistContainers.Add(new PlaylistContainerViewModel("Menus", settings.FlowScenes.Where(fs=>fs.Playlist.Tracks.Any()).Cast<IScreen>().ToList()));
            }

            if (settings.Stages.Any(f => f.Playlist.Tracks.Any()))
            {
                PlaylistContainers.Add(new PlaylistContainerViewModel("Stages", settings.Stages.Where(fs => fs.Playlist.Tracks.Any()).Cast<IScreen>().ToList()));
            }

            if (settings.Characters.Any(f => f.Playlist.Tracks.Any()))
            {
                PlaylistContainers.Add(new PlaylistContainerViewModel("Characters", settings.Characters.Where(fs => fs.Playlist.Tracks.Any()).Cast<IScreen>().ToList()));
            }

            if (settings.CustomScreens.Any(f => f.Playlist.Tracks.Any()))
            {
                PlaylistContainers.Add(new PlaylistContainerViewModel("Misc", settings.CustomScreens.Where(fs => fs.Playlist.Tracks.Any()).Cast<IScreen>().ToList()));
            }

            Settings = settings;
            MessageBoxService = messageBoxService;
            LoadingScreenService = loadingScreenService;
        }

        private bool CanExport(object obj)
        {
            bool canExport = false;
            foreach(var playlistContainer in PlaylistContainers)
            {
                canExport |= playlistContainer.Playlists.Any(p => p.IsSelected);
            }

            return canExport;
        }

        private void OnExport(object obj)
        {
            var export = new MediaPlayerDAL.Model.Export();
            foreach (var container in PlaylistContainers)
            {
                if(container.Playlists.Any(p=>p.IsSelected))
                {
                    if(container.Name == "Menus")
                    {
                        var fs = Settings.FlowScenes.Where(f => container.Playlists.Where(p => p.IsSelected).Select(p => p.Name).Contains(f.Name));
                        export.FlowScenes = fs.ToList();
                    }

                    if (container.Name == "Stages")
                    {
                        var stages = Settings.Stages.Where(f => container.Playlists.Where(p => p.IsSelected).Select(p => p.Name).Contains(f.Name));
                        export.Stages = stages.ToList();
                    }

                    if (container.Name == "Characters")
                    {
                        var characters = Settings.Characters.Where(f => container.Playlists.Where(p => p.IsSelected).Select(p => p.Name).Contains(f.Name));
                        export.Characters = characters.ToList();
                    }

                    if (container.Name == "Misc")
                    {
                        var customScreens = Settings.CustomScreens.Where(f => container.Playlists.Where(p => p.IsSelected).Select(p => p.Name).Contains(f.Name));
                        export.CustomScreens = customScreens.ToList();
                    }
                }
            }

            if( export.Stages.Any()
                || export.FlowScenes.Any()
                || export.CustomScreens.Any()
                || export.Characters.Any())
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "Jukebox Playlist (*.jpl)|*.jpl";
                if (saveFileDialog.ShowDialog() == true)
                {
                    ImportExportService importExportService = new ImportExportService(Settings, MessageBoxService, LoadingScreenService);
                    LoadingScreenService.Show("Exporting...", () =>
                    {
                        importExportService.Export(export, saveFileDialog.FileName, (file, progress) =>
                        {
                            EventAggregator.Publish(new LoadingProgressEvent(progress, "Exporting...", Path.GetFileName(file)));
                        });
                    });
                }
            }            
        }

        public ObservableCollection<PlaylistContainerViewModel> PlaylistContainers { get; set; }
        public RelayCommand ExportCommand { get; set; }
        public Settings Settings { get; }
        public MessageBoxService MessageBoxService { get; }
        public LoadingScreenService LoadingScreenService { get; }
    }
}
