﻿using MediaPlayerDAL.Contract;
using MediaPlayerDAL.Model;
using Microsoft.Win32;
using PlaylistManager.GlobalEvent;
using PlaylistManager.Service;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace PlaylistManager.ViewModel.Export
{
    public class ImportPlaylistViewModel : BaseViewModel
    {
        private ImportExportService ImportExportService { get;set; }
        private MediaPlayerDAL.Model.Export Export { get; set; }
        public bool IsValid { get; private set; }

        public ImportPlaylistViewModel(string playlistFilePath, Settings settings, MessageBoxService messageBoxService, LoadingScreenService loadingScreenService)
        {
            ImportExportService = new ImportExportService(settings, messageBoxService, loadingScreenService);
            Export = ImportExportService.Extract(playlistFilePath);
            ImportCommand = new RelayCommand(OnImport, CanImport);
            PlaylistContainers = new ObservableCollection<PlaylistContainerViewModel>();
            PlaylistFilePath = playlistFilePath;
            MessageBoxService = messageBoxService;
            LoadingScreenService = loadingScreenService;
            if (Export != null)
            {

                if (Export.FlowScenes.Any(f => f.Playlist.Tracks.Any()))
                {
                    PlaylistContainers.Add(new PlaylistContainerViewModel("Menus", Export.FlowScenes.Where(fs => fs.Playlist.Tracks.Any()).Cast<IScreen>().ToList()));
                }

                if (Export.Stages.Any(f => f.Playlist.Tracks.Any()))
                {
                    PlaylistContainers.Add(new PlaylistContainerViewModel("Stages", Export.Stages.Where(fs => fs.Playlist.Tracks.Any()).Cast<IScreen>().ToList()));
                }

                if (Export.Characters.Any(f => f.Playlist.Tracks.Any()))
                {
                    PlaylistContainers.Add(new PlaylistContainerViewModel("Characters", Export.Characters.Where(fs => fs.Playlist.Tracks.Any()).Cast<IScreen>().ToList()));
                }

                if (Export.CustomScreens.Any(f => f.Playlist.Tracks.Any()))
                {
                    PlaylistContainers.Add(new PlaylistContainerViewModel("Misc", Export.CustomScreens.Where(fs => fs.Playlist.Tracks.Any()).Cast<IScreen>().ToList()));
                }
                
                IsValid = true;
            }
            else
            {
                IsValid = false;
            }
        }

        private bool CanImport(object obj)
        {
            bool canExport = false;
            foreach (var playlistContainer in PlaylistContainers)
            {
                canExport |= playlistContainer.Playlists.Any(p => p.IsSelected);
            }

            return canExport;
        }

        private void OnImport(object obj)
        {
            var export = new MediaPlayerDAL.Model.Export();
            foreach (var container in PlaylistContainers)
            {
                if(container.Playlists.Any(p=>p.IsSelected))
                {
                    if(container.Name == "Menus")
                    {
                        var fs = Export.FlowScenes.Where(f => container.Playlists.Where(p => p.IsSelected).Select(p => p.Name).Contains(f.Name));
                        export.FlowScenes = fs.ToList();
                    }

                    if (container.Name == "Stages")
                    {
                        var stages = Export.Stages.Where(f => container.Playlists.Where(p => p.IsSelected).Select(p => p.Name).Contains(f.Name));
                        export.Stages = stages.ToList();
                    }

                    if (container.Name == "Characters")
                    {
                        var characters = Export.Characters.Where(f => container.Playlists.Where(p => p.IsSelected).Select(p => p.Name).Contains(f.Name));
                        export.Characters = characters.ToList();
                    }

                    if (container.Name == "Misc")
                    {
                        var customScreens = Export.CustomScreens.Where(f => container.Playlists.Where(p => p.IsSelected).Select(p => p.Name).Contains(f.Name));
                        export.CustomScreens = customScreens.ToList();
                    }
                }
            }

            if( export.Stages.Any()
                || export.FlowScenes.Any()
                || export.CustomScreens.Any()
                || export.Characters.Any())
            {
                LoadingScreenService.Show("Importing...", () =>
                {
                    ImportExportService.Import(export, (file, progress) =>
                    {
                        EventAggregator.Publish(new LoadingProgressEvent(progress, "Importing...", Path.GetFileName(file)));
                    });
                });
            }            
        }

        public ObservableCollection<PlaylistContainerViewModel> PlaylistContainers { get; set; }
        public RelayCommand ImportCommand { get; set; }
        public string PlaylistFilePath { get; }
        public MessageBoxService MessageBoxService { get; }
        public LoadingScreenService LoadingScreenService { get; }
    }
}
