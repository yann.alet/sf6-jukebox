﻿using MediaPlayerDAL.Contract;

namespace PlaylistManager.ViewModel.Export
{
    public class PlaylistViewModel : BaseViewModel
    {
        private bool isSelected;

        public PlaylistViewModel(IScreen screen)
        {
            Name = screen.Name;
        }

        public string Name { get; set; }
        public bool IsSelected 
        { 
            get => isSelected;
            set
            { 
                isSelected = value;
                OnPropertyChanged();
            }
        }
    }
}
