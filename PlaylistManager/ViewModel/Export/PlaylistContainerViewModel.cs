﻿using MediaPlayerDAL.Contract;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace PlaylistManager.ViewModel.Export
{
    public class PlaylistContainerViewModel : BaseViewModel
    {
        private bool _isSelected;

        public PlaylistContainerViewModel(string name, IList<IScreen> screens)
        {
            Name = name;
            Playlists = new ObservableCollection<PlaylistViewModel>();
            foreach(IScreen screen in screens)
            {
                var playlist = new PlaylistViewModel(screen);
                Playlists.Add(playlist);
            }
        }

        public ObservableCollection<PlaylistViewModel> Playlists { get; set; }
        public string Name { get; set; }
        public bool IsSelected 
        { 
            get => _isSelected;
            set
            {
                _isSelected = value;
                foreach (var playlist in Playlists)
                {
                    playlist.IsSelected = _isSelected;
                }
            }
        }
    }
}
