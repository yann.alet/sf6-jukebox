﻿using MediaPlayerDAL.Model;
using PlaylistManager.Service;
using System;

namespace PlaylistManager.ViewModel
{
    public class FlowSceneViewModel : BaseScreenViewModel<FlowScene>
    {
        public FlowSceneViewModel(FlowScene flowScene, PlayerService playerService, LoadingScreenService loadingScreenService)
            : base(flowScene, playerService, loadingScreenService)
        {
                  
        }

        public override string ThumbnailPath => String.Format("/PlaylistManager;component/Images/Menus/{0}.png", Model.FlowSceneId);

        public override bool SupportsDynamicTracks => false;
    }
}
