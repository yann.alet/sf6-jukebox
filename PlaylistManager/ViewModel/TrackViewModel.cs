﻿using MediaPlayerDAL.Model;
using PlaylistManager.GlobalEvent;
using PlaylistManager.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;

namespace PlaylistManager.ViewModel
{
    public class TrackViewModel : BaseViewModel
    {
        public TrackViewModel(Track track, PlayerService playerService, LoadingScreenService loadingScreenService, bool supportsDynamicTracks)
        {
            Track = track;
            FullPath = Track.Path;
            IsPathValid = File.Exists(FullPath);

            if (!IsPathValid)
            {
                IsInvalidDescription = "File not found";
            }
            else
            {
                IsCorrectSampleRate = playerService.HasValidSampleRate(FullPath);
            }

            PlayerService = playerService;
            LoadingScreenService = loadingScreenService;
            PlayerService.OnRead += PlayerService_OnRead;
            PlayerService.OnPlay += PlayerService_OnPlay;
            PlayerService.OnTransition += PlayerService_OnTransition;
            PlayerService.OnStop += PlayerService_OnStop;
            InitializeTransitions();
            PlaybackSettings = new PlaybackSettingsViewModel(Track.PlaybackSettings);
            PlayCommand = new RelayCommand(x =>
            {
                PlayerId = PlayerService.Play(FullPath, new PlaybackSettings(1));
                IsPlaying = true;
            }, y => IsPathValid && !IsPlaying);

            StopCommand = new RelayCommand(x =>
            {
                PlayerService.Stop();
            }, y => IsPlaying);

            ShowInExplorerCommand = new RelayCommand(x =>
            {
                string argument = "/select, \"" + FullPath + "\"";
                Process.Start("explorer.exe", argument);
            }, y => File.Exists(FullPath));

            OpenEditorCommand = new RelayCommand(x =>
            {
                EventAggregator.Publish(new TrackEditorRequestedEvent(this));
            });

            FixSampleRateCommand = new RelayCommand(OnFixSampleRate, CanFixSampleRate);
            TrackBackup = Track.Clone();
            SupportsDynamicTracks = supportsDynamicTracks;
        }

        public bool SupportsDynamicTracks { get; set; }

        public bool IsDirty 
        {
            get
            {
                return isDirty || Transitions.Any(t => t.IsDirty) || PlaybackSettings.IsDirty;
            }

            private set
            {
                if(value == false)
                {
                    ClearIsDirty();   
                }
                isDirty = value;
            }
        }

        public void ClearIsDirty()
        {
            foreach (var t in Transitions)
            {
                t.ClearDirty();
            }

            PlaybackSettings.IsDirty = false;
            isDirty = false;
        }

        private void InitializeTransitions()
        {
            if (Transitions == null)
            {
                Transitions = new ObservableCollection<TransitionViewModel>();
            }
            else
            {
                Transitions.Clear();
            }

            if (!Track.Transitions.Any())
            {
                Track.Transitions.Add(new Transition() { TransitionType = MediaPlayerDAL.TransitionType.Round1 });
                Track.Transitions.Add(new Transition() { TransitionType = MediaPlayerDAL.TransitionType.Round2 });
                Track.Transitions.Add(new Transition() { TransitionType = MediaPlayerDAL.TransitionType.Round3 });
                Track.Transitions.Add(new Transition() { TransitionType = MediaPlayerDAL.TransitionType.Danger });
            }

            foreach (var transition in Track.Transitions)
            {
                Transitions.Add(new TransitionViewModel(this, transition, PlayerService, LoadingScreenService));                
            }

            ValidateTransitions();
            Transitions.CollectionChanged -= Transitions_CollectionChanged;
            Transitions.CollectionChanged += Transitions_CollectionChanged;
        }

        private void ValidateTransitions()
        {
            HasValidTransitions = Transitions.All(t => t.Tracks.All(t2 => t2.IsValid));
            if (!HasValidTransitions)
            {
                IsInvalidDescription = "Invalid transitions";
            }
        }

        private void Transitions_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            IsDirty = true;            
        }

        public void RemoveTransitionTrack(TransitionViewModel transition, TrackViewModel track)
        {
            var trans = Transitions.FirstOrDefault(t => t == transition);
            if (trans != null)
            {
                trans.RemoveTrack(track);
                IsDirty = true;
            }

            ValidateTransitions();
            OnPropertyChanged(nameof(IsValid));
            EventAggregator.Publish(new TrackValidityUpdatedEvent(this));
        }

        public void Revert()
        {
            Track = TrackBackup;
            TrackBackup = Track.Clone();
            PlaybackSettings.Revert();
            InitializeTransitions();
            IsDirty = false;
        }

        public bool IsActive { get; set; }

        public int GetTracksCount()
        {
            int i = 1;
            foreach (var transition in Transitions)
            {
                i += transition.Tracks.Count;
            }

            return i;
        }

        private void OnFixSampleRate(object param)
        {
            LoadingScreenService.Show("Resampling audio files", () =>
            {
                PlayerService.Resampler.Resample(new List<string> { FullPath }, (file, progress) =>
                {
                    EventAggregator.Publish(new LoadingProgressEvent(progress, "Resampling audio files", Path.GetFileName(file)));
                });
                Application.Current.Dispatcher.BeginInvoke(new Action(() =>
                {
                    IsCorrectSampleRate = PlayerService.HasValidSampleRate(FullPath);
                    OnPropertyChanged(nameof(IsValid));
                    EventAggregator.Publish(new TrackValidityUpdatedEvent(this));
                }));
            });
        }

        private bool CanFixSampleRate(object param)
        {
            return File.Exists(FullPath) && !IsCorrectSampleRate;
        }

        private void PlayerService_OnRead(object sender, PlayerService.OnReadInfo e)
        {
            if (e.PlayerIndex == PlayerId)
            {
                Progress = (Convert.ToSingle(e.Position) / e.Length) * 100;
            }
        }

        private void PlayerService_OnStop(object sender, int trackId)
        {
            if (PlayerId == trackId)
            {
                Progress = 0;
                IsPlaying = false;
                PlayerId = -1;
            }
        }

        private void PlayerService_OnTransition(object sender, int trackId)
        {
            if (PlayerId == trackId)
            {
                Progress = 0;
                IsPlaying = false;
                IsActive = false;
                PlayerId = -1;
            }
        }

        private int? PlayerId { get; set; }
        private float _progress;
        public string IsInvalidDescription 
        { 
            get => _isInvalidDescription;
            private set
            {
                _isInvalidDescription = value;
                OnPropertyChanged();
            }
        }

        public bool IsDynamicTrack 
        { 
            get
            {
                return Transitions.Any(t=>t.Tracks.Any());
            }
        }

        public float Progress
        {
            get { return _progress; }
            set
            {
                _progress = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(LowPrecisionProgress));
            }
        }

        public string LowPrecisionProgress
        {
            get
            {
                return String.Format("{0:0.00}", Progress);
            }
        }

        public bool IsValid
        {
            get
            {
                return IsPathValid && IsCorrectSampleRate;
            }
        }

        public bool IsPathValid
        {
            get;
        }

        public bool HasValidTransitions
        {
            get => hasValidTransitions; 
            private set
            {
                hasValidTransitions = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(HasErrors));
            }
        }

        public bool HasErrors
        {
            get => !IsValid || !HasValidTransitions;
        }

        public bool IsCorrectSampleRate
        {
            get => _isCorrectSampleRate;
            private set
            {
                _isCorrectSampleRate = value;
                IsInvalidDescription = _isCorrectSampleRate ? String.Empty : "Incorrect sample rate";
                OnPropertyChanged();
            }
        }

        public ObservableCollection<TransitionViewModel> Transitions { get; set; }
        public TransitionViewModel SelectedTransition
        {
            get => selectedTransition;
            set
            {
                selectedTransition = value;
                OnPropertyChanged();
            }
        }

        private void PlayerService_OnPlay(object sender, int trackId)
        {
            if (IsPlaying && PlayerId != trackId)
            {
                IsPlaying = false;
            }
        }

        public Track GetModel()
        {
            Track.Path = FullPath;
            Track.Transitions.Clear();
            foreach (var transition in Transitions)
            {
                Track.Transitions.Add(transition.ToModel());
            }
            return Track;
        }

        private Track Track { get; set; }
        private Track TrackBackup { get; set; }
        PlayerService PlayerService { get; set; }
        public LoadingScreenService LoadingScreenService { get; }

        private bool isSelected;
        private bool isPlaying;
        private TransitionViewModel selectedTransition;
        private bool isDirty;
        private bool hasValidTransitions;
        private string _isInvalidDescription;
        private bool _isCorrectSampleRate;

        public int Id { get; set; }
        public string FileName => String.IsNullOrEmpty(FullPath) ? String.Empty : Path.GetFileName(FullPath);
        public string FullPath { get; set; }
        public RelayCommand PlayCommand { get; set; }
        public RelayCommand StopCommand { get; set; }
        public RelayCommand TransitionCommand { get; set; }
        public RelayCommand ShowInExplorerCommand { get; set; }
        public RelayCommand FixSampleRateCommand { get; set; }
        public RelayCommand OpenEditorCommand { get; set; }

        public PlaybackSettingsViewModel PlaybackSettings { get; }

        public bool IsPlaying 
        { 
            get => isPlaying; 
            set
            {
                isPlaying = value;
                OnPropertyChanged();
            }
        }

        public bool IsSelected 
        {
            get => isSelected; 
            set
            {
                isSelected = value;
                OnPropertyChanged();
            }
        }
    }
}
