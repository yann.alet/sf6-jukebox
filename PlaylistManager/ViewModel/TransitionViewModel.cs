﻿using MediaPlayerDAL.Model;
using PlaylistManager.Service;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace PlaylistManager.ViewModel
{
    public class TransitionViewModel : BaseViewModel
    {
        private bool isDirty;

        public TransitionViewModel(TrackViewModel parentTrack, Transition transition, PlayerService playerService, LoadingScreenService loadingScreenService)
        {
            ParentTrack = parentTrack;
            Transition = transition;
            PlayerService = playerService;
            LoadingScreenService = loadingScreenService;
            Tracks = new ObservableCollection<TrackViewModel>();
            foreach (var track in transition.Tracks)
            {
                AddTrack(new TrackViewModel(track, playerService, LoadingScreenService, false));
            }
            Tracks.CollectionChanged += Tracks_CollectionChanged;
        }

        public void AddTrack(TrackViewModel track)
        {
            Tracks.Add(track);
            ParentTrack.ForcePropertyReevaluation("IsDynamicTrack");
        }

        public void RemoveTrack(TrackViewModel track)
        {
            Tracks.Remove(track);
            ParentTrack.ForcePropertyReevaluation("IsDynamicTrack");
        }

        private void Tracks_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            IsDirty = true;
        }

        public void ClearDirty()
        {
            foreach (var track in Tracks)
            {
                track.ClearIsDirty();
            }
            isDirty = false;
        }

        public TrackViewModel ParentTrack { get; }
        public Transition Transition { get; }
        private PlayerService PlayerService { get; }
        public LoadingScreenService LoadingScreenService { get; }
        public bool IsDirty 
        {
            get
            {
                return isDirty || Tracks.Any(t=>t.IsDirty);
            }

            private set => isDirty = value; 
        }

        public string Name => $"Transition for {Transition.TransitionType}";

        public string ShortName => $"{Transition.TransitionType}";

        public ObservableCollection<TrackViewModel> Tracks { get; set; }

        internal virtual void AddTracksToPlaylist(List<string> tracks)
        {
            foreach (string track in tracks)
            {
                Tracks.Add(new TrackViewModel(new Track() { Path = track }, PlayerService, LoadingScreenService, false));
            }

            IsDirty = true;
        }

        public Transition ToModel()
        {
            Transition.Tracks = new List<Track>();
            foreach (var track in Tracks) 
            {
                Transition.Tracks.Add(new Track() { Path = track.FullPath, PlaybackSettings = track.PlaybackSettings.PlaybackSettingsModel });
            }

            return Transition;
        }
    }
}
