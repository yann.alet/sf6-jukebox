﻿using MediaPlayerDAL.Model;
using System.Windows.Input;

namespace PlaylistManager.ViewModel
{
    public class KeyBindingsViewModel : BaseViewModel
    {
        private Key nextTrack;
        private Key previousTrack;
        private Key volumeUp;
        private Key volumeDown;
        private Key restart;
        private Key showOverlay;

        public Key NextTrack 
        {
            get { return nextTrack; }
            set
            {
                nextTrack = value;
                OnPropertyChanged();
            }
        }

        public Key PreviousTrack
        {
            get { return previousTrack; }
            set
            {
                previousTrack = value;
                OnPropertyChanged();
            }
        }

        public Key VolumeUp
        {
            get { return volumeUp; }
            set
            {
                volumeUp = value;
                OnPropertyChanged();
            }
        }

        public Key VolumeDown
        {
            get { return volumeDown; }
            set
            {
                volumeDown = value;
                OnPropertyChanged();
            }
        }

        public Key Restart
        {
            get { return restart; }
            set
            {
                restart = value;
                OnPropertyChanged();
            }
        }

        public Key ShowOverlay
        {
            get { return showOverlay; }
            set
            {
                showOverlay = value;
                OnPropertyChanged();
            }
        }

        public KeyBindings GetModel()
        {
            KeyBindings bindings = new KeyBindings();
            bindings.NextTrack = KeyInterop.VirtualKeyFromKey(NextTrack);
            bindings.PreviousTrack = KeyInterop.VirtualKeyFromKey(PreviousTrack);
            bindings.VolumeUp = KeyInterop.VirtualKeyFromKey(VolumeUp);
            bindings.VolumeDown = KeyInterop.VirtualKeyFromKey(VolumeDown);
            bindings.Restart = KeyInterop.VirtualKeyFromKey(Restart);
            bindings.ShowOverlay = KeyInterop.VirtualKeyFromKey(ShowOverlay);
            return bindings;
        }

        public static KeyBindingsViewModel FromModel(KeyBindings bindings)
        {
            KeyBindingsViewModel vm = new KeyBindingsViewModel();
            vm.NextTrack = KeyInterop.KeyFromVirtualKey(bindings.NextTrack);
            vm.PreviousTrack = KeyInterop.KeyFromVirtualKey(bindings.PreviousTrack);
            vm.VolumeUp = KeyInterop.KeyFromVirtualKey(bindings.VolumeUp);
            vm.VolumeDown = KeyInterop.KeyFromVirtualKey(bindings.VolumeDown);
            vm.Restart = KeyInterop.KeyFromVirtualKey(bindings.Restart);
            vm.ShowOverlay = KeyInterop.KeyFromVirtualKey(bindings.ShowOverlay);
            return vm;
        }
    }
}
