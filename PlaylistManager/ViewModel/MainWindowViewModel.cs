﻿using MediaPlayerDAL;
using MediaPlayerDAL.Model;
using MediaPlayerDAL.Service;
using MediaPlayerLib;
using MediaPlayerLib.NAudio;
using MediaPlayerLib.Network.Contract;
using Microsoft.Win32;
using PlaylistManager.Contract;
using PlaylistManager.GlobalEvent;
using PlaylistManager.Service;
using PlaylistManager.View;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Windows;

namespace PlaylistManager.ViewModel
{
    internal class MainWindowViewModel : BaseViewModel, IDisposable
    {
        public MainWindowViewModel(LoadingScreenService loadingScreenService, MessageBoxService messageBoxService)
        {
            SerializationService = new SerializationService();
            SaveCommand = new RelayCommand(OnSave);
            ExitCommand = new RelayCommand(OnExit);
            ExportPlaylistCommand = new RelayCommand(OnExportPlaylist);
            ImportPlaylistCommand = new RelayCommand(OnImportPlaylist);
            OpenDiscordCommand = new RelayCommand(OnOpenDiscordCommand);
            OpenUserGuideCommand = new RelayCommand(OnOpenUserGuideCommand);
            CreateDiagnosticCommand = new RelayCommand(OnCreateDiagnosticCommand);
            Stages = new PlaylistsContainerViewModel<StageViewModel>("Stages");
            FlowScenes = new PlaylistsContainerViewModel<FlowSceneViewModel>("Menus");
            CustomScreens = new PlaylistsContainerViewModel<CustomScreenViewModel>("Misc");
            Characters = new PlaylistsContainerViewModel<CharacterViewModel>("Characters");
            List<IValid> containers = new List<IValid>
            {
                Stages,
                FlowScenes,
                CustomScreens,
                Characters
            };
            Notification = new NotificationViewModel(containers);
            LoadingScreenService = loadingScreenService;
            MessageBoxService = messageBoxService;
        }

        private void OnOpenUserGuideCommand(object obj)
        {
            Process.Start(new ProcessStartInfo
            {
                FileName = "https://docs.google.com/document/d/1Q19xke2ys-sf-xg2-zhau5odeNgITSClGs7lRVzEFbQ",
                UseShellExecute = true
            });
        }

        private void OnCreateDiagnosticCommand(object obj)
        {
            string diagnosticDir = Path.Combine(Path.GetTempPath(), "JukeboxDiagnostics");
            if(Directory.Exists(diagnosticDir))
            {
                Directory.Delete(diagnosticDir, true);
            }

            Directory.CreateDirectory(diagnosticDir);
            var playlistManagerDir = Directory.GetParent(Assembly.GetExecutingAssembly().Location);
            if(playlistManagerDir != null && playlistManagerDir.Parent != null)
            {
                var reframeworkDir = playlistManagerDir.Parent;
                var settingsFile = Path.Combine(reframeworkDir.FullName, @"plugins\settings.xml");
                if (File.Exists(settingsFile))
                {
                    File.Copy(settingsFile, Path.Combine(diagnosticDir, "settings.xml"));
                }
                else
                {
                    File.CreateText(Path.Combine(diagnosticDir, "No settings file found.txt")).Close();
                }
                var overlayLogsDir = Path.Combine(reframeworkDir.FullName, @"SF6JukeboxOverlay\logs.txt");
                if(File.Exists(overlayLogsDir))
                {
                    File.Copy(overlayLogsDir, Path.Combine(diagnosticDir, "overlay_logs.txt"));
                }
                else
                {
                    File.CreateText(Path.Combine(diagnosticDir, "No overlay logs found.txt")).Close();
                }
                var playlistManagerLogsDir = Path.Combine(reframeworkDir.FullName, @"PlaylistManager\logs.txt");
                if(File.Exists(playlistManagerLogsDir))
                {
                    File.Copy(playlistManagerLogsDir, Path.Combine(diagnosticDir, "playlist_manager_logs.txt"));
                }
                else
                {
                    File.CreateText(Path.Combine(diagnosticDir, "No playlist manager logs found.txt")).Close();
                }
                var sf6Dir = reframeworkDir.Parent;
                if(sf6Dir != null)
                {
                    string logsFilePath = Path.Combine(sf6Dir.FullName, "re2_framework_log.txt");
                    if(!File.Exists(logsFilePath))
                    {
                        string reframeworkDllPath = Path.Combine(sf6Dir.FullName, "dinput8.dll");
                        if(!File.Exists(reframeworkDllPath))
                        {
                            MessageBoxService.ShowOkMessageBox("Looks like you do not have reframework properly installed.\r\nPlease have a look at the user guide for instructions.", "Error");
                        }
                        File.CreateText(Path.Combine(diagnosticDir, "No logs found.txt")).Close();
                    }
                    else
                    {
                        File.Copy(logsFilePath, Path.Combine(diagnosticDir, Path.Combine(diagnosticDir, "re2_framework_log.txt")));
                    }
                }
                else
                {
                    MessageBoxService.ShowOkMessageBox("Looks like you installed the mod in the wrong folder", "Error");
                }
            }
            else
            {
                MessageBoxService.ShowOkMessageBox("Looks like you installed the mod in the wrong folder", "Error");
            }

            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "Jukebox Diagnostic file (*.zip)|*.zip";
            if (saveFileDialog.ShowDialog() == true)
            {
                ZipFile.CreateFromDirectory(diagnosticDir, saveFileDialog.FileName);
                string argument = "/select, \"" + saveFileDialog.FileName + "\"";
                Process.Start("explorer.exe", argument);
            }                
        }

        private void OnOpenDiscordCommand(object obj)
        {
            Process.Start(new ProcessStartInfo
            {
                FileName = "https://discord.gg/gmVP33TCRD",
                UseShellExecute = true
            });
        }

        private void OnExit(object obj)
        {
            Application.Current.Shutdown();
        }

        private void OnImportPlaylist(object obj)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Jukebox Playlist (*.jpl)|*.jpl";
            if (openFileDialog.ShowDialog() == true)
            {
                EventAggregator.Publish(new ImportPlaylistRequestedEvent() { PlaylistFilePath = openFileDialog.FileName});
            }
        }

        private void OnExportPlaylist(object obj)
        {
            EventAggregator.Publish(new ExportPlaylistRequestedEvent());
        }

        public string LogsPath
        {
            get
            {
                string logsPath = Directory.GetParent(Assembly.GetExecutingAssembly().Location).FullName;
                return Path.Combine(logsPath, "logs.txt");
            }
        }

        public ILogger Logger { get; set; }

        public Settings SettingsModel { get; private set; }

        public void Load()
        {
            SettingsModel = SerializationService.Load();
            Logger = new FileLogger(LogsPath);
            PlayerService = new PlayerService(SettingsModel, Logger);
            Settings = new SettingsViewModel(SettingsModel);
            var allTracks = new List<string>();
            foreach (var stage in SettingsModel.Stages)
            {
                allTracks.AddRange(stage.Playlist.Tracks.Select(t => t.Path));
            }

            foreach(var flowScene in SettingsModel.FlowScenes)
            {
                allTracks.AddRange(flowScene.Playlist.Tracks.Select(t => t.Path));
            }

            foreach(var customScreen in SettingsModel.CustomScreens) 
            { 
                allTracks.AddRange(customScreen.Playlist.Tracks.Select(t => t.Path));
            }

            foreach (var characters in SettingsModel.Characters)
            {
                allTracks.AddRange(characters.Playlist.Tracks.Select(t => t.Path));
            }

            if (Directory.Exists(Settings.RandomPlaylistPath))
            {
                allTracks.AddRange(Directory.GetFiles(Settings.RandomPlaylistPath, "*.mp3", SearchOption.AllDirectories));                
            }
            ValidateTracks(allTracks.Distinct().ToList());
        }

        public void UpdatePlaylists()
        {
            foreach (var stage in SettingsModel.Stages)
            {
                Stages.Playlists.Add(new StageViewModel(stage, PlayerService, LoadingScreenService));
            }

            foreach (var flowScene in SettingsModel.FlowScenes)
            {
                FlowScenes.Playlists.Add(new FlowSceneViewModel(flowScene, PlayerService, LoadingScreenService));
            }

            foreach (var customScreen in SettingsModel.CustomScreens)
            {
                CustomScreens.Playlists.Add(new CustomScreenViewModel(customScreen, PlayerService, LoadingScreenService));
            }

            foreach (var characters in SettingsModel.Characters)
            {
                Characters.Playlists.Add(new CharacterViewModel(characters, PlayerService, LoadingScreenService));
            }
        }

        public NotificationViewModel Notification { get; set; }
        public PlaylistsContainerViewModel<StageViewModel> Stages { get; set; }
        public PlaylistsContainerViewModel<FlowSceneViewModel> FlowScenes { get; set; }
        public PlaylistsContainerViewModel<CustomScreenViewModel> CustomScreens { get; set; }
        public PlaylistsContainerViewModel<CharacterViewModel> Characters { get; set; }
        public SettingsViewModel Settings { get; set; }
        public SerializationService SerializationService {get;}
        public RelayCommand SaveCommand { get; set; }
        public RelayCommand ExitCommand { get; set; }
        public RelayCommand OpenUserGuideCommand { get; set; }
        public RelayCommand OpenDiscordCommand { get; set; }
        public RelayCommand CreateDiagnosticCommand { get; set; }
        public RelayCommand ExportPlaylistCommand { get; set; }
        public RelayCommand ImportPlaylistCommand { get; set; }
        public PlayerService PlayerService { get; private set; }
        public LoadingScreenService LoadingScreenService { get; }
        public MessageBoxService MessageBoxService { get; }

        private void ValidateTracks(List<string> allTracks)
        {
            var invalidFiles = PlayerService.Resampler.GetFilesWithInvalidSampleRate(allTracks, (file, progress) =>
            {
                EventAggregator.Publish(new LoadingProgressEvent(progress, "Verifying audio files", $"{Path.GetFileName(file)}"));
            });
            if (invalidFiles.Any())
            {
                string message = $"You have {invalidFiles.Count} track(s) with a sample rate incompatible with the jukebox.\r\nDo you want the Playlist Manager to fix this?";
                var m = new MessageBoxEvent(message, "Error", MessageBoxType.YesNo);
                m.OnYes = new Action(() =>
                {
                    LoadingScreenService.Show("Resampling audio files", ()=>
                    {
                        PlayerService.Resampler.Resample(invalidFiles, (file, progress) =>
                        {
                            EventAggregator.Publish(new LoadingProgressEvent(progress, "Resampling audio files", Path.GetFileName(file)));
                        });
                    }, ()=>
                    {
                        UpdatePlaylists();
                    });
                });
                m.OnNo = new Action(() =>
                {
                    UpdatePlaylists();
                });

                EventAggregator.Publish(m);
            }
            else
            {
                UpdatePlaylists();
                EventAggregator.Publish(new LoadingProgressEvent(1, String.Empty, String.Empty));
            }
        }

        private void OnSave(object obj)
        {
            if (Save())
            {
                EventAggregator.Publish(new MessageBoxEvent("Settings saved!", "Success"));
            }
            else
            {
                EventAggregator.Publish(new MessageBoxEvent("Failed to save settings.", "Error"));
            }
        }

        public bool Save()
        {
            Settings settings = new Settings();
            settings.Enabled = Settings.IsEnabled ? 1 : 0;
            settings.RandomPlaylistPath = Settings.RandomPlaylistPath;
            settings.Volume = Settings.Volume;
            settings.OverlayEnabled = Settings.IsOverlayEnabled ? 1 : 0;
            settings.PlaylistPriority = Settings.SelectedPlaylistPriority.Model;
            settings.MusicOverrideMode = Settings.MusicOverrideMode;
            settings.Shortcuts = Settings.Shortcuts.GetModel();
            settings.Stages = new List<Stage>();
            settings.FlowScenes = new List<FlowScene>();
            settings.CustomScreens = new List<CustomScreen>();
            settings.Characters = new List<Character>();
            settings.Version = Settings.Version;

            foreach (var stage in Stages.Playlists)
            {
                settings.Stages.Add(stage.ToModel());
            }

            foreach (var flowScene in FlowScenes.Playlists)
            {
                settings.FlowScenes.Add(flowScene.ToModel());
            }

            foreach (var customScreen in CustomScreens.Playlists)
            {
                settings.CustomScreens.Add(customScreen.ToModel());
            }

            foreach (var character in Characters.Playlists)
            {
                settings.Characters.Add(character.ToModel());
            }

            return SerializationService.Save(settings);
        }

        public void Dispose()
        {
            if(PlayerService != null)
            {
                PlayerService.Dispose();
            }
        }
    }
}
