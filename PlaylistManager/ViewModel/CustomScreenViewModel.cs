﻿using MediaPlayerDAL.Model;
using PlaylistManager.Service;
using System;

namespace PlaylistManager.ViewModel
{
    public class CustomScreenViewModel : BaseScreenViewModel<CustomScreen>
    {
        public CustomScreenViewModel(CustomScreen model, PlayerService playerService, LoadingScreenService loadingScreenService) : base(model, playerService, loadingScreenService)
        {
        }

        public override string ThumbnailPath => String.Format("/PlaylistManager;component/Images/CustomScreens/{0}.png", Model.CustomScreenId);

        public override bool SupportsDynamicTracks => false;
    }
}
