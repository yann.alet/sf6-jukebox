﻿using PlaylistManager.GlobalEvent;
using PlaylistManager.Service;
using System;
using System.Windows;

namespace PlaylistManager.ViewModel
{
    public class LoadingViewModel : BaseViewModel
    {
        public LoadingViewModel()
        {
            EventAggregator.Subscribe<LoadingProgressEvent>(OnLoadingProgress);
        }

        private void OnLoadingProgress(LoadingProgressEvent e)
        {
            TaskTitle = e.TaskTitle;
            TaskDescription = e.TaskDescription;
            Progress = e.Progress;

            if(e.Progress == 1)
            {
                EventAggregator.Unsubscribe<LoadingProgressEvent>(OnLoadingProgress);
                if (View != null)
                {
                    View.Dispatcher.BeginInvoke(new Action(() =>
                    {
                        View.Close();
                    }));
                }
            }
        }

        public Window View { get; set; }

        private float progress;
        private string taskTitle;
        private string taskDescription;

        public float Progress 
        { 
            get => progress;
            set
            {
                progress = value;
                OnPropertyChanged();
            }
        }

        public string TaskTitle 
        {
            get => taskTitle; 
            set
            {
                taskTitle = value;
                OnPropertyChanged();
            }
        }

        public string TaskDescription 
        { 
            get => taskDescription; 
            private set
            {
                taskDescription = value;
                OnPropertyChanged();
            }
        }
    }
}
