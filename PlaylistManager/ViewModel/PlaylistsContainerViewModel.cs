﻿using MediaPlayerDAL.Contract;
using PlaylistManager.Contract;
using PlaylistManager.GlobalEvent;
using PlaylistManager.Service;
using System;
using System.Collections.ObjectModel;

namespace PlaylistManager.ViewModel
{
    public class PlaylistsContainerViewModel<T> : BaseViewModel, IValid where T: IValid
    {
        private bool isValid;

        public PlaylistsContainerViewModel(string name)
        {
            Name = name;
            Playlists = new ObservableCollection<T>();
            Playlists.CollectionChanged += Playlists_CollectionChanged;
            EventAggregator.Subscribe<PlaylistValidityUpdatedEvent>(OnPlaylistValidityUpdated);            
        }        

        public string Name { get; }

        public ObservableCollection<T> Playlists { get; private set; }

        public bool IsValid
        {
            get => isValid;
            set
            {
                isValid = value;
                OnPropertyChanged();
            }
        }

        private void OnPlaylistValidityUpdated(PlaylistValidityUpdatedEvent @event)
        {
            Validate();
        }        

        private void Playlists_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Validate();
        }

        public void Validate()
        {
            IsValid = true;
            foreach (var playlist in Playlists)
            {
                IsValid &= playlist.IsValid;
            }
        }
    }
}
