﻿using PlaylistManager.Service;
using System.Collections.ObjectModel;
using PlaylistManager.Contract;
using MediaPlayerDAL.Contract;
using System.Linq;
using Microsoft.Win32;
using System.Windows;
using System.Collections.Generic;
using MediaPlayerDAL.Model;
using PlaylistManager.GlobalEvent;
using System;

namespace PlaylistManager.ViewModel
{
    public abstract class BaseScreenViewModel<T> : BaseViewModel, IValid, IScreenViewModel<T> where T : IScreen
    {
        private bool isRandomPlaylist;
        private bool isValid;
        private bool isEnabled;
        //private ObservableCollection<TrackViewModel> selectedTracks;
        private TrackViewModel selectedTrack;

        public BaseScreenViewModel(T model, PlayerService playerService, LoadingScreenService loadingScreenService)
        {
            Model = model;
            IsEnabled = model.Enabled == 1;
            PlayerService = playerService;
            LoadingScreenService = loadingScreenService;
            Playlist = new ObservableCollection<TrackViewModel>();
            foreach(var track in Model.Playlist.Tracks)
            {
                Playlist.Add(new TrackViewModel(track, PlayerService, LoadingScreenService, SupportsDynamicTracks));
            }

            IsRandomPlaylist = Model.IsRandomPlaylist == 1;
            AddTrackCommand = new RelayCommand(OnAddTrackCommand);
            RemoveTrackCommand = new RelayCommand(OnRemoveTrackCommand, x => { return Playlist.Any(t => t.IsSelected); });
            Playlist.CollectionChanged += Playlist_CollectionChanged;
            EventAggregator.Subscribe<TrackValidityUpdatedEvent>(OnTrackValidityUpdated);
            Validate();
        }

        public ObservableCollection<TrackViewModel> Playlist { get; set; }
        public T Model { get; }
        public PlayerService PlayerService { get; }
        public LoadingScreenService LoadingScreenService { get; }
        public RelayCommand AddTrackCommand { get; set; }
        public RelayCommand RemoveTrackCommand { get; set; }
        public abstract string ThumbnailPath { get; }
        public PlayMode CurrentMode { get => Model.Playlist.Mode; set => Model.Playlist.Mode = value; }
        public float FadeOutDuration { get => Model.Playlist.FadeOutDuration; set => Model.Playlist.FadeOutDuration = value; }
        
        public TrackViewModel SelectedTrack
        {
            get => selectedTrack;
            set
            {
                selectedTrack = value;
                if (selectedTrack != null)
                {
                    selectedTrack.SelectedTransition = selectedTrack.Transitions.First();
                }
                OnPropertyChanged();
            }
        }

        //public ObservableCollection<TrackViewModel> SelectedTracks 
        //{
        //    get => selectedTracks; 
        //    set
        //    {
        //        selectedTracks = value;
        //        if(selectedTracks != null)
        //        {
        //            selectedTracks.First().SelectedTransition = selectedTracks.First().Transitions.First();
        //        }
        //        OnPropertyChanged();
        //    }
        //}

        public bool IsEnabled
        {
            get => isEnabled;
            set
            {
                isEnabled = value;
                OnPropertyChanged();
            }
        }

        public bool IsValid 
        { 
            get => isValid; 
            private set
            {
                bool wasUpdated = isValid != value;
                isValid = value;
                OnPropertyChanged();
                if(wasUpdated)
                {
                    EventAggregator.Publish(new PlaylistValidityUpdatedEvent(this));
                }
            }

        }

        private void OnTrackValidityUpdated(TrackValidityUpdatedEvent @event)
        {
            Validate();
        }

        public string Name => Model.Name;

        private void Playlist_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            Validate();
            if(!Playlist.Any())
            {
                IsRandomPlaylist = true;
            }
        }

        public void Validate()
        {
            IsValid = true;
            foreach (var track in Playlist)
            {
                IsValid &= track.IsPathValid && track.IsCorrectSampleRate && track.HasValidTransitions;
            }
        }

        public bool IsRandomPlaylist 
        {
            get => isRandomPlaylist; 
            set
            {
                isRandomPlaylist = value;
                OnPropertyChanged();
            }
        }

        public abstract bool SupportsDynamicTracks { get; }

        private void OnRemoveTrackCommand(object obj)
        {
            for (int i = 0; i < Playlist.Count; i++)
            {
                if (Playlist[i].IsSelected)
                {
                    Playlist.RemoveAt(i);
                    i--;
                }
            }
        }

        public void RemoveSelectedTracks()
        {            
            for(int i = 0; i < Playlist.Count;i++)
            {
                if (Playlist[i].IsSelected)
                {
                    Playlist.RemoveAt(i--);
                }
            }
        }

        private void OnAddTrackCommand(object obj)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "MP3 file|*.mp3";
            dialog.Multiselect = true;
            bool? result = dialog.ShowDialog();
            if (result != null && result.Value)
            {
                if (Playlist.Any(t => t.FullPath == dialog.FileName))
                {
                    MessageBox.Show("Track already added to this playlist", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
                else
                {
                    AddTracksToPlaylist(dialog.FileNames.ToList());
                }
            }
        }

        internal virtual void AddTracksToPlaylist(List<string> tracks)
        {
            if(tracks.Any())
            {
                IsRandomPlaylist = false;
            }

            foreach (string track in tracks)
            {
                var playbackSettings = new PlaybackSettings();
                playbackSettings.Volume = 0.5f;
                playbackSettings.Gain = 1;
                playbackSettings.StartupSilence = TimeSpan.Zero;
                playbackSettings.StartupTrimming = TimeSpan.Zero;
                playbackSettings.EndTrimming = TimeSpan.Zero;
                playbackSettings.TransitionIsSynced = false;
                Playlist.Add(new TrackViewModel(new Track() { Path = track, PlaybackSettings = playbackSettings }, PlayerService, LoadingScreenService, SupportsDynamicTracks));
            }

            IsEnabled = true;
        }

        public virtual T ToModel()
        {
            Model.IsRandomPlaylist = IsRandomPlaylist ? 1 : 0;
            Model.Enabled = IsEnabled ? 1 : 0;
            Model.Playlist.Tracks = Playlist.Select(t => t.GetModel()).ToList();
            Model.Playlist.FadeOutDuration = FadeOutDuration;            
            return Model;
        }
    }
}
