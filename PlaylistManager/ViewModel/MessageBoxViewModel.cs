﻿using PlaylistManager.View;
using System;
using System.Windows;

namespace PlaylistManager.ViewModel
{
    public class MessageBoxViewModel : BaseViewModel
    {
        public MessageBoxViewModel(string title, string message, MessageBoxType type)
        {
            Title = title;
            Message = message;
            Type = type;

            OkCommand = new RelayCommand(x => 
            {
                OnOk?.Invoke();
                Owner?.Close();
            });
            YesCommand = new RelayCommand(x =>
            {
                OnYes?.Invoke();
                Owner?.Close();
            });
            NoCommand = new RelayCommand(x =>
            {
                OnNo?.Invoke();
                Owner?.Close();
            });
            CancelCommand = new RelayCommand(x =>
            {
                OnCancel?.Invoke();
                Owner?.Close();
            });
        }

        public Window Owner { get; set; }
        
        public string Message { get; set; }
        public string Title { get; set; }
        public MessageBoxType Type { get; set; }

        public Action OnOk { get; set; }
        public Action OnYes { get; set; }
        public Action OnNo { get; set; }
        public Action OnCancel { get; set; }

        public RelayCommand OkCommand { get; }
        public RelayCommand YesCommand { get; }
        public RelayCommand NoCommand { get; }
        public RelayCommand CancelCommand { get; }
    }
}
