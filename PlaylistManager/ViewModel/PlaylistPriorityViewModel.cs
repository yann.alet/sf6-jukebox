﻿using MediaPlayerDAL;

namespace PlaylistManager.ViewModel
{
    public class PlaylistPriorityViewModel : BaseViewModel
    {
        public PlaylistPriorityViewModel(PlaylistPriority model)
        {
            Model = model;
        }

        public string Name 
        { 
            get
            {
                switch (Model)
                {
                    case PlaylistPriority.Random:
                        return "Random";
                    case PlaylistPriority.Stage:
                        return "Stage";
                    case PlaylistPriority.P1:
                        return "Player 1";
                    case PlaylistPriority.P2:
                        return "Player 2";
                    case PlaylistPriority.Player:
                        return "Player's character";
                    case PlaylistPriority.Opponent:
                        return "Opponent's character";
                    default:
                        return string.Empty;
                }
            }
        }

        public PlaylistPriority Model { get; }
    }
}
