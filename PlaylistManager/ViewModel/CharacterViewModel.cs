﻿using MediaPlayerDAL.Model;
using PlaylistManager.Service;
using System;

namespace PlaylistManager.ViewModel
{
    internal class CharacterViewModel : BaseScreenViewModel<Character>
    {
        public CharacterViewModel(Character model, PlayerService playerService, LoadingScreenService loadingScreenService) : base(model, playerService, loadingScreenService)
        {
        }

        public override string ThumbnailPath => String.Format("/PlaylistManager;component/Images/Characters/{0}.png", Model.CharacterId);

        public override bool SupportsDynamicTracks => true;
    }
}
