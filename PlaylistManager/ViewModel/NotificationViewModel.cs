﻿using PlaylistManager.Contract;
using PlaylistManager.GlobalEvent;
using PlaylistManager.Service;
using System.Collections.Generic;

namespace PlaylistManager.ViewModel
{
    public class NotificationViewModel : BaseViewModel
    {
        private bool _isVisibile;
        private string _description = string.Empty;

        public NotificationViewModel(List<IValid> playlistsContainers)
        {
            IsVisibile = false;
            EventAggregator.Subscribe<PlaylistValidityUpdatedEvent>(OnPlaylistValidityUpdated);
            PlaylistsContainers = playlistsContainers;
        }

        private List<IValid> PlaylistsContainers { get; }

        public string Description 
        { 
            get => _description;
            set
            {
                _description = value;
                OnPropertyChanged();
            }
        }

        public bool IsVisibile
        {
            get => _isVisibile;
            set
            {
                _isVisibile = value;
                OnPropertyChanged();
            }
        }

        private void UpdateVisibility()
        {
            IsVisibile = false;
            foreach (var playlistContainer in PlaylistsContainers)
            {
                IsVisibile |= !playlistContainer.IsValid;
            }

            if(IsVisibile)
            {
                Description = "You have one or more playlist(s) in an invalid state.";
            }
        }

        private void OnPlaylistValidityUpdated(PlaylistValidityUpdatedEvent e)
        {
            UpdateVisibility();
        }
    }
}
