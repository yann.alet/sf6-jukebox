﻿using MediaPlayerDAL.Model;
using System;

namespace PlaylistManager.ViewModel
{
    public class PlaybackSettingsViewModel : BaseViewModel
    {
        private bool isDirty;

        public PlaybackSettingsViewModel(PlaybackSettings playbackSettings)
        {
            PlaybackSettingsModel = playbackSettings;
            PlaybackSettingsModelBackup = PlaybackSettingsModel.Clone();
        }

        public PlaybackSettings PlaybackSettingsModel { get; private set; }
        private PlaybackSettings PlaybackSettingsModelBackup { get; set; }

        public float? Volume
        {
            get
            {
                return PlaybackSettingsModel.Volume;
            }
            set
            {
                PlaybackSettingsModel.Volume = value;
            }
        }

        public bool? IsInfinite
        {
            get
            {
                return PlaybackSettingsModel.IsInfinite;
            }
            set
            {
                PlaybackSettingsModel.IsInfinite = value;
            }
        }

        public float? Gain
        {
            get
            {
                return PlaybackSettingsModel.Gain;
            }
            set
            {
                PlaybackSettingsModel.Gain = value;
                IsDirty = true;
            }
        }

        public TimeSpan? StartupSilence
        {
            get
            {
                return PlaybackSettingsModel.StartupSilence;
            }
            set
            {
                PlaybackSettingsModel.StartupSilence = value;
                IsDirty = true;
            }
        }

        public TimeSpan? StartupTrimming
        {
            get
            {
                return PlaybackSettingsModel.StartupTrimming;
            }
            set
            {
                PlaybackSettingsModel.StartupTrimming = value;
                IsDirty = true;
            }
        }

        public TimeSpan? EndTrimming
        {
            get
            {
                return PlaybackSettingsModel.EndTrimming;
            }
            set
            {
                PlaybackSettingsModel.EndTrimming = value;
                IsDirty = true;
            }
        }

        public bool TransitionIsSynced
        {
            get
            {
                return PlaybackSettingsModel.TransitionIsSynced;
            }
            set
            {
                PlaybackSettingsModel.TransitionIsSynced = value;
                IsDirty = true;
            }
        }

        public bool IsDirty 
        { 
            get => isDirty; 
            set => isDirty = value; 
        }

        public void Revert()
        {
            PlaybackSettingsModel = PlaybackSettingsModelBackup;
            PlaybackSettingsModelBackup = PlaybackSettingsModel.Clone();
            IsDirty = false;
        }
    }
}
