﻿using MediaPlayerDAL.Model;
using PlaylistManager.GlobalEvent;
using PlaylistManager.Service;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

namespace PlaylistManager.ViewModel.TrackEditor
{
    public class TrackEditorViewModel : BaseViewModel
    {
        private ObservableCollection<TrackWaveViewModel> _tracks;
        private RelayCommand _playCommand;
        private RelayCommand _pauseResumeCommand;
        private RelayCommand _resumeCommand;
        private RelayCommand _stopCommand;
        private int _pixelLengthInMilliSeconds;
        private float scale;
        private float _volume;
        private bool _isPlaying;
        private bool _isPaused;

        public event EventHandler OnPlay;
        public event EventHandler OnStop;
        public event EventHandler OnPause;
        public event EventHandler OnResume;
        public event EventHandler<int> OnSetPosition;

        public bool IsMultiTrackEnabled { get; set; }

        public TrackEditorViewModel(MessageBoxService messageBoxService, LoadingScreenService loadingScreenService, PlayerService playerService, TrackViewModel trackViewModel)
        {
            MessageBoxService = messageBoxService;
            LoadingScreenService = loadingScreenService;
            PlayerService = playerService;
            TrackViewModel = trackViewModel;
        }

        public bool SupportsDynamicTracks => TrackViewModel.SupportsDynamicTracks;

        public void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            if(TrackViewModel.IsDirty)
            {
                MessageBoxService.ShowYesNoCancelMessageBox("Would you like to save your changes?", "Save changes",
                () => 
                { 
                    if(!Save())
                    {
                        e.Cancel = true;
                    }
                },
                () => { Revert(); },
                () => { Revert(); e.Cancel = true; });
            }            
        }

        public bool Save()
        {
            if (Validate())
            {
                Tracks.First().Track.ClearIsDirty();
                SaveOnClose = true;
                return true;
            }

            return false;
        }

        private bool Validate()
        {
            var firstTrackPlaybackSettings = Tracks.First().Track.PlaybackSettings;
            if (firstTrackPlaybackSettings.StartupSilence.HasValue && firstTrackPlaybackSettings.StartupSilence.Value.TotalMilliseconds > 0)
            {
                MessageBoxService.ShowOkMessageBox("The first track cannot have a delayed start.", "Error");
                return false;
            }

            if (Tracks.All(t => t.IsSyncedMode))
            {
                foreach (TrackWaveViewModel outerTrack in Tracks)
                {
                    foreach (TrackWaveViewModel innerTrack in Tracks.Where(t => t != outerTrack))
                    {
                        TimeSpan diff = innerTrack.GetActualTrackLength() - outerTrack.GetActualTrackLength();
                        if (diff.TotalMilliseconds > 200)
                        {
                            MessageBoxService.ShowOkMessageBox("Tracks must end at the same time code.", "Error");
                            return false;
                        }
                    }
                }
            }
            else if(Tracks.Count > 1 && !Tracks.All(t => t.IsSyncedMode == false))
            {
                MessageBoxService.ShowOkMessageBox("Cannot mix synced and not synced tracks.", "Error");
                return false;
            }
            return true;
        }

        public void Revert()
        {
            TrackViewModel.Revert();
            SaveOnClose = false;
        }

        public bool SaveOnClose { get; private set; }

        public ObservableCollection<TrackWaveViewModel> Tracks 
        { 
            get => _tracks;
            set
            {
                _tracks = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand PlayCommand
        {
            get => _playCommand;
            set
            {
                _playCommand = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand PauseResumeCommand
        {
            get => _pauseResumeCommand;
            set
            {
                _pauseResumeCommand = value;
                OnPropertyChanged();
            }
        }

        public RelayCommand StopCommand
        {
            get => _stopCommand;
            set
            {
                _stopCommand = value;
                OnPropertyChanged();
            }
        }

        public MessageBoxService MessageBoxService { get; }
        public LoadingScreenService LoadingScreenService { get; }
        public PlayerService PlayerService { get; }
        
        public TrackViewModel TrackViewModel { get; }        

        const int DefaultPixelLengthInMilliSeconds = 100;

        public float Volume 
        { 
            get => _volume; 
            set
            {
                _volume = value;
                PlayerService.SetPlaybackSettings(new PlaybackSettings() { Volume = _volume });
            }
        }

        public float Scale
        {
            get => scale; 
            set 
            { 
                scale = value;
                OnPropertyChanged();
                PixelLengthInMilliSeconds = Convert.ToInt32(DefaultPixelLengthInMilliSeconds / Scale);
                if (Tracks != null)
                {
                    foreach (var track in Tracks)
                    {
                        track.Redraw(PixelLengthInMilliSeconds);
                    }
                }
            }
        }

        public int PixelLengthInMilliSeconds
        {
            get => _pixelLengthInMilliSeconds;
            set
            {
                _pixelLengthInMilliSeconds = value;
                OnPropertyChanged();
            }
        }

        public bool IsPlaying 
        { 
            get => _isPlaying; 
            set
            {
                _isPlaying = value;
                OnPropertyChanged();
            }
        }

        public bool IsPaused
        {
            get => _isPaused;
            set
            {
                _isPaused = value;
                OnPropertyChanged();
            }
        }

        private int LastTrackIndex { get; set; } = 1;

        public void Init()
        {
            PixelLengthInMilliSeconds = 100;
            Scale = 1.0f;
            float tracksCount = TrackViewModel.GetTracksCount();
            TrackViewModel.IsActive = true;
            IsFirstRead = true;
            var rootTrackWaveViewModel = new TrackWaveViewModel(PlayerService, MessageBoxService, TrackViewModel, this, PixelLengthInMilliSeconds) { Index = 1, IsActive = true };
            rootTrackWaveViewModel.Transitions = new ObservableCollection<TransitionViewModel>() { new TransitionViewModel(TrackViewModel, new Transition() { TransitionType = MediaPlayerDAL.TransitionType.RoundInit}, PlayerService, LoadingScreenService) };
            rootTrackWaveViewModel.Transition = rootTrackWaveViewModel.Transitions.First();
            rootTrackWaveViewModel.IsActive = true;
            Tracks = new ObservableCollection<TrackWaveViewModel> { rootTrackWaveViewModel };
            EventAggregator.Publish(new LoadingProgressEvent(LastTrackIndex / tracksCount, "Loading tracks", TrackViewModel.FileName));
            
            foreach (var transition in TrackViewModel.Transitions)
            {
                foreach (var track in transition.Tracks)
                {
                    var trackWave = new TrackWaveViewModel(PlayerService, MessageBoxService, track, this, PixelLengthInMilliSeconds) { Transition = transition, Transitions = TrackViewModel.Transitions, ParentTrack = TrackViewModel, Index = ++LastTrackIndex, IsActive = false };
                    Tracks.Add(trackWave);
                    EventAggregator.Publish(new LoadingProgressEvent(LastTrackIndex / tracksCount, "Loading tracks", track.FileName));
                }
            }

            UpdateSnapPoints();

            PlayerService.OnRead += PlayerService_OnRead;

            PlayCommand = new RelayCommand(x =>
            {
                var activeTrack = Tracks.FirstOrDefault(t => t.IsActive);
                if (activeTrack != null)
                {
                    var vm = new PlaybackSettings(activeTrack.Gain)
                    {
                        StartupSilence = TimeSpan.FromMilliseconds(activeTrack.StartOffsetInMilliSeconds),
                        StartupTrimming = TimeSpan.FromMilliseconds(activeTrack.StartTrimmingInMilliSeconds),
                        EndTrimming = TimeSpan.FromMilliseconds(activeTrack.EndTrimmingInMilliSeconds),
                        IsInfinite = true
                    };

                    activeTrack.PlayerId = PlayerService.Play(activeTrack.Track.FullPath, vm);
                    IsPlaying = true;                    
                }
            });
            StopCommand = new RelayCommand(x =>
            {
                IsFirstRead = true;
                IsPlaying = false;
                IsPaused = false;
                PlayerService.Stop();
                OnStop?.Invoke(this, EventArgs.Empty);
            });

            PauseResumeCommand = new RelayCommand(x =>
            {
                if (IsPaused)
                {
                    PlayerService.Resume();
                    IsPaused = !IsPaused;
                    OnResume?.Invoke(this, EventArgs.Empty);
                }
                else
                { 
                    PlayerService.Pause();
                    IsPaused = !IsPaused;
                    OnPause?.Invoke(this, EventArgs.Empty);
                }
            });
        }

        public void UpdateSnapPoints()
        {
            var snapPoints = new Dictionary<TrackWaveViewModel, TimeSpan>();
            foreach(TrackWaveViewModel track in Tracks.Where(t=>t.Track.IsValid))
            {
                snapPoints[track] = track.GetActualTrackLength();
            }

            foreach (TrackWaveViewModel track in Tracks.Where(t => t.Track.IsValid))
            {
                track.SnapPoints = snapPoints.Where(kvp=> kvp.Key != track).Select(sp=>sp.Value).ToList();
            }
        }

        public void SeekTo(int positionInMilliSeconds)
        {
            PlayerService.SetPosition(positionInMilliSeconds);
            OnSetPosition(this, positionInMilliSeconds);
        }

        private void AddTrackWaveViewModels(IList<TrackViewModel> tracks)
        {
            float i = 0;
            foreach (var track in tracks)
            {
                var trackWave = new TrackWaveViewModel(PlayerService, MessageBoxService, track, this, PixelLengthInMilliSeconds) { Transition = TrackViewModel.Transitions.First(), Transitions = TrackViewModel.Transitions, ParentTrack = TrackViewModel, Index = ++LastTrackIndex };
                Tracks.Add(trackWave);
                EventAggregator.Publish(new LoadingProgressEvent(++i / tracks.Count, "Loading tracks", track.FileName));
            }

            UpdateSnapPoints();
        }

        bool IsFirstRead { get; set; }

        private void PlayerService_OnRead(object sender, PlayerService.OnReadInfo e)
        {
            if(IsFirstRead)
            {
                OnPlay?.Invoke(this, EventArgs.Empty);
                IsFirstRead = false;
            }
        }

        private PlaybackSettings GetDefaultPlaybackSettings()
        {
            return new PlaybackSettings
            {
                Gain = 1.0f,
                Volume = 0.5f,
                IsInfinite = true,
                EndTrimming = TimeSpan.Zero,
                StartupSilence = TimeSpan.Zero,
                StartupTrimming = TimeSpan.Zero,
                TransitionIsSynced = false
            };
        }

        internal void AddTracks(List<string> list)
        {
            var addedTracks = new List<TrackViewModel>();
            foreach(string track in list)
            {
                var trackViewModel = new TrackViewModel(new Track() { Path = track, PlaybackSettings = GetDefaultPlaybackSettings() }, PlayerService, LoadingScreenService, false);
                TrackViewModel.Transitions.First().AddTrack(trackViewModel);
                addedTracks.Add(trackViewModel);
            }

            LoadingScreenService.Show("Loading tracks...", () => AddTrackWaveViewModels(addedTracks));
        }
    }
}
