﻿using MediaPlayerDAL.Model;
using MediaPlayerLib.NAudio;
using PlaylistManager.GlobalEvent;
using PlaylistManager.Service;
using PlaylistManager.SignalProcessing;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace PlaylistManager.ViewModel.TrackEditor
{
    public class TrackWaveViewModel : BaseViewModel, IDisposable
    {
        public int WaveHeight => 120;
        public int WaveWidth
        {
            get => waveWidth;
            set
            {
                waveWidth = value;
                OnPropertyChanged();
            }
        }

        private float progress;
        private TransitionViewModel transition;
        private float gain;
        private WriteableBitmap waveImage;
        private int waveWidth;
        private bool _isActive;
        private bool _isActiveTransition;
        //private bool _isActiveEnabled;
        //private bool _isTransitionEnabled;

        public TrackWaveViewModel(PlayerService playerService, MessageBoxService messageBoxService, TrackViewModel track, TrackEditorViewModel trackEditorViewModel, int pixelLengthInMilliSeconds)
        {
            Title = Path.GetFileNameWithoutExtension(track.FileName);
            PlayerId = -1;
            DeleteTrackCommand = new RelayCommand(OnDeleteTrack, x => { return Index > 1 && !trackEditorViewModel.IsPlaying; });
            ShowInExplorerCommand = new RelayCommand(OnShowInExplorer, y => File.Exists(track.FullPath));
            PlayerService = playerService;
            MessageBoxService = messageBoxService;
            TrackEditorViewModel = trackEditorViewModel;
            Track = track;
            if (track.IsValid)
            {
                WaveData = AudioFile.GetAudioData(track.FullPath);
                PlaybackSettings.IsInfinite = true;
                Redraw(pixelLengthInMilliSeconds);
                playerService.OnRead += PlayerService_OnRead;                
                TrackEditorViewModel.OnPlay += TrackEditorViewModel_OnPlay;
                TrackEditorViewModel.OnStop += TrackEditorViewModel_OnStop;
                TrackEditorViewModel.OnPause += TrackEditorViewModel_OnPause;
                TrackEditorViewModel.OnResume += TrackEditorViewModel_OnResume;
                PlayerService.OnCrossFade += PlayerService_OnCrossFade;
            }
        }

        private void OnShowInExplorer(object x)
        {
            string argument = "/select, \"" + Track.FullPath + "\"";
            Process.Start("explorer.exe", argument);
        }

        private void TrackEditorViewModel_OnResume(object sender, EventArgs e)
        {
            OnPropertyChanged(nameof(IsTransitionEnabled));            
        }

        private void TrackEditorViewModel_OnPause(object sender, EventArgs e)
        {
            OnPropertyChanged(nameof(IsTransitionEnabled));
        }

        private void TrackEditorViewModel_OnStop(object sender, EventArgs e)
        {
            IsActiveTransition = false;
            OnPropertyChanged(nameof(IsTransitionEnabled));
            OnPropertyChanged(nameof(IsActiveEnabled));
            PlayerId = -1;
        }

        private void TrackEditorViewModel_OnPlay(object sender, EventArgs e)
        {
            OnPropertyChanged(nameof(IsTransitionEnabled));
            OnPropertyChanged(nameof(IsActiveEnabled));
        }

        public PlayerService PlayerService { get; }
        public MessageBoxService MessageBoxService { get; }
        public TrackViewModel Track { get; }
        public TrackEditorViewModel TrackEditorViewModel { get; }
        public int PlayerId { get; set; }
        public RelayCommand DeleteTrackCommand { get; set; }
        public RelayCommand ShowInExplorerCommand { get; set; }

        private PlaybackSettingsViewModel PlaybackSettings 
        { 
            get
            {
                return Track.PlaybackSettings;
            }
        }

        private void OnDeleteTrack(object param)
        {
            MessageBoxService.ShowYesNoMessageBox("Are you sure you want to delete this track?", "Delete Track", () =>
            {
                var track = TrackEditorViewModel.Tracks.First().Track;
                var transition = track.Transitions.First(t => t.Transition.TransitionType == Transition.Transition.TransitionType);
                track.RemoveTransitionTrack(transition, Track);
                TrackEditorViewModel.Tracks.Remove(this);
            });       
        }

        public bool IsActiveEnabled 
        { 
            get
            {
                return !IsActive && !TrackEditorViewModel.IsPaused && !TrackEditorViewModel.IsPlaying;
            }
        }

        public bool IsTransitionEnabled
        {
            get
            {
                return !IsActive && !_isActiveTransition && !TrackEditorViewModel.IsPaused && TrackEditorViewModel.IsPlaying;
            }
        }

        public bool IsActive
        {
            get => _isActive;
            set
            {
                _isActive = value;
                if(_isActive)
                {
                    if (TrackEditorViewModel.Tracks != null)
                    {
                        foreach (var track in TrackEditorViewModel.Tracks)
                        {
                            if (track != this)
                            {
                                track.IsActive = false;
                            }
                        }
                    }
                }
                OnPropertyChanged();
                OnPropertyChanged(nameof(IsTransitionEnabled));
                OnPropertyChanged(nameof(IsActiveEnabled));
            }
        }

        public bool IsActiveTransition
        {
            get => _isActiveTransition;
            set
            {
                _isActiveTransition = value;
                if (_isActiveTransition)
                {
                    if (TrackEditorViewModel.Tracks != null)
                    {
                        foreach (var track in TrackEditorViewModel.Tracks)
                        {
                            if (track != this)
                            {
                                track.IsActiveTransition = false;
                            }
                        }
                    }
                    StartTransition();
                }
                OnPropertyChanged();
                OnPropertyChanged(nameof(IsTransitionEnabled));
                OnPropertyChanged(nameof(IsActiveEnabled));
            }
        }

        private void StartTransition()
        {
            var vm = new PlaybackSettings(gain)
            {
                StartupSilence = TimeSpan.FromMilliseconds(StartOffsetInMilliSeconds),
                StartupTrimming = TimeSpan.FromMilliseconds(StartTrimmingInMilliSeconds),
                EndTrimming = TimeSpan.FromMilliseconds(EndTrimmingInMilliSeconds),
                TransitionIsSynced = IsSyncedMode
            };
            PlayerId = PlayerService.Transition(Track.FullPath, vm);
            IsActive = true;
            if (!IsSyncedMode)
            {
                TrackEditorViewModel.SeekTo(0);
            }
        }

        public int PixelLengthInMilliSeconds { get; private set; }        

        public int EndTrimmingInMilliSeconds 
        {
            get
            {
                return Convert.ToInt32(PlaybackSettings.EndTrimming.Value.TotalMilliseconds);
            }
            set
            {
                PlaybackSettings.EndTrimming = TimeSpan.FromMilliseconds(value);
                TrackEditorViewModel.UpdateSnapPoints();
            }
        }

        public int StartOffsetInMilliSeconds
        {
            get
            {
                return Convert.ToInt32(PlaybackSettings.StartupSilence.Value.TotalMilliseconds);
            }
            set
            {
                PlaybackSettings.StartupSilence = TimeSpan.FromMilliseconds(value);
                TrackEditorViewModel.UpdateSnapPoints();
            }
        }

        public int StartTrimmingInMilliSeconds
        {
            get
            {
                return Convert.ToInt32(PlaybackSettings.StartupTrimming.Value.TotalMilliseconds);
            }
            set
            {
                PlaybackSettings.StartupTrimming = TimeSpan.FromMilliseconds(value);
                TrackEditorViewModel.UpdateSnapPoints();
            }
        }

        public void Redraw(int pixelLengthInMilliSeconds)
        {
            PixelLengthInMilliSeconds = pixelLengthInMilliSeconds;
            var trackLength = (int)GetTrackLength().TotalMilliseconds;
            WaveWidth = trackLength / pixelLengthInMilliSeconds;
            WriteableBitmap writeableBmp = BitmapFactory.New(WaveWidth, WaveHeight);
            float remap = 1.0f;
            using (writeableBmp.GetBitmapContext())
            {
                writeableBmp.Clear(Colors.Transparent);
                int sampleRate = WaveData.Length / 2 / WaveWidth;
                int mid = WaveHeight / 2;
                int lastI = 0;
                for (int i = 0; i < WaveData.Length; i += 2 * sampleRate)
                {
                    int x = i / sampleRate / 2;
                    float signalValue;
                    signalValue = i == 0 ? WaveData[i] : GetAverage(lastI, i);

                    int strength = Convert.ToInt32(signalValue * mid);
                    writeableBmp.DrawLine(x, mid, x,Convert.ToInt32(mid + strength * remap), Colors.Gray);
                    writeableBmp.DrawLine(x, mid, x, Convert.ToInt32(mid + strength * -1 * remap), Colors.Gray);

                    lastI = i;
                }
            }

            WaveImage = writeableBmp;
        }

        private float GetAverage(int from, int to)
        {
            float average = 0;
            for (int i = from; i < to; i+=2)
            {
                average += Math.Abs(WaveData[i]);
            }
            average /= ((to - from)/2);
            return average;
            /*var fourier = new Fourier(timeList, amplitudeList);
            float avg = Convert.ToSingle(fourier.fft_output.Sum(o => o.Real)) / fourier.fft_output.Count;
            float boost = 1;
            return avg * boost;*/
        }

        private float GetMax(int from, int to)
        {
            float max = 0;
            for (int i = from; i < to; i += 2)
            {
                if (Math.Abs(WaveData[i]) > Math.Abs(max))
                {
                    max = (max + WaveData[i])/2;
                }
            }
            return max;
        }

        private float SampleRate => 44100;

        private float[] WaveData { get; set; }

        public TimeSpan GetTrackLength()
        {
            int size = WaveData.Length;
            int sizePerChannel = size / 2;
            float lengthInSeconds = sizePerChannel / SampleRate;
            var length = TimeSpan.FromSeconds(lengthInSeconds);
            return length;
        }

        public TimeSpan GetActualTrackLength()
        {
            TimeSpan length = GetTrackLength();
            if(Track.PlaybackSettings.EndTrimming.HasValue)
            {
                length = length.Subtract(Track.PlaybackSettings.EndTrimming.Value);
            }
            if(Track.PlaybackSettings.StartupSilence.HasValue)
            {
                length = length.Add(Track.PlaybackSettings.StartupSilence.Value);
            }
            if (Track.PlaybackSettings.StartupTrimming.HasValue)
            {
                length = length.Subtract(Track.PlaybackSettings.StartupTrimming.Value);
            }

            return length;
        }

        public List<TimeSpan> SnapPoints { get; set; }

        private void PlayerService_OnCrossFade(object sender, int e)
        {
            if (e != PlayerId)
            {
                PlayerId = -1;
                IsActive = false;
            }
        }

        public TrackViewModel ParentTrack { get; set; }

        public float Gain
        {
            get
            {
                return PlaybackSettings.Gain.Value;
            }
            set
            {
                PlaybackSettings.Gain = value;
                if (PlayerId != -1)
                {
                    PlayerService.SetPlaybackSettings(PlayerId, PlaybackSettings.PlaybackSettingsModel);
                }
            }
        }

        public int Index { get; set; }

        public TransitionViewModel Transition
        {
            get => transition;
            set
            {
                if (value != null && transition != null)
                {
                    transition.RemoveTrack(Track);
                    value.AddTrack(Track);
                }
                transition = value;
            }
        }
        public ObservableCollection<TransitionViewModel> Transitions { get; set; }

        public string Title { get; set; }

        public float Progress
        {
            get => progress;
            set
            {
                progress = value;
                OnPropertyChanged();
            }
        }

        private void PlayerService_OnRead(object sender, PlayerService.OnReadInfo e)
        {
            if (e.PlayerIndex == PlayerId)
            {
                Progress = e.Position / Convert.ToSingle(e.Length);
            }
        }

        public void Dispose()
        {
            PlayerService.OnRead -= PlayerService_OnRead;
        }

        public WriteableBitmap WaveImage
        {
            get => waveImage;
            set
            {
                waveImage = value;
                OnPropertyChanged();
            }
        }
        
        public bool IsSyncedMode 
        { 
            get => PlaybackSettings.TransitionIsSynced; 
            set
            {
                PlaybackSettings.TransitionIsSynced = value;
                OnPropertyChanged();
            }
        }
    }
}

