﻿using MediaPlayerDAL.Model;
using PlaylistManager.Service;
using System;

namespace PlaylistManager.ViewModel
{
    internal class StageViewModel : BaseScreenViewModel<Stage>
    {
        public StageViewModel(Stage model, PlayerService playerService, LoadingScreenService loadingScreenService) 
            : base(model, playerService, loadingScreenService)
        {
        }

        public override string ThumbnailPath => String.Format("/PlaylistManager;component/Images/Stages/{0}.png", Name);

        public override bool SupportsDynamicTracks => true;
    }
}
