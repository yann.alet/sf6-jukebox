﻿using PlaylistManager.Contract;
//using PlaylistManager.View;
using PlaylistManager.ViewModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using System.Windows;
using System;
using PlaylistManager.View;

namespace PlaylistManager.Service
{
    public class LoadingScreenService
    {
        IFreezableWindow Window { get; set; }

        public void SetOwner(IFreezableWindow window) 
        {
            Window = window;
        }

        private Point GetOwnerCenter()
        {
            var window = Window as Window;
            Point center = new Point(window.Left + window.Width/2, window.Top + window.Height / 2);
            return center;
        }

        public void Show(string title, Action task, Action onCompletion = null)
        {
            Window.BlurBackground();
            Point center = GetOwnerCenter();
            Task.Run(() =>
            {
                Thread.Sleep(200);
                ShowLoadingScreen(Application.Current.Dispatcher, title, center);
                AutoResetEvent sync = new AutoResetEvent(false);
                Application.Current.Dispatcher.Invoke(() =>
                {
                    task?.Invoke();
                    sync.Set();
                });
                sync.WaitOne();

                ((Window)Window).Dispatcher.Invoke(() =>
                {
                    onCompletion?.Invoke();
                });
            });            
        }

        private void ShowLoadingScreen(Dispatcher MainDispatcher, string title, Point ownerCenter)
        {
            Thread newWindowThread = new Thread(new ThreadStart(() =>
            {
                MainDispatcher.BeginInvoke(new Action(() =>
                {
                    Window.BlurBackground();
                }));

                SynchronizationContext.SetSynchronizationContext(new DispatcherSynchronizationContext(Dispatcher.CurrentDispatcher));
                var vm = new LoadingViewModel() { TaskTitle = title };
                LoadingView loadingView = new LoadingView() { DataContext = vm, Topmost = true };
                loadingView.Top = ownerCenter.Y - loadingView.Height / 2;
                loadingView.Left = ownerCenter.X - loadingView.Width / 2;
                vm.View = loadingView;
                loadingView.Closed += (s, e) =>
                {
                    MainDispatcher.BeginInvoke(new Action(() =>
                    {
                        Window.UnBlurBackground();
                    }));

                    Dispatcher.CurrentDispatcher.BeginInvokeShutdown(DispatcherPriority.Background);
                };

                loadingView.Show();
                Dispatcher.Run();
            }));
            newWindowThread.Name = Guid.NewGuid().ToString();
            newWindowThread.SetApartmentState(ApartmentState.STA);
            newWindowThread.IsBackground = true;
            newWindowThread.Start();
        }
    }
}
