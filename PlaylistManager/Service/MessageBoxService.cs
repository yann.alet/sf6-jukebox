﻿using PlaylistManager.GlobalEvent;
using PlaylistManager.View;

//using PlaylistManager.View;
using System;
using System.Windows;

namespace PlaylistManager.Service
{
    public class MessageBoxService
    {
        private Window Owner { get; set; }

        public void SetOwner(Window owner)
        {
            Owner = owner;
        }

        public void ShowOkMessageBox(string message, string title, Action onOk = null)
        {
            var e = new MessageBoxEvent(message, title, MessageBoxType.Ok);
            if(onOk != null)
            {
                e.OnOk = onOk;
            }

            e.Owner = Owner;

            EventAggregator.Publish(e);
        }

        public void ShowYesNoMessageBox(string message, string title, Action onYes = null, Action onNo = null)
        {
            var e = new MessageBoxEvent(message, title, MessageBoxType.YesNo);
            if (onYes != null)
            {
                e.OnYes = onYes;
            }

            if(onNo != null)
            {
                e.OnNo = onNo;
            }

            e.Owner = Owner;

            EventAggregator.Publish(e);
        }

        public void ShowYesNoCancelMessageBox(string message, string title, Action onYes = null, Action onNo = null, Action onCancel = null)
        {
            var e = new MessageBoxEvent(message, title, MessageBoxType.YesNoCancel);
            if (onYes != null)
            {
                e.OnYes = onYes;
            }

            if (onNo != null)
            {
                e.OnNo = onNo;
            }

            if(onCancel != null)
            {
                e.OnCancel = onCancel;
            }

            e.Owner = Owner;

            EventAggregator.Publish(e);
        }
    }
}
