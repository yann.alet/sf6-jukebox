﻿using MediaPlayerLib;
using MediaPlayerLib.NAudio;
using MediaPlayerLib.Network.Contract;
using MediaPlayerLib.Requests;
using System;
using System.IO;
using System.Reflection;

namespace PlaylistManager.Service
{
    public class PlayerService : IDisposable
    {
        public class OnReadInfo
        {
            public OnReadInfo(int playerIndex, long position, long length)
            {
                Position = position;
                Length = length;
                PlayerIndex = playerIndex;
            }

            public long Length { get; }
            public long Position { get; }
            public int PlayerIndex { get; }
        }

        public event EventHandler<int> OnPlay;
        public event EventHandler<int> OnStop;
        public event EventHandler<int> OnTransition;
        public event EventHandler<OnReadInfo> OnRead;
        public event EventHandler<int> OnCrossFade;

        public PlayerService(MediaPlayerDAL.Model.Settings settings, ILogger logger)
        {
            ThreadedPlayer = new ThreadedPlayer(settings, logger, true);
            ThreadedPlayer.OnRead += ThreadedPlayer_OnRead;
            ThreadedPlayer.OnCrossFade += ThreadedPlayer_OnCrossFade;
            var executingDir = Directory.GetParent(Assembly.GetExecutingAssembly().Location);
            string resamplingBackupFolder = Path.Combine(executingDir.FullName, @"Resampling-Backup");
            if(!Directory.Exists(resamplingBackupFolder))
            {
                Directory.CreateDirectory(resamplingBackupFolder);
            }

            Resampler = new Resampler(logger, resamplingBackupFolder, 44100);
        }

        private void ThreadedPlayer_OnCrossFade(object sender, int e)
        {
            OnCrossFade?.Invoke(sender, e);
        }

        private void ThreadedPlayer_OnRead(object sender, NaudioPlayer.OnReadInfo e)
        {
            OnRead?.Invoke(this, new OnReadInfo(e.PlayerId, e.Position, e.Length));
        }

        public int PlayerId { get; set; } = -1;
        public bool IsPlaying { get; set; }
        public Resampler Resampler { get; }

        public float[] GetAudioData(int playerId)
        {
            return ThreadedPlayer.GetAudioData(playerId);
        }

        public float[] GetAudioData(string path, MediaPlayerDAL.Model.PlaybackSettings playbackSettings)
        {
            return ThreadedPlayer.GetAudioData(path, playbackSettings);
        }

        public void SetPlaybackSettings(MediaPlayerDAL.Model.PlaybackSettings settings)
        {
            ThreadedPlayer.SetPlaybackSettings(settings);
        }

        public void SetPlaybackSettings(int playerId, MediaPlayerDAL.Model.PlaybackSettings settings)
        {
            ThreadedPlayer.SetPlaybackSettings(playerId, settings);
        }

        public int Play(string filePath, MediaPlayerDAL.Model.PlaybackSettings settings)
        {
            if(IsPlaying)
            {
                OnStop?.Invoke(this, PlayerId);
            }

            PlayerId = ThreadedPlayer.Play(new Track(filePath, settings));            
            IsPlaying = true;
            OnPlay?.Invoke(this, PlayerId);
            return PlayerId;
        }

        public int Transition(string filePath, MediaPlayerDAL.Model.PlaybackSettings settings)
        {
            int playerId = ThreadedPlayer.Transition(new Track(filePath, settings));
            OnTransition?.Invoke(this, PlayerId);
            IsPlaying = true;
            PlayerId = playerId;
            return PlayerId;
        }

        public int Stop()
        {
            PlayerId = ThreadedPlayer.Stop();
            OnStop?.Invoke(this, PlayerId);
            IsPlaying = false;
            return PlayerId;
        }

        public void Pause()
        {
            ThreadedPlayer.Pause();
        }

        public void Resume()
        {
            ThreadedPlayer.Resume();
        }

        public void SetPosition(int positionInMilliSeconds)
        {
            ThreadedPlayer.SetPosition(positionInMilliSeconds);
        }

        public void Dispose()
        {
            ThreadedPlayer.Dispose();
        }

        public bool HasValidSampleRate(string path)
        {
            bool isValid = Resampler.HasValidSampleRate(path);
            return isValid;
        }

        public void Resample(string path)
        {
            Resampler.Resample(new System.Collections.Generic.List<string> { path } );
        }

        ThreadedPlayer ThreadedPlayer { get; set; }
    }
}
