﻿using MediaPlayerDAL.Model;
using System;
using System.IO.Compression;
using System.IO;
using System.Linq;
using System.Reflection;
using MediaPlayerDAL.Service;
using System.Collections.Generic;
using MediaPlayerDAL.Contract;
using MediaPlayerDAL;

namespace PlaylistManager.Service
{
    public class ProgressTracker
    {
        public Action<string, float> Callback { get; set; }
        public int Processed { get; set; }
        public int TotalToProcess { get; set; }
    }

    public class ImportExportService
    {
        private string SupportedVersion = "1.1.0";

        public Settings Settings { get; }
        public MessageBoxService MessageBoxService { get; }
        public LoadingScreenService LoadingScreenService { get; }

        public ImportExportService(Settings settings, MessageBoxService messageBoxService, LoadingScreenService loadingScreenService)
        {
            Settings = settings;
            MessageBoxService = messageBoxService;
            LoadingScreenService = loadingScreenService;
        }

        public Export Extract(string exportFilePath)
        {
            string tempDir = Path.Combine(Path.GetTempPath(), "JukeboxImport");
            if (Directory.Exists(tempDir))
            {
                Directory.Delete(tempDir, true);
            }
            Directory.CreateDirectory(tempDir);
            string zipFilePath = Path.Combine(tempDir, Path.GetFileNameWithoutExtension(exportFilePath) + ".zip");

            // Uncorrupt
            using (var fs = new FileStream(zipFilePath, FileMode.CreateNew))
            {
                using (var fs2 = new FileStream(exportFilePath, FileMode.Open))
                {
                    int bytesRead = -1;
                    byte[] bytes = new byte[1024];
                    bool firstChunk = true;
                    while ((bytesRead = fs2.Read(bytes, 0, 1024)) > 0)
                    {
                        if (!firstChunk)
                        {
                            fs.Write(bytes, 0, bytesRead);
                        }
                        firstChunk = false;
                    }
                }

                fs.Flush(true);
            }

            ZipFile.ExtractToDirectory(zipFilePath, tempDir);
            File.Delete(zipFilePath);

            using (var fs = new FileStream(Path.Combine(tempDir, "playlist.xml"), FileMode.Open))
            {
                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(MediaPlayerDAL.Model.Export));
                MediaPlayerDAL.Model.Export export = x.Deserialize(fs) as MediaPlayerDAL.Model.Export;
                if(export.Version != SupportedVersion)
                {
                    MessageBoxService.ShowOkMessageBox($"The version of the export file is not supported. Expected version: {SupportedVersion}. Export version: {export.Version}.", "Import Error");
                    return null;
                }

                FixPlaylistTrackPaths(export, tempDir);
                return export;
            }
        }

        public void Import(Export export, Action<string, float> onProgressCallback = null)
        {   
            var executingDir = Directory.GetParent(Assembly.GetExecutingAssembly().Location);
            var importDirectory = Path.Combine(executingDir.Parent.FullName, @"imports");
            if (!Directory.Exists(importDirectory))
            {
                Directory.CreateDirectory(importDirectory);
            }

            importDirectory = Path.Combine(importDirectory, DateTime.Now.ToString("MM-dd-yy") + " - " + Guid.NewGuid().ToString());
            if (!Directory.Exists(importDirectory))
            {
                Directory.CreateDirectory(importDirectory);
            }

            foreach (var stage in export.Stages)
            {
                CreateImportDirectoryStructure("stages", importDirectory, stage);
                var screenSettings = Settings.Stages.FirstOrDefault(s => s.Name == stage.Name);
                if (screenSettings != null)
                {
                    string screensDirectory = Path.Combine(importDirectory, "stages");
                    string screenDirectory = Path.Combine(screensDirectory, stage.Name);
                    CopyPlaylist(stage.Playlist, screenDirectory);
                    screenSettings.Playlist.Tracks.AddRange(stage.Playlist.Tracks);
                    if(stage.Playlist.Tracks.Any())
                    {
                        screenSettings.Enabled = 1;
                        screenSettings.IsRandomPlaylist = 0;
                    }
                    onProgressCallback?.Invoke("stages", 0.2f);
                }
            }

            foreach (var character in export.Characters)
            {
                CreateImportDirectoryStructure("characters", importDirectory, character);
                var screenSettings = Settings.Characters.FirstOrDefault(s => s.Name == character.Name);
                if (screenSettings != null)
                {
                    string screensDirectory = Path.Combine(importDirectory, "characters");
                    string screenDirectory = Path.Combine(screensDirectory, character.Name);
                    CopyPlaylist(character.Playlist, screenDirectory);
                    screenSettings.Playlist.Tracks.AddRange(character.Playlist.Tracks);
                    if (character.Playlist.Tracks.Any())
                    {
                        screenSettings.Enabled = 1;
                        screenSettings.IsRandomPlaylist = 0;
                    }
                    onProgressCallback?.Invoke("characters", 0.4f);
                }
            }

            foreach (var customScreen in export.CustomScreens)
            {
                CreateImportDirectoryStructure("misc", importDirectory, customScreen);
                var screenSettings = Settings.CustomScreens.FirstOrDefault(s => s.Name == customScreen.Name);
                if (screenSettings != null)
                {
                    string screensDirectory = Path.Combine(importDirectory, "misc");
                    string screenDirectory = Path.Combine(screensDirectory, customScreen.Name);
                    CopyPlaylist(customScreen.Playlist, screenDirectory);
                    screenSettings.Playlist.Tracks.AddRange(customScreen.Playlist.Tracks);
                    if (customScreen.Playlist.Tracks.Any())
                    {
                        screenSettings.Enabled = 1;
                        screenSettings.IsRandomPlaylist = 0;
                    }
                    onProgressCallback?.Invoke("misc", 0.6f);
                }
            }

            foreach (var flowScene in export.FlowScenes)
            {
                CreateImportDirectoryStructure("menus", importDirectory, flowScene);
                var screenSettings = Settings.FlowScenes.FirstOrDefault(s => s.Name == flowScene.Name);
                if (screenSettings != null)
                {
                    string screensDirectory = Path.Combine(importDirectory, "menus");
                    string screenDirectory = Path.Combine(screensDirectory, flowScene.Name);
                    CopyPlaylist(flowScene.Playlist, screenDirectory);
                    screenSettings.Playlist.Tracks.AddRange(flowScene.Playlist.Tracks);
                    if (flowScene.Playlist.Tracks.Any())
                    {
                        screenSettings.Enabled = 1;
                        screenSettings.IsRandomPlaylist = 0;
                    }
                    onProgressCallback?.Invoke("menus", 0.6f);
                }
            }
            
            SerializationService serializationService = new SerializationService();
            serializationService.Save(Settings);

            onProgressCallback?.Invoke("Export Completed", 1);
            MessageBoxService.ShowOkMessageBox("Import completed. Please restart the Playlist Manager to reload the imported playlist(s).", "Import");
        }

        private void CopyPlaylist(Playlist playlist, string partgetDirectory)
        {
            foreach (var track in playlist.Tracks)
            {
                string targetTrackPath = Path.Combine(partgetDirectory, Path.GetFileName(track.Path));
                File.Copy(track.Path, targetTrackPath);
                track.Path = targetTrackPath;
                foreach(var transition in track.Transitions)
                {
                    foreach(var transitionTrack in transition.Tracks)
                    {
                        string targetTransitionTrackPath = Path.Combine(partgetDirectory, Path.GetFileName(transitionTrack.Path));
                        if (!File.Exists(targetTransitionTrackPath))
                        {
                            File.Copy(transitionTrack.Path, targetTransitionTrackPath);
                        }
                        transitionTrack.Path = targetTransitionTrackPath;
                    }
                }
            }
        }

        private void CreateImportDirectoryStructure(string parentDirectoryName, string importDirectory, IScreen screen)
        {
            string stagesDirectory = Path.Combine(importDirectory, parentDirectoryName);
            if (!Directory.Exists(stagesDirectory))
            {
                Directory.CreateDirectory(stagesDirectory);
            }

            string stageDirectory = Path.Combine(stagesDirectory, screen.Name);
            if (!Directory.Exists(stageDirectory))
            {
                Directory.CreateDirectory(stageDirectory);
            }
        }

        private void FixPlaylistTrackPaths(Export export, string baseFolder)
        {
            var playlists = export.Characters.Select(c => c.Playlist).ToList();
            playlists.AddRange(export.CustomScreens.Select(c => c.Playlist));
            playlists.AddRange(export.FlowScenes.Select(c => c.Playlist));
            playlists.AddRange(export.Stages.Select(c => c.Playlist));
            foreach (var playlist in playlists)
            {                
                foreach (var track in playlist.Tracks)
                {
                    track.Path = FixTrackPath(track.Path, baseFolder);
                    foreach(var transition in track.Transitions)
                    {
                        foreach(var transitionTrack in transition.Tracks)
                        {
                            transitionTrack.Path = FixTrackPath(transitionTrack.Path, baseFolder);
                        }
                    }
                }
            }            
        }

        private string FixTrackPath(string trackPath, string baseFolder)
        {
            int i = trackPath.LastIndexOf("\\");
            i = trackPath.Substring(0, i - 1).LastIndexOf("\\");
            i = trackPath.Substring(0, i - 1).LastIndexOf("\\");
            i = trackPath.Substring(0, i - 1).LastIndexOf("\\");
            string relativePath = trackPath.Substring(i + 1);
            return Path.Combine(baseFolder, relativePath);
        }

        private void CopyFilesRecursively(string sourcePath, string targetPath)
        {
            //Now Create all of the directories
            foreach (string dirPath in Directory.GetDirectories(sourcePath, "*", SearchOption.AllDirectories))
            {
                Directory.CreateDirectory(dirPath.Replace(sourcePath, targetPath));
            }

            //Copy all the files & Replaces any files with the same name
            foreach (string newPath in Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories))
            {
                File.Copy(newPath, newPath.Replace(sourcePath, targetPath), true);
            }
        }

        public void CopyPlaylistTracks(Playlist playlist, string targetFolder, ProgressTracker progressTracker = null)
        {
            foreach (var track in playlist.Tracks)
            {
                string targetTrackPath = Path.Combine(targetFolder, Path.GetFileName(track.Path));
                File.Copy(track.Path, targetTrackPath, true);
                track.Path = targetTrackPath;
                progressTracker.Processed++;
                foreach (Transition transition in track.Transitions)
                {
                    foreach (Track transitionTrack in transition.Tracks)
                    {
                        string targetTransitionTrackPath = Path.Combine(targetFolder, Path.GetFileName(transitionTrack.Path));
                        File.Copy(transitionTrack.Path, targetTransitionTrackPath, true);
                        transitionTrack.Path = targetTransitionTrackPath;
                        if (progressTracker != null)
                        {
                            progressTracker.Processed++;
                        }
                    }

                    if (progressTracker != null)
                    {
                        float progress = Convert.ToSingle(progressTracker.Processed) / progressTracker.TotalToProcess;
                        progressTracker.Callback?.Invoke($"{Path.GetFileName(track.Path)}", progress);
                    }
                }
            }
        }

        private int GetTotalTracksInExport(Export export)
        {
            int total = 0;
            List<IScreen> screens = new List<IScreen>();
            screens.AddRange(export.Stages);
            screens.AddRange(export.FlowScenes);
            screens.AddRange(export.Characters);
            screens.AddRange(export.CustomScreens);
            foreach (var screen in screens)
            {
                foreach(var track in screen.Playlist.Tracks)
                {
                    total++;
                    foreach (var transition in track.Transitions)
                    {
                        total += transition.Tracks.Count;
                    }
                }                
            }

            return total;
        }

        public void Export(Export export, string targetFilePath, Action<string, float> onProgressCallback = null)
        {
            string zipFilePath = Path.Combine(Path.GetDirectoryName(targetFilePath), Path.GetFileNameWithoutExtension(targetFilePath) + ".zip");
            string parentDirectory = Path.GetDirectoryName(targetFilePath);
            string tempDir = Path.Combine(Path.GetTempPath(), "JukeboxExport");
            string tracksPath = Path.Combine(tempDir, "tracks");
            ProgressTracker progressTracker = new ProgressTracker { Callback = onProgressCallback, TotalToProcess = GetTotalTracksInExport(export) };

            if (File.Exists(targetFilePath))
            {
                File.Delete(targetFilePath);
            }

            if (File.Exists(zipFilePath))
            {
                File.Delete(zipFilePath);
            }

            if (Directory.Exists(tempDir))
            {
                Directory.Delete(tempDir, true);
            }
            if (Directory.Exists(tracksPath))
            {
                Directory.Delete(tracksPath, true);
            }

            Directory.CreateDirectory(tracksPath);
            if (export.Stages.Any())
            {
                string stagesTracksPath = Path.Combine(tracksPath, "stages");
                Directory.CreateDirectory(stagesTracksPath);
                foreach (var stage in export.Stages)
                {
                    string stageTrackPath = Path.Combine(stagesTracksPath, stage.Name);
                    Directory.CreateDirectory(stageTrackPath);
                    CopyPlaylistTracks(stage.Playlist, stageTrackPath, progressTracker);
                }                
            }

            if (export.FlowScenes.Any())
            {
                string menusTracksPath = Path.Combine(tracksPath, "menus");
                Directory.CreateDirectory(menusTracksPath);
                foreach (var flowScene in export.FlowScenes)
                {
                    string menuTrackPath = Path.Combine(menusTracksPath, flowScene.Name);
                    Directory.CreateDirectory(menuTrackPath);
                    CopyPlaylistTracks(flowScene.Playlist, menuTrackPath, progressTracker);
                }
            }

            if (export.Characters.Any())
            {
                string charactersTracksPath = Path.Combine(tracksPath, "characters");
                Directory.CreateDirectory(charactersTracksPath);
                foreach (var character in export.Characters)
                {
                    string characterTrackPath = Path.Combine(charactersTracksPath, character.Name);
                    Directory.CreateDirectory(characterTrackPath);
                    CopyPlaylistTracks(character.Playlist, characterTrackPath, progressTracker);
                }
            }

            if (export.CustomScreens.Any())
            {
                string miscsTracksPath = Path.Combine(tracksPath, "misc");
                Directory.CreateDirectory(miscsTracksPath);
                foreach (var customScreen in export.CustomScreens)
                {
                    string miscTrackPath = Path.Combine(miscsTracksPath, customScreen.Name);
                    Directory.CreateDirectory(miscTrackPath);
                    CopyPlaylistTracks(customScreen.Playlist, miscTrackPath, progressTracker);
                }
            }

            System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(export.GetType());

            var exportPlaylistFilePath = Path.Combine(tempDir, "playlist.xml");
            using (var fs = new FileStream(exportPlaylistFilePath, FileMode.Create))
            {
                export.Version = SupportedVersion;
                x.Serialize(fs, export);
                fs.Flush();
            }

            
            ZipFile.CreateFromDirectory(tempDir, zipFilePath);

            // Corrupt file
            using (var fs = new FileStream(targetFilePath, FileMode.CreateNew))
            {
                byte[] corruptionBuffer = new byte[1024];

                fs.Write(corruptionBuffer, 0, corruptionBuffer.Length);
                fs.Flush(true);
                using (var fs2 = new FileStream(zipFilePath, FileMode.Open))
                {
                    int bytesRead = -1;
                    byte[] bytes = new byte[1024];
                    while ((bytesRead = fs2.Read(bytes, 0, 1024)) > 0)
                    {
                        fs.Write(bytes, 0, bytesRead);
                    }
                }

                fs.Flush(true);
            }

            File.Delete(zipFilePath);

            onProgressCallback?.Invoke("Export Completed", 1);
            MessageBoxService.ShowOkMessageBox("Export completed!", "Export");
        }
    }
}
