﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Threading;

namespace PlaylistManager.Service
{
    public static class EventAggregator
    {
        private static readonly Dictionary<Type, List<Tuple<object, Dispatcher>>> strongSubscribers = new Dictionary<Type, List<Tuple<object, Dispatcher>>>();


        private static bool isSuspended;

        public static Action<T> Subscribe<T>(Action<T> action, Dispatcher dispatcher = null)
        {
            Type t = typeof(T);

            if (IsSubscribed(action))
            {
                Unsubscribe(action);
            }

            if (!strongSubscribers.ContainsKey(t))
            {
                strongSubscribers[t] = new List<Tuple<object, Dispatcher>>();
            }

            strongSubscribers[t].Add(new Tuple<object, Dispatcher>(action, dispatcher));
            return action;
        }

        public static void Unsubscribe<T>(Action<T> action)
        {
            Type t = typeof(T);

            // Remove from strong references
            if (strongSubscribers.ContainsKey(t))
            {
                strongSubscribers[t].RemoveAll(a => a.Item1 as Action<T> == action);
            }
        }

        public static void Publish<T>(T e)
        {
            if (isSuspended)
            {
                return;
            }

            Type t = typeof(T);

            // Collect the subscribers that want to listen to our events
            //  This step is needed in case an Event Call changes the subscriber list
            List<Tuple<Action<T>, Dispatcher>> subscriberList = new List<Tuple<Action<T>, Dispatcher>>();

            if (strongSubscribers.ContainsKey(t))
            {
                foreach (var action in strongSubscribers[t])
                {
                    var tuple = new Tuple<Action<T>, Dispatcher>(action.Item1 as Action<T>, action.Item2 as Dispatcher);
                    subscriberList.Add(tuple);
                }
            }

            // Publish the Event to our subscribers
            foreach (Tuple<Action<T>, Dispatcher> action in subscriberList)
            {
                if (action.Item2 != null)
                {
                    action.Item2.Invoke(new Action(() =>
                    {
                        action.Item1(e);
                    }));
                }
                else
                {
                    action.Item1(e);
                }
            }
        }

        public static void Suspend()
        {
            isSuspended = true;
        }

        public static void Resume()
        {
            isSuspended = false;
        }

        public static bool IsSubscribed<T>(Action<T> action)
        {
            Type t = typeof(T);

            return strongSubscribers.ContainsKey(t) && strongSubscribers[t].Any(a => a as Action<T> == action);
        }
    }
}
